// name		: meta-press.js
// SPDX-FileCopyrightText: 2017-2021 Simon Descarpentres <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only
//
/* globals List, Choices, browser */
// domParser : Working Draft (Firefox 12)
// crypto : Firefox 26
// fetch : Firefox 39
// includes : Firefox 43
// permissions : Firefox 48
// storage : Firefox 48
// Intl.DisplayNames : unsupported yet (Chrome 81 - 2020-04-20)
// XPathEvaluator.evaluate
//
import * as µ from './utils.js'
import * as mµ from './mp_utils.js'
mµ.set_theme() // exceptional priority
import * as g from './gettext_html_auto.js/gettext_html_auto.js'

// dropdown button
let menus = document.querySelectorAll('.drop_btn')
for (const i of menus) {
	i.onclick = e => {
		let elt = e.target
		if (elt.classList.contains('drop_active')) {
			elt.innerHTML = '&#x2795;'
		} else {
			elt.innerHTML = '&#x2796;'
		}
		elt.classList.toggle('drop_active')
		elt.parentNode.querySelector('.drop_down_div').classList.toggle('drop_display')
	}
}

var test_integrity_data
var test_src
var xml_src
var sources_objs
var mp_req
var controller 
var rest_search_source = [] // Marin
function make_mp_print (name,nat_obj) {
	return function () {
		log += `${name} : `
		nat_obj.apply(console, arguments)
		for (var i=0; i<arguments.length; i++)
			log += ' '+ String(arguments[i])
		log += '\n'
	}
}
var alert_console = false
if (alert_console) {
	var log = ''
	console.error = make_mp_print('Error', console.error)
	console.warn = make_mp_print('Warning', console.warn)
	console.log = make_mp_print('Log', console.log)
	console.info = make_mp_print('Info', console.info)
	let btn = document.getElementById('mp_dev')
	btn.style.display = 'block'
	btn.addEventListener('click', () => alert(log))
}
/* TEST SOURCES */
function no_err(name, msg, caller) {
	let local_url = new URL(window.location)
	let query = local_url.searchParams.get('q')
	if(test_src === '1') {
		let opener_btn = opener.document.getElementById(name)
		if(!opener_btn.classList.contains('error') &&
			!opener_btn.classList.contains('warn') &&
				!opener_btn.classList.contains('no_result_error')) {
			edit_style_class(opener_btn,'success')
			opener_btn.innerHTML = 'V'
		}
		var opener_link  = opener.document.getElementById(`report_${name}`).getElementsByClassName(caller)[0].querySelector(`[data-query='${query}']`).getElementsByTagName('a')[0]
		if(!opener_link.classList.contains('error') &&
			!opener_link.classList.contains('warn') &&
				!opener_link.classList.contains('no_result_error'))
		{
			let elt = opener.document.getElementById(`report_${name}`).getElementsByClassName(caller)[0]
			add_result_test_to_query(elt, name, msg, query, 'V', 'success')
		}
		
	}
}
function add_err(error,name,status,caller) {
	console[status](name, error)
	mp_test.testList.search(`report_${name}`,['src_id'])
	let local_url = new URL(window.location)
	let query = local_url.searchParams.get('q')
	let elt, val, elt_btn
	if(typeof(test_src) !== 'undefined' && test_src === '1') {
		elt_btn = opener.document.getElementById(name)// on recupere le bouton de la matrice
		elt = opener.document.getElementById(`report_${name}`).getElementsByClassName(caller)[0] // on trouve l'élément report de la liste correspondant
		if(caller !== 'test_diff') {
			if (error === 'No result') {
				val = 'N'
				status = 'no_result_error'
				if(elt_btn.innerHTML!=='N') {
					edit_style_class(elt_btn,status)
				}
			}
			else { val = 'X' }
			edit_style_class(elt,`${status}_`)
			elt_btn.innerHTML = val
			// on definit la valeur sur le bouton, on la change uniquement si c'est un V (a voir si on la changerait pas dans tous les cas pour suppr les N
		}
		add_result_test_to_query(elt, name, error, query, val, status)
	} else if(caller === 'test_integrity') {
		elt_btn  = document.getElementById(name)// on recupere le bouton de la matrice
		elt_btn.innerHTML = "X" // on definit la valeur sur le bouton
		elt = document.getElementById(`report_${name}`).getElementsByClassName(caller)[0] // on trouve l'élément report de la liste correspondant
		let p = document.createElement('a')
		p.setAttribute('data-error', error)
		p.innerHTML = error
		elt.appendChild(p)
		edit_style_class(p,status)
		elt.previousElementSibling.style.display = 'flex'
	} else {
		return
	}
	if(!elt_btn.classList.contains('error') && !elt_btn.classList.contains(status) && caller !== 'test_diff') {// si le bouton n'a pas la class error ou la class du status
			edit_style_class(elt_btn,status) // on l'ajoute
	}
	mp_test.testList.search()
}
function add_result_test_to_query(elt, name, txt, query, val, status) {
	txt = txt.toString()
	local_url.searchParams.set('test_sources','0')
	local_url.searchParams.set('name',name)
	local_url.searchParams.set('q',query)
	local_url.searchParams.set('submit','1')
	if(typeof(txt.split(' - ')[1]) !== 'undefined') {
		var nb_art = txt.split(' - ')[0]
		txt = txt.split(' - ')[1]
	}
	let div_q, link_q
	elt.previousElementSibling.style.display = 'flex'
	if(elt.querySelector(`[data-query='${query}']`) !== null) {
		div_q = elt.querySelector(`[data-query='${query}']`)
		link_q = div_q.getElementsByTagName('a')[0]
		if(link_q.getElementsByTagName('span')[0])
			link_q.getElementsByTagName('span')[0].innerHTML = val
	} else {
		div_q = document.createElement('div')
		div_q.setAttribute('data-query',query)
		elt.appendChild(div_q)
		link_q = document.createElement('a')
		link_q.innerHTML = val ? `<span>${val}</span> - ` + query : query
		link_q.target = '_blank'
		link_q.href = local_url
		div_q.appendChild(link_q)
	}
	edit_style_class(link_q,status)
	if(div_q.querySelector(`[data-error="${txt}"]`) !== null) {
		let p = div_q.querySelector(`[data-error="${txt}"]`)
		if(typeof(nb_art) !== 'undefined') {
			let title = p.getAttribute('title')
			p.setAttribute('title',`${title}, ${nb_art}`)
		}
	} else {
		let p = document.createElement('p')
		p.setAttribute('data-error', txt)
		if(typeof(nb_art) !== 'undefined')
			p.setAttribute('title',`Articles : ${nb_art}`)
		p.innerHTML = txt
		div_q.appendChild(p)
	}
}
function edit_style_class(elt,status) {
	elt.classList.remove('default','success','warn','error','no_result_error')
	if(µ.is_array(status)) {
		for(let i of status) { elt.classList.add(i) }
	} else { elt.classList.add(status) }
}
var mp_test = {
	testList: new List('mp_display_test', {
		item: 'test-item',
		valueNames: ['f_h1','src_info',
			{ name: 'src_id', attr: 'id' },
			{ name: 'info', attr: 'style' },
			{ name: 'f_icon', attr: 'src' },
			{ name: 'f_name', attr: 'title'},
			{ name: 'key_src', attr: 'data-key_src'},
			{ name: 'name_src', attr: 'data-name_src'},
			{ name: 'toggle_fct', attr:'script'}
		]
	})
}
async function retest_sources() {
	document.getElementById('mp_test_results').style = "display:none;"
	mp_test.testList.filter()
	var err_src = document.querySelectorAll('.test_src.error_, .test_src.default_, .test_src.warn_, .test_src.no_result_error_')
	reset_all_src_for_query()
	console.log(all_src_for_query)
	for(let err_src_tested of err_src) {
		let key_src_to_retest = err_src_tested.getAttribute('data-key_src')
		let name_src_to_retest = err_src_tested.getAttribute('data-name_src')
		get_all_search_terms(key_src_to_retest)
		edit_style_class(err_src_tested,'default')
		err_src_tested.innerHTML = ''
		let btn_mat = document.getElementById(name_src_to_retest)
		btn_mat.textContent = 'O'
		edit_style_class(btn_mat,'default')
		//err_src_tested.parentNode.parentNode.getElementsByClassName('btn_retest')[0].getElementsByTagName('button')[0].click()
	}
	console.log(all_src_for_query)
	mµ.clear_reserved_name()
	test_sources()
	results_test(false)
}
/**
 * Export the test result to JSON format.
 */
function export_test_JSON(){
	mp_test.testList.filter()
	let elt = document.getElementById('mp_report')
	let all_test_results = elt.querySelectorAll('.test_diff div[data-results]')
	let json_str ={}
	for(let i of all_test_results) {
		let query = i.getAttribute('data-query')
		let name = i.getAttribute('data-name')
		let str = i.getAttribute('data-results')
		let obj = {}
		obj[query] = JSON.parse(str)
		if(typeof(json_str[name]) === 'undefined')
			json_str[name] = obj
		else
			json_str[name] = Object.assign(json_str[name], obj)
		//json_str += str
	}
	mp_test.testList.filter(i => i.values().f_h1 === '')
	json_str = JSON.stringify(json_str, null, '\t')
	µ.upload_file_to_user(`test_meta-press.es.json`, json_str)
}
/**
 * Give the default search terme for the given tech tags.
 * @param {Array} src_tags_tech List of tags (string)
 * @returns {Array} the terms
 */
function get_default_terms(src_tags_tech) { 
	if(src_tags_tech.includes('many words')) {
		return DEFAULT_SEARCH_TERMS['many words']
	} else if (src_tags_tech.includes('one word')) {
		return DEFAULT_SEARCH_TERMS['one word']
	} else {
		return DEFAULT_SEARCH_TERMS['approx']
	}
}
function filter_src_test(strict_equal) {
	let val = document.getElementById('mp_filter_report').value
	val = µ.triw(val)
	if(strict_equal && val !== "")
		mp_test.testList.filter(i => i.values().f_h1.toLowerCase() === val.toLowerCase())
	else {
		mp_test.testList.filter(i => i.values().f_h1.toLowerCase().includes(val.toLowerCase()))
	}
}
document.getElementById('mp_search_filter_report').addEventListener('click', () => {filter_src_test(true)})
document.getElementById('mp_filter_report').addEventListener('keyup', () => {filter_src_test(false)})
document.getElementById('mp_clear_filter_report').addEventListener('click', () => {
	document.getElementById('mp_filter_report').value = ''
	mp_test.testList.filter(i => i.values().f_h1 === '')//funciton filter sur ''
})
var dateBefore, dateAfter
var all_open = false
var local_url = new URL(window.location)//recupère l'url
local_url = local_url.origin + local_url.pathname
local_url = new URL(local_url)
const DEFAULT_SEARCH_TERMS = {
	'one word': ['Esperanto','Biden', 'Quadrature du Net'],
	'many words': ['Quadrature du net','Watergate','Gilets Jaunes','Jaunes Gilets'],
	'approx': ['Esperanto','Quadrature du Net']
}
var all_src_for_query
function reset_all_src_for_query(){
all_src_for_query = {}
for(let i of Object.keys(DEFAULT_SEARCH_TERMS)){
	for(let j of DEFAULT_SEARCH_TERMS[i]){
		if(!Object.keys(all_src_for_query).includes(j.toLowerCase()))
			all_src_for_query[j.toLowerCase()] = {"not_rdy": [], "rdy": []}
	}
}
}
function get_all_search_terms(src_key){
	let src_positive_sch_terms = sources_objs[src_key]['positive_test_search_term']
	let src_tags_tech = sources_objs[src_key]['tags']['tech']
	let src_tested_query
	if (typeof(src_positive_sch_terms) !== 'undefined') {
		if (µ.is_array(src_positive_sch_terms)) {
			src_tested_query = src_positive_sch_terms
			let default_sch_terms = get_default_terms(src_tags_tech) //Marin
			let max_length = Math.max(src_tested_query.length, default_sch_terms.length)
			for (let s = 0; s < max_length; s++) {
				if (typeof (src_tested_query[s]) === "undefined" || src_tested_query[s] === ''){
					src_tested_query[s] = default_sch_terms[s]
				} else {
					if(!Object.keys(all_src_for_query).includes(src_tested_query[s].toLowerCase()))
						all_src_for_query[src_tested_query[s].toLowerCase()] = {"not_rdy": [], "rdy": []}
				}
				all_src_for_query[src_tested_query[s].toLowerCase()]["not_rdy"].push(src_key)
			}
		}
	} else {// if positive_test_search_term is undefined
		src_tested_query = get_default_terms(src_tags_tech)
		for (let s of Object.keys(src_tested_query)) {
			all_src_for_query[src_tested_query[s].toLowerCase()]["not_rdy"].push(src_key)
		}
	}
}
/**
 * Test (integrity, unit and json diff) for the given sources.
 * @param {Object} sources_objs The sources
 */
function before_test_sources(sources_objs) {
	reset_all_src_for_query()
	var elt = document.getElementById('mp_display_result_test') //recupère mp_display_result_test
	dateBefore = new Date().getTime()
	mp_test.testList.clear()
	for(let src_tested of Object.keys(sources_objs)) {//pour toutes les sources
		// matrice creation
		let src_tested_name = sources_objs[src_tested]['tags']['name']//on récupère le nom
		let src_tested_info = sources_objs[src_tested]['tags']['notes']
		let src_tested_favicon = sources_objs[src_tested]['favicon_url']//et le favicon
		get_all_search_terms(src_tested, sources_objs)
		mp_test.testList.add({
			f_h1:src_tested_name,
			src_info: src_tested_info ? src_tested_info : '',
			info: src_tested_info ? 'display:block;' : 'display:none;',
			src_id:`report_${src_tested_name}`,
			f_icon:src_tested_favicon,
			f_name: src_tested,
			key_src: src_tested,
			name_src: src_tested_name
		})//on ajoute cette sources dans la liste pour le rapport
		let src_test_btn, report_div
		if(!document.getElementById(src_tested_name)) {//si il n'y a pas de bouton dans la matrice
			src_test_btn = document.createElement('button')//on le créer
			src_test_btn.id = src_tested_name//on lui ajoute son nom en id
			src_test_btn.title = src_tested_name//et son nom en title
			src_test_btn.addEventListener('click',() => {
				mp_test.testList.filter(i => i.values().f_h1 === src_tested_name)
			})
			elt.append(src_test_btn)//on ajoute le bouton dans la matrice
		} else {//sinon
			src_test_btn = document.getElementById(src_tested_name) //on récupère directement le bouton depuis son id
		}
		src_test_btn.classList.add('btn_test', 'default')//on ajoute au bouton la class défault
		src_test_btn.textContent = 'O'//et une valeur par défault
		report_div = document.getElementById(`report_${src_tested_name}`) // on récupère la div du rapport
		add_retest_btn(report_div)// on lance la fonction create div qui ajoute le bouton retest
		let btn = report_div.querySelectorAll('.result button')
		for(let i of btn){
			i.addEventListener('click', ()=>{
				let btn_elt = i
				let div_to_display = btn_elt.nextElementSibling
				if(div_to_display.style.display === 'none'){
					div_to_display.style.display = 'block'
					btn_elt.getElementsByClassName('triangle')[0].style = 'border-top: none;border-bottom: solid 10px #D0D0D0;'
				} else {
					div_to_display.style.display = 'none'
					btn_elt.getElementsByClassName('triangle')[0].style = 'border-bottom: none;border-top: solid 10px #D0D0D0;'
				}
			})
		}
		// test
	}
	test_sources()
}
function test_sources(){
	for(let q of Object.keys(all_src_for_query)){
		let clone_all = JSON.parse(JSON.stringify(all_src_for_query[q]["not_rdy"]))
		for(let src_tested of clone_all){
			mµ.test_src_integrity(sources_objs, src_tested, test_integrity_data, (ready_to_test) => {
				let src_tested_query
				let src_tags_tech = sources_objs[src_tested]['tags']['tech']// on récupère le tags technique
				let src_positive_sch_terms = sources_objs[src_tested].positive_test_search_term ? sources_objs[src_tested].positive_test_search_term : get_default_terms(src_tags_tech)
				for (const i of ready_to_test[1]) {
					add_err(i.text, i.src_name, i.level,'test_integrity')
				}
				for(let query of src_positive_sch_terms){
					query = query.toLowerCase()
					µ.remove_array_value(all_src_for_query[query]["not_rdy"],src_tested)
					if (ready_to_test[0]) {
						all_src_for_query[query]["rdy"].push( sources_objs[src_tested]['tags']['name'])
					}
					if(all_src_for_query[query]["not_rdy"].length === 0){
							sources_tested(all_src_for_query[query]["rdy"], query, local_url)
					}
				}
			})
		}
	}
	all_open = true
}
function sources_tested(src_tested, src_tested_query, local_url) {
	var elt = document.getElementById('mp_display_result_test')
	var pool = parseInt(elt.getAttribute('data-pool'))
	if(pool > 0) {
		elt.setAttribute('data-pool',pool-1)
		var new_url = new URL(local_url.origin + local_url.pathname)
		new_url.searchParams.set('q', src_tested_query)
		for(let src_tested_name of src_tested){
			let div = document.getElementById(`report_${src_tested_name}`).getElementsByClassName('test_src')[0]
			add_result_test_to_query(div, src_tested_name, '', src_tested_query, 'O', 'default')
			new_url.searchParams.append('name', src_tested_name)
		}
		new_url.searchParams.set('submit',1)
		new_url.searchParams.delete('launch_test')
		new_url.searchParams.set('test_sources',1)
		window.open(new_url.toString(), '_blank')
	} else {
		setTimeout(function () {sources_tested(src_tested, src_tested_query, local_url)}, 200)
	}
}
var old_display = undefined
function add_retest_btn(div) {
	let retest_src_btn = document.createElement('button') // on créer le bouton
	retest_src_btn.classList.add("btn-retro") // on lui ajoute la class btn-retro
	retest_src_btn.textContent = 'Retest' //on lui ajoute son texte
	retest_src_btn.addEventListener('click', async(e) => {// on lui ajoute un évènement
		let par_div = e.target.parentNode.parentNode
		let name = par_div.id
		name = µ.regextract('^report_(.*)',name)
		let data_queries = par_div.getElementsByClassName('test_src')[0].getElementsByTagName('div')
		for(let i of data_queries) {
			let data_query = i.getAttribute('data-query')
			sources_tested([name], data_query, local_url)
		}
	})
	let div_btn = div.getElementsByClassName('btn_retest')[0]
	div_btn.append(retest_src_btn)
}
function results_test(speed_test) {
	var pool = parseInt(
		document.getElementById('mp_display_result_test').getAttribute('data-pool'))
	if(pool===200 && all_open) {// when pool == 400, all tab for test are close, test is finished
		if(speed_test) {
			dateAfter = new Date().getTime()
			console.log('Speed test: '+ (dateAfter - dateBefore)/1000)
		}
		let caller = {
			"test_integrity": "Test integrity",
			"test_src": "Test sources",
			"test_diff": "Test diff"
		}
		for (let i of Object.keys(caller)){
			let warning = document.querySelectorAll(`.${i} a.warn`).length
			let error = document.querySelectorAll(`.${i} a.error`).length
			let txt = `${caller[i]}: `
			if(i === 'test_src'){
				let success = document.querySelectorAll(`.${i} a.success`).length
				let no_result = document.querySelectorAll(`.${i} a.no_result_error`).length
				txt += `Success: ${success}, No result Error: ${no_result}, `
			}
			txt += `Warning: ${warning}, Error: ${error}`
			let p = document.getElementById(i)
			p.innerHTML = txt
		}
		mp_test.testList.filter(i => i.values().f_h1 === '')
		let div = document.getElementById('mp_test_results')
		div.style = "display:inline"
		var retest_btn = document.getElementById('mp_retest')
		retest_btn.style = "display:inline;"
		retest_btn.addEventListener('click',retest_sources)
		var test_JSON_btn = document.getElementById('mp_test_JSON')
		test_JSON_btn.style = "display:inline;"
		test_JSON_btn.addEventListener('click',export_test_JSON)
	}
	else { setTimeout(function () {results_test(speed_test)}, 200)}
}
/*	 */
(async () => {
	'use strict'
	/* * definitions * */
	const MAX_RES_BY_SRC = await mµ.get_stored_max_res_by_src()
	var   HEADLINE_PAGE_SIZE = await mµ.get_stored_headline_page_size()
	const HEADLINE_TITLE_SIZE = 140  // charaters
	const META_FINDINGS_PAGE_SIZE = 15
	const EXCERPT_SIZE = 350	// charaters //jsondiff // maxtextcaractere
	var   dynamic_css = mµ.createStyleSheet()
	const CSS_CHECKBOX_RULE_INDEX = dynamic_css.insertRule(`.f_selection {display:none}`, 0)
	const userLang = await mµ.get_wanted_locale()
	var sources_keys = []
	var current_source_selection = []
	var current_source_nb = 0
	mp_req = {
		findingList: new List('findings', {  // Definition of a result record in List.js
			item: 'finding-item',
			valueNames: ['f_h1', 'f_txt', 'f_source', 'f_dt', 'f_by',
				{ name: 'f_source_title', attr: 'title' },
				{ name: 'mp_data_source', attr: 'mp-data-src'},
				{ name: 'f_by_title', attr: 'title' },
				{ name: 'f_ISO_dt', attr: 'datetime' },
				{ name: 'f_h1_title', attr: 'title' },
				{ name: 'f_dt_title', attr: 'title' },
				{ name: 'f_img_src', attr: 'src'},
				{ name: 'f_img_alt', attr: 'alt'},
				{ name: 'f_img_title', attr: 'title'},
				{ name: 'mp_data_img', attr: 'mp-data-img'},
				{ name: 'mp_data_alt', attr: 'mp-data-alt'},
				{ name: 'mp_data_title', attr: 'mp-data-title'},
				{ name: 'mp_data_key', attr: 'mp-data-key'},
				{ name: 'f_url', attr: 'href' },
				{ name: 'f_icon', attr: 'src' },
			],
			page: 10,
			pagination: { outerWindow: 2, innerWindow: 3 },
			// fuzzySearch: { distance: 1501 }, // Unused. 1000 would search through 400 char.
		}),
		metaFindingsList: new List('meta-findings', { // List of source related info for a query
			item: 'meta-findings-item',
			valueNames: [
				{ name: 'mf_icon', attr: 'src' },
				'mf_name',
				{ name: 'mf_title', attr: 'title' },
				{ name: 'mf_ext_link', attr: 'href' },
				{ name: 'mf_remove_source', attr: 'data-mf_remove_source' },
				{ name: 'mf_res_nb', attr: 'data-mf_res_nb' },
				{ name: 'mf_report', attr: 'data-mf_report_source'},
				'mf_locale_res_nb',
			],
			page: META_FINDINGS_PAGE_SIZE,
			pagination: true
		}),
		running_query_countdown: 0,
		final_result_displayed: 0,
		query_result_timeout: null,
		query_final_result_timeout: null,
		query_start_date: null,
		query_abort_search_timeout : null 
		//query_promises: []  //N'est plus utile
	}
	function update_nb_src() {
		current_source_nb = current_source_selection.length
		document.getElementById('cur_tag_sel_nb').textContent = current_source_nb
		mµ.get_nb_needed_host_perm(current_source_selection, sources_objs).then((obj) => {
			let span = document.getElementById('cur_tag_perm_nb')
			span.textContent = obj.not_perm
			span.setAttribute('title', obj.names.toString())
		})
		let search_button = document.getElementById('mp_submit')
		if (current_source_nb === 0) {
			search_button.disabled = true
			search_button.style.cursor = 'not-allowed'
		} else if (search_button.disabled) {
			search_button.disabled = false
			search_button.style.cursor = ''
		}
	}
	/**
	 * Display the image stored in mp-data-img.
	 * @param {click} e Click event
	 */
	function load_result_image (e) {
		var mp_data_src = e.target.attributes['mp-data-img'].textContent
		var mp_data_alt = e.target.attributes['mp-data-alt'] ?
			e.target.attributes['mp-data-alt'].textContent : ''
		var mp_data_title = e.target.attributes['mp-data-title'] ?
			e.target.attributes['mp-data-title'].textContent : ''
		for (let img of document.querySelectorAll(`img[mp-data-img='${mp_data_src}']`)) {
			img.src = mp_data_src
			img.alt = mp_data_alt
			img.title = mp_data_title
		}
	}
	function image_not_load (list, src, alt, title) {
		list['mp_data_img'] = src
		list['mp_data_alt'] = alt
		list['mp_data_title'] = title
		list['f_img_src'] = img_load
		list['f_img_alt'] = alt_load_txt
		list['f_img_title'] = ''
	}
	function separate_txt_img (l_fmp, f_img, n) {
		// console.log(f_img) // to find attributes to select
		// console.log(l_fmp, n)
		if (f_img === '' && n.type === 'XML') { // extract image from f_txt
			var f_txt = l_fmp['f_txt']
			var f_txt_dom = µ.dom_parser.parseFromString(f_txt, 'text/html')
			f_img = f_txt_dom.querySelector('img')
			// f_img = f_txt_dom.getElementsByTagName('img')[0] // it's not faster with Firefox 90.a1
			// it's slower in Chromium Version 90.0.4430.85 (Build officiel) Arch Linux (64 bits)
			l_fmp['f_txt'] = f_txt_dom.body.textContent // removes image from f_txt
		}
		// console.log('f_img', f_img)  // to check what have been caught
		// let n_src_attr = n.r_img_src_attr
		// if (typeof(n_src_attr) === 'undefined') n_src_attr = 'src'
		if (f_img) {
			l_fmp['f_img_src'] = µ.urlify(f_img.getAttribute('src'), n.domain_part)
			l_fmp['f_img_alt'] = f_img.getAttribute('alt') || ''
			l_fmp['f_img_title'] = f_img.getAttribute('title') || ''
			/* if (!is_load_photos && l_fmp['f_img_src']) {
				image_not_load(l_fmp, l_fmp['f_img_src'], l_fmp['f_img_alt'], l_fmp['f_img_title'])
			} */
		}
		return l_fmp
	}
	document.getElementById('mp_query').value = new URL(window.location).searchParams.get('q')
	document.getElementById('mp_query').addEventListener('keypress', function(evt) {
		if (!evt) evt = window.event
		var keyCode = evt.keyCode || evt.which
		if (keyCode === 13) {	// search on pressing Enter key
			document.getElementById('mp_submit').click()
			return false	// returning false will prevent the event from bubbling up.
		}
	})
	if(await mµ.get_stored_sentence_search() === "1") {
		document.getElementById('mp_query').addEventListener('input', () => {
			let query = document.getElementById('mp_query').value
			let mw_query = new RegExp('[^ ]  *[^ ]')
			let mw_tech = tag_select_multiple['tech']
			if (0 < tag_select_multiple['name'].getValue(true).length) {
				mw_tech.removeActiveItemsByValue('one word')
				mw_tech.removeActiveItemsByValue('many words')
			} else {
				if(mw_query.test(query)) {
					mw_tech.removeActiveItemsByValue('one word')
					mw_tech.setChoiceByValue('many words')
				} else {
					mw_tech.removeActiveItemsByValue('many words')
					mw_tech.setChoiceByValue('one word')
				}
			}
			on_tag_sel({})
		})
	}
	function alert_perm() {
		alert(mp_i18n.gettext(
			`Meta-Press.es needs the permission to read the content of the selected sources.
Those 'host' permissions can be dropped or requested once and for all via the
Meta-Press.es setting page.`))
	}
	async function tick_query_duration() {
		document.getElementById('mp_running_duration').textContent =
			(new Date() - mp_req.query_start_date) / 1000
		mp_req.duration_ticker = setTimeout(tick_query_duration, 1000)
	}
	function results_test_json(n) {
		mp_req.findingList.filter(i => {
			if (i.elm) return i.elm.getElementsByClassName('f_selection')[0]})
		let cur_sel_items = []
		var test_filters
		for (let i of mp_req.findingList.matchingItems) {
			if(cur_sel_items.length < n || !n) cur_sel_items.push(i.values())
		}
		let json_str = `{
			"findings": ${JSON.stringify(cur_sel_items)},
			"search_terms": ${JSON.stringify(document.getElementById('mp_query').value)}
		}`
		return json_str
	}
	document.getElementById('mp_submit').addEventListener('click', async () => {
		// disable source panel
		if (current_source_nb === 0) return
		document.getElementById('list_src').style.display = 'none'
		let el = document.getElementById("mp_stop") 
		if (el.disabled) { 
			el.disabled = false
			el.textContent = "Cancel"
			el.style.opacity = 1
		}
		mp_req.search_terms = document.getElementById('mp_query').value
		if (mp_req.running_query_countdown !== 0 || '' === mp_req.search_terms)
			return // to prevent 2nd search during a running one
		mp_req.running_query_countdown = 1 // to prevent 2nd search during permission request
		try {
			let search_src = {}
			for (const i of current_source_selection) {
				search_src[i] = sources_objs[i]
			}
			if (!await mµ.request_sources_perm(search_src)) {
				alert_perm()
				mp_req.running_query_countdown = 0
				return
			}
			mµ.get_nb_needed_host_perm(current_source_selection, sources_objs).then(val => {
				document.getElementById('cur_tag_perm_nb').textContent = val.not_perm
			})
		} catch (exc) {
			console.warn('Ignored exception : ', exc)
		}
		document.getElementById('mp_submit').disabled = true
		document.getElementById('mp_submit').style.cursor = 'not-allowed'
		document.getElementById('mp_stop').style.display = 'inline'
		document.getElementById('search_stats').style.display='none'
		document.getElementById('donation_reminder').style.display='none'
		mp_req.metaFindingsList.search()
		document.body.style.cursor = 'wait'
		mp_req.running_query_countdown = current_source_nb
		//console.log(`Countdown refill ${current_source_nb}`);
		if ('1' !== test_src){
			mp_req.query_start_date = new Date()
			mp_req.duration_ticker = setTimeout(tick_query_duration, 1000)
			clear_results()
		}
		// for (let i of current_source_selection) {	//!\ pb with closures
		//rest_search_source = [] 
		rest_search_source = [...current_source_selection] 	
		controller = new AbortController()
		controller.signal.onabort = (evt) => { 
			if ('1' === test_src) return
			document.getElementById('mp_running_stats').style.display = 'none'
			document.getElementById('mp_load_result').style.display = 'block'
			el.disabled = true
			el.textContent = mp_i18n.gettext("Wait please...")
			el.style.opacity = 0.75
		}
		let time_obj = await browser.storage.sync.get('max_time_before_abort')
		let time = parseInt(time_obj.max_time_before_abort, 10)
		if(time > 0){
			clearTimeout(mp_req.query_abort_search_timeout)
			mp_req.query_abort_search_timeout = setTimeout(() => { controller.abort() }, 1000 * time) 
		}
		function ongoing_2s_timeout() {
			display_ongoing_results()
			mp_req.repeat_timeout_ongoing = setTimeout(ongoing_2s_timeout, 2000)
		}
		mp_req.repeat_timeout_ongoing = setTimeout(ongoing_2s_timeout, 2000)
		current_source_selection.forEach(async (i) => { 
			var n = sources_objs[i]
			var src_query_start_date = new Date()
			try { // https://yashints.dev/blog/2019/08/17/js-async-await
				var raw = fetch(format_search_url(n.search_url, mp_req.search_terms), {
					signal: controller.signal,
					method: n.method || 'GET',
					headers: { 'Content-Type': 'application/x-www-form-urlencoded'},
					body: n.body ? format_search_url(n.body, mp_req.search_terms) : null
				})
			} catch (exc) {
				update_search_query_countdown(i) 
				add_err(exc, n.tags.name,'error', 'test_src')
				throw new Error(`${i}: await fetch error : ${exc}`)
			}
			raw.then( async (response) => {
					var raw_rep = response
					if (!raw_rep.ok) {
						update_search_query_countdown(i) 
						//add_err(`${i}: fetch not ok : ${raw_rep.status}`,n.tags.name,'error', 'test_src')
						throw new Error(`${i}: fetch not ok : ${raw_rep.status}`)
					}
					var c_type = raw_rep.headers.get('content-type')
					if (c_type.includes('application/json') || (n.type === 'JSON')) {
						try {
							if(n.jsonp_to_json_re) {
								let raw = await raw_rep.text()
								raw = µ.triw(raw)
								raw = µ.drop_escaped_quotes(raw)
								rep = µ.regextract(
									n.jsonp_to_json_re[0], raw, n.jsonp_to_json_re[1])
								rep = JSON.parse(rep)
							} else { rep = await raw_rep.json() }
						} catch (exc) {
							update_search_query_countdown(i) 
							add_err(exc,n.tags.name,'error', 'test_src')
							throw new Error(`${i} await raw_rep.json() error : ${exc} ${raw_rep}`)
						}
					} else {
						if (n.tags.charset) {
							raw_rep = await raw_rep.arrayBuffer()
							var text_decoder = new TextDecoder(n.tags.charset, {fatal: true})
							raw_rep = text_decoder.decode(raw_rep)
						} else {
							raw_rep = await raw_rep.text()
						}
						c_type = µ.clean_c_type(c_type)
						// if (n.type === 'RSS') c_type = 'text/xml'	//!\ some sources give a wrong c_type
						var rep = µ.dom_parser.parseFromString(raw_rep, c_type)
						if (µ.isParserError(rep)) {
							if (c_type.includes('xml')) // what was the source needing this ?
								rep = µ.XML_encode_UTF8(µ.HTML_decode_entities(µ.trim(µ.drop_low_unprintable_utf8(raw_rep))))
							rep = µ.dom_parser.parseFromString(rep, c_type)
							if (µ.isParserError(rep)) {
								update_search_query_countdown(i) 
								add_err(`${i} dom parser error ${rep}`,n.tags.name,'error', 'test_src')
								throw new Error(`${i} dom parser error ${rep}`, rep)
							}
						}
					}
					// console.group(`${n.tags.name}`)
					if (!n.domain_part || n.extends) n.domain_part = µ.domain_part(i)
					if (!n.favicon_url && ! ('JSON' === n.type))
						n.favicon_url = µ.get_favicon_url(rep, n.domain_part)
					var fdgs = []
					if ('JSON' === n.type)
						fdgs = typeof(n.results) !== 'undefined'  ? $_('results', rep, n) : rep
					else {
						if (typeof(n.results) !== 'undefined')
							try { fdgs = rep.querySelectorAll(n.results)}
							catch (exc) { add_err(`${i} - ${exc}`,n.tags.name,'error', 'test_src') }
						else if (n.results_xpath)
							try { fdgs = µ.evaluateXPath(rep, n.results_xpath)}
							catch (exc) { add_err(`${i} - ${exc}`,n.tags.name,'error', 'test_src') }
					}
					var fdgs_nb = fdgs ? fdgs.length : 0
					var res_nb = 0
					if (n.type !== 'XML') {
						try { res_nb = $_('res_nb', rep, n)}
						catch (exc) { add_err(`${i} res_nb ${exc}`,n.tags.name,'error', 'test_src') }
					}
					res_nb = Number(res_nb) || fdgs_nb
					if (0 === res_nb) {
						update_search_query_countdown(i) 
						add_err('No result',n.tags.name,'error', 'test_src')
						return 
					}
					var max_fdgs = MAX_RES_BY_SRC
					fdgs_nb = Math.min(fdgs_nb, max_fdgs)
					if (typeof(fdgs_nb) === 'undefined') return search_query_countdown()
					var f_h1='', f_url='', f_by='', f_dt=new Date(0), f_nb=0, f_img=''
					var l_fmp ={'f_img_src':'','f_img_alt':'','f_img_title':'','mp_data_img':'',
						'mp_data_alt':'','mp_data_title':'','f_txt':''}
					var dup_elt, dup_val
					for (let f of fdgs) {
						if (max_fdgs-- === 0) break
						f_nb = MAX_RES_BY_SRC - max_fdgs
						// console.group(`${n.tags.name} result ${f_nb}`)
						f_h1 = ''; f_url=''; l_fmp['f_txt']=''; f_by=''; f_img=''; f_dt=new Date(0)
						l_fmp['f_img_src']=''; l_fmp['f_img_alt']=''; l_fmp['f_img_title']='';
						l_fmp['mp_data_img']=''; l_fmp['mp_data_alt']=''; l_fmp['mp_data_title']=''
						try {
							f_h1	= $_('r_h1', f, n, f_nb)
							if (!f_h1) throw "missing this f_h1"
						} catch (exc) {
							add_err(`${i} - ${f_nb} f_h1 ${exc}`,n.tags.name,'error', 'test_src')
							res_nb -= 1
							continue
						}
						try {
							f_url = µ.urlify($_('r_url', f, n, f_nb), n.domain_part)
							if (!f_url) throw "missing this f_url"
						} catch (exc) {
							add_err(`${i} - ${f_nb} f_url ${exc}`,n.tags.name,'error', 'test_src')
							res_nb -= 1
							continue
						}
						try { l_fmp['f_txt'] = $_('r_txt', f, n, f_nb) }
						catch (exc) { add_err(`${i} - ${f_nb} f_txt ${exc}`,n.tags.name,'error', 'test_src') }
						if (n.type !== 'XML') {
							let is_r_img = typeof n.r_img === 'string'
							if(is_r_img || typeof n.r_img_xpath === 'string') {
								if (is_r_img)
									try { f_img = f.querySelector(n.r_img) }
									catch (exc) { add_err(`${i} ${f_nb} f_img ${exc}`,n.tags.name,'error', 'test_src') }
								else // (typeof n.r_img_xpath === 'string')
									try { f_img = µ.evaluateXPath(f, n.r_img_xpath) }
									catch (exc) { add_err(`${i} ${f_nb} f_img_xpath ${exc}`,n.tags.name,'error', 'test_src') }
							}
						}
						try {
							if(typeof(f_img) !== 'undefined') {
								l_fmp = separate_txt_img(l_fmp, f_img, n)
							}
							if (typeof(n.r_img_src) !== 'undefined' || typeof(n.r_img_src_xpath) !== 'undefined')  {
								l_fmp['f_img_src'] = $_('r_img_src', f, n, f_nb)
								if (typeof(l_fmp['f_img_src']) !== 'undefined')
									l_fmp['f_img_src'] = µ.urlify(l_fmp['f_img_src'], n.domain_part)
							}
							if (typeof(n.r_img_alt) !== 'undefined')
								l_fmp['f_img_alt'] = $_('r_img_alt', f, n, f_nb)
							if (typeof(n.r_img_title) !== 'undefined')
								l_fmp['f_img_title'] = $_('r_img_title', f, n, f_nb)
						} catch (exc) {
							add_err(`${n.r_img} - ${exc}`, n.tags.name, 'warn', 'test_src')
						}
						if (!is_load_photos && l_fmp['f_img_src']) {
							image_not_load(l_fmp, l_fmp['f_img_src'], l_fmp['f_img_alt'],l_fmp['f_img_title'])
						}
						try { f_by = $_('r_by', f, n, f_nb) || n.tags.name }
						catch (exc) { add_err(`${f_nb} - f_by ${exc}`,n.tags.name,'error', 'test_src') }
						try { f_dt = $_('r_dt', f, n, f_nb)
							if (f_dt === '') throw "No f_dt after lookup ($_)"
							f_dt	= mµ.parse_dt_str(f_dt, n.tags.tz, f_nb, n, month_nb_json)
						} catch (exc) { add_err(`${f_nb} -  f_dt ${exc}`,n.tags.name,'error', 'test_src') }
						let f_text
						test_src === '1' ? f_text = µ.shorten(l_fmp['f_txt'], EXCERPT_SIZE)
							: f_text = µ.bolden(l_fmp['f_txt'], mp_req.search_terms)
						try {
							if (await mµ.get_stored_undup_results() === "1" && (dup_elt = get_dup(f_h1)).length) {
								dup_elt = dup_elt[0]
								dup_val = dup_elt.values()
								dup_val.f_source += dup_val.f_source.slice(-1) === '…' ? '' : '…'
								dup_val.f_source_title += `, ${n.tags.name}`
								dup_elt.values(dup_val)
							} else {
								mp_req.findingList.add({f_h1:f_h1, f_url:f_url, f_by:f_by,
									f_txt: f_text,//µ.bolden(l_fmp['f_txt'], mp_req.search_terms),
									f_h1_title:	f_h1,
									f_by_title:	f_by,
									f_dt: f_dt.toISOString().slice(0, -14),
									f_epoc:	f_dt.valueOf(),
									f_ISO_dt: f_dt.toISOString(),
									f_dt_title: f_dt.toLocaleString(userLang),
									f_icon:	n.favicon_url,
									f_img_src: l_fmp['f_img_src'],
									f_img_alt: l_fmp['f_img_alt'],
									f_img_title: l_fmp['f_img_title'],
									mp_data_img: l_fmp['mp_data_img'],
									mp_data_alt: l_fmp['mp_data_alt'],
									mp_data_title: l_fmp['mp_data_title'],
									f_source: n.tags.name,
									f_source_title: n.tags.name,
									mp_data_source: n.tags.name,
									mp_data_key: i
								})
							}
						} catch (exc) { add_err(`${f_nb} - ${f_dt} ${exc}`,n.tags.name,'error', 'test_src') }
						// console.groupEnd()
					}
					no_err(n.tags.name,'No error', 'test_src')
					if(res_nb != 0 && test_src!=1) {
						mp_req.metaFindingsList.add({
							mf_icon: n.favicon_url,
							mf_name: n.tags.name,
							mf_title: mf_title(n.tags.name),
							mf_ext_link: format_search_url(n.search_url_web || n.search_url, mp_req.search_terms),
							mf_remove_source: n.tags.name,
							mf_res_nb: res_nb,
							mf_locale_res_nb: res_nb.toLocaleString(userLang),
						})
					}
					update_search_query_countdown(i) 
					console.log(i, new Date() - src_query_start_date, 'ms',
					mp_req.running_query_countdown, '/', current_source_nb, ':', res_nb, 'res')
				}).catch( (exc) => { 
					update_search_query_countdown(i)
					if (exc.name == 'AbortError') console.log(`${i} IS ABORTED`)
					else add_err(exc,n.tags.name,'error', 'test_src')
        })
			// console.groupEnd()
		})
	})
	function get_dup(f_h1) {
		return mp_req.findingList.get('f_h1', f_h1)
	}
	function update_search_query_countdown(current_s) { // Marin
		let tooltip_text = document.getElementById("mp_tooltip_text")
		let btn_src = document.getElementById("mp_running_button_src")
		let btn_src_text = document.getElementById("mp_running_button_src_text")
		rest_search_source = rest_search_source.filter(s => s !== current_s) 
		search_query_countdown()
		if (test_src !== '1')
			if(mp_req.running_query_countdown === 0){
				clearTimeout(mp_req.query_abort_search_timeout)
				clearTimeout(mp_req.query_result_timeout)
				btn_src.style.display = "none"
				btn_src.textContent = '+'
				tooltip_text.style.visibility = "hidden"
				btn_src_text.style.visibility = "hidden"
				tooltip_text.innerHTML = ""
				btn_src_text.innerHTML = ""
				display_ongoing_results()
			} else if(mp_req.running_query_countdown < 20){ 
				let t = `<h4>${mp_i18n.gettext("Waiting for...")}</h4>`
				btn_src.style.display = "inline"
				tooltip_text.style.visibility = "visible"
				for(let s of rest_search_source){
					let obj = sources_objs[s] //object source
					let obj_key = s
					let obj_favicon = obj.favicon_url === undefined ? "." : obj.favicon_url
					let obj_name = obj.tags.name
					t += `<p class="tooltip_text_family"><img class="f_icon" src=${obj_favicon} width=24> <b>${obj_name}</b> : 
					${obj_key}<p>`
				}
				if(btn_src_text.style.visibility === "visible") btn_src_text.innerHTML = t
				tooltip_text.innerHTML = t
			}
	}
	function search_query_countdown() {
		mp_req.running_query_countdown = rest_search_source.length 
		clearTimeout(mp_req.query_result_timeout)
		mp_req.query_result_timeout = setTimeout(() => {
			display_ongoing_results()
		}, 600)
	}
	function filter_only_one_element(elt) {
		if (elt === null) return
		mp_req.findingList.search(elt.textContent, ['f_source_title'])
		bold_clicked_a(elt, 'mf_name')
		mf_date_slicing()
		filter_last(document.getElementById('mf_all_res'), null)
	}
	function display_ongoing_results() {
		document.getElementById('mp_query_countdown').textContent = mp_req.running_query_countdown
		document.getElementById('mp_running_src_fetched').textContent = current_source_nb
		if(typeof(controller) !== 'undefined' && !controller.signal.aborted) 
		if ('1' !== test_src) {
			document.getElementById('mp_running_stats').style.display = 'block'
			document.getElementsByTagName('title')[0].textContent = document.getElementById(
				'ongoing_tab_title').textContent
		}
		if (mp_req.findingList.size() > 0) {
			mp_req.findingList.sort('f_epoc', {order: 'desc'})
			mp_req.metaFindingsList.show(0, mp_req.metaFindingsList.size())
			for (let a of document.getElementsByClassName('mf_name')) {
				// a.addEventListener('click', evt => {  // would bind multiple times the event
				a.onclick = (evt) => filter_only_one_element(evt.target)
				a.parentNode.onclick = (evt) => {
					filter_only_one_element(evt.target.querySelector('.mf_name'))
				}
			}
			for (let a of document.getElementsByClassName('mf_remove_source')) {
				a.onclick = evt => {
					document.body.style.cursor = 'wait'
					const mf_name = evt.target.parentNode.querySelector('.mf_name')
					mf_name.onclick({target: mf_name}) // filter only the elements to delete
					const cur_page_size = mp_req.findingList.page
					mp_req.findingList.show(0, mp_req.findingList.size()) // display all the elts to del
					const source_name = evt.target.getAttribute('data-mf_remove_source')
					mp_req.findingList.remove('mp_data_source', source_name)
					mp_req.metaFindingsList.remove('mf_name', source_name)
					document.getElementById('mp_query_meta_total').textContent =
						get_mf_res_nb().toLocaleString(userLang)
					mp_req.findingList.show(0, cur_page_size)
					document.getElementById('mf_total').click()
					document.body.style.cursor = 'auto'
				}
			}
			for (let a of document.getElementsByClassName('mf_report')) {
				a.onclick = () => {
					let pop = document.querySelector("#popup")
					if (null !== pop) {
						let par = pop.parentNode
						par.removeChild(pop)
						par.querySelector(".mf_report").disabled = false
					}
					let tar = a
					let parent = a.parentNode
					let name = parent.firstElementChild.textContent.trim()
					let popup = document.createElement("div")
					tar.disabled = true
					popup.classList.add("popup")
					popup.setAttribute("id", "popup")
					popup.innerHTML = `<h4>Report problems for ${name}</h4>
					<select id="select_prob" autocomplete="on" class="btn"></select>
					<label>Send search sentences and settings ?<input id="permit_send" type="checkbox"></label>
					<p>Describe your problem :</p>
					<input class="input_report" type="text" name="problem" id="problems_txt">
					<div>
						<button id="cancel_report" class="a btn">Cancel</button>
						<button id="send_report" class="a btn">Send</button>
					</div>`
					let select = popup.querySelector("#select_prob")
					let list_prob = ["No result", "Missing results", "Bad results", "Inapropriate results"]
					for (let i of list_prob) {
						let opt = document.createElement("option")
						opt.value = i
						opt.textContent = mp_i18n.gettext(i)
						select.appendChild(opt)
					}
					parent.appendChild(popup)
					document.getElementById("cancel_report").onclick = evt => {
						parent.removeChild(popup)
						tar.disabled = false
					}
					document.getElementById("send_report").onclick = evt => {
						let comment = document.getElementById("problems_txt").value
						let prob = document.getElementById("select_prob").value
						let date = new Date()
						if (comment === "") comment = "none"
						// no tab better for readable url
						let url = `https://www.meta-press.es/fbk/${µ.trim(manifest.version)}/${prob}/${name}`
						url = new URL(encodeURI(url))
						fetch(url)
						let setting = ""
						if (document.querySelector("#permit_send").checked) {
							let param = gen_permalink(true).searchParams
							for (const i of param) setting += `&${i[0]}=${i[1]}`
						}
						url = `https://www.meta-press.es/fbk/${µ.trim(manifest.version)}/${prob}/${name}?date=${date.toISOString()}&txt=${comment}${setting}`
						fetch(url)
						parent.removeChild(popup)
					}
				}
			}
			mp_req.metaFindingsList.show(0, META_FINDINGS_PAGE_SIZE)
			mp_req.metaFindingsList.sort('mf_res_nb', {order: 'desc'})
			document.getElementById('mf_total').onclick = evt => {
				mp_req.findingList.search()
				bold_clicked_a(evt.target, 'mf_name')
				mf_date_slicing()
				filter_last(document.getElementById('mf_all_res'), null)
			}
			mf_date_slicing()
			for (let sbm of document.getElementsByClassName('sidebar-module'))
				sbm.style.display='block'
			mp_req.findingList.update()
		}
		if (! mp_req.final_result_displayed && mp_req.running_query_countdown === 0)
			display_final_results()
	}
	var searchInMetaCallback_timeout
	function searchInMetaCallback (evt) {
		clearTimeout(searchInMetaCallback_timeout)
		searchInMetaCallback_timeout = setTimeout(() => {
			mp_req.metaFindingsList.search(µ.triw(evt.target.value), ["mf_name"])
		}, evt.keyCode === 13 ? 0 : 500)
	}
	function get_mf_res_nb () {
		let a = 0
		for (let r of mp_req.metaFindingsList.items) a += r.values().mf_res_nb
		return a
	}
	const result_attach_event = () => {
		for (let a of document.querySelectorAll(`[src="${img_load}"]`)) {
			a.removeEventListener('click', load_result_image)
			a.addEventListener('click', load_result_image, {once: true})
		}
		for (let a of document.getElementsByClassName('f_txt'))
			if (µ.isOverflown(a)) a.style.resize = 'vertical'
	}
	async function display_final_results() {
		if (test_src === '1') {
			let elt = opener.document.getElementById('mp_display_result_test')
			let cur_url = new URL(window.location)
			let all_names = cur_url.searchParams.getAll('name')
			let query = cur_url.searchParams.get('q')
			let opener_elt_test, json_str
			for(let name of all_names){
				mp_req.findingList.search(name, ['f_source_title'])
				json_str = JSON.parse(results_test_json(3))
				json_str = JSON.stringify(json_str)
				let new_result_json = JSON.parse(results_test_json())
				let old_result_json = await mµ.exec_in_background('get_test_src', [name])
				if (typeof(old_result_json) !== 'undefined' && old_result_json[query] && jsondiffpatch.diff(old_result_json, new_result_json)) {
					old_result_json = old_result_json[query]
					var json_diff = jsondiffpatch.diff(old_result_json.findings, new_result_json.findings)
					let txt, status
					if(json_diff) {
						if(json_diff['_0'] || json_diff['_1'] || json_diff['_2']) {
							txt = 'Lost article'
							status = 'warn'
						}
						if (json_diff[0]) {//Si il y a une différence sur le premier article
							if (json_diff[0].f_h1 &&
								(json_diff['_1'] || !json_diff[1] || json_diff[1].f_h1) &&
								(json_diff['_2'] || !json_diff[2] || json_diff[2].f_h1)) {//Si le titre est modifié pour tout les anciens article
								txt= 'All new articles'
								status = 'error'
								for(let i in Object.keys(new_result_json.findings)){
									if(Object.values(new_result_json.findings[i]).includes(json_diff[0].f_h1[0])) {//on vérifie si le titre de l'article est toujours présents dans les recherches
										txt= 'New article'//la différence est du a un nouvel article
										status = 'warn'
									}
								}
								if(txt) {add_err(txt,name,status,'test_diff')}
							}
							else{//sinon
								for(let i of Object.keys(json_diff)){//pour chaque article modifié
									for(let j of Object.keys(json_diff[i])){//on récupère la clé modifié
										if(DIFF_TEXT[j]) {//si la clé appartient au clé a retourner
											txt = DIFF_TEXT[j] + ` ${i}`//on l'ajoute au texte
											status = 'warn'
											add_err(txt,name,status,'test_diff')
										}
									}
								}
								status = 'error'
							}
						}
					}
				}
				mp_req.findingList.search()
				opener.document.getElementById('mp_filter_report').value = ''
				opener.document.getElementById('mp_search_filter_report').click()
				opener_elt_test = opener.document.getElementById(`report_${name}`).getElementsByClassName('test_diff')[0]
				opener_elt_test = opener_elt_test.querySelector(`[data-query='${query}']`)
				if(opener_elt_test === null){
					opener_elt_test = opener.document.createElement('div')
					opener_elt_test.setAttribute('data-query',query)
					opener.document.getElementById(`report_${name}`).getElementsByClassName('test_diff')[0].appendChild(opener_elt_test)
				}
				opener_elt_test.setAttribute('data-results', json_str)
				opener_elt_test.setAttribute('data-name', name)
			}
			let pool = parseInt(elt.getAttribute('data-pool')) + 1
			elt.setAttribute('data-pool', pool)
			self.close()
		} else {
			clearTimeout(mp_req.repeat_timeout_ongoing)
			mp_req.final_result_displayed = 1
			location.href = '#'; location.href = '#before_search_stats'
			document.getElementById('mp_running_stats').style.display = 'none'
			document.getElementById('mp_load_result').style.display = 'none'
			document.getElementById('search_stats').style.display = 'block'
			document.getElementById('date_range').style.display = 'flex'
			// document.getElementById('mp_sel_mod').style.display = ''
			let length = mp_req.findingList.size()
			if (length > 0) {
				document.getElementById('date_filter_sta')
					.setAttribute('min', mp_req.findingList.items[length - 1]._values.f_dt)
				let date_max
				if (length > 1)
					date_max = mp_req.findingList.items[1]._values.f_dt
				else
					date_max = mp_req.findingList.items[0]
				document.getElementById('date_filter_sto')
					.setAttribute('max', date_max)

			}
			if (µ.rnd(1) > 7)
				document.getElementById('donation_reminder').style.display = 'block'
			clearTimeout(mp_req.duration_ticker)
			if (0 === mp_req.findingList.size()) {
				document.getElementById('no_results').style.display = 'block'
			} else {
				document.getElementById('mp_page_sizes').style.display = 'block'
			}
			mp_req.findingList.on('updated', result_attach_event)
			mp_req.findingList.update()
			document.getElementById('mp_query_meta_total').textContent =
				get_mf_res_nb().toLocaleString(userLang)
			document.getElementById('mp_query_meta_local_total').textContent = mp_req.findingList.size()
			var mp_end_date = ndt()
			var req_duration = (mp_end_date - mp_req.query_start_date) / 1000
			document.getElementById('mp_duration').textContent = req_duration
			document.getElementById('mp_duration').title = mp_end_date.toLocaleString(userLang)
			document.getElementById('mp_query_search_terms').textContent = mp_req.search_terms
			document.getElementById('mp_src_nb').textContent =
				mp_req.metaFindingsList.items.filter(i => i.values().mf_res_nb > 0).length
			document.getElementById('mp_src_fetched').textContent = current_source_nb
			document.getElementById('mp_submit').disabled = false
			document.getElementById('mp_submit').style.cursor = 'pointer'
			document.getElementById('mp_stop').style.display = 'none'
			document.getElementsByTagName('title')[0].textContent = document.getElementById(
				'tab_title').textContent
			document.body.style.cursor = 'auto'
			let id_sch_s = cur_querystring.get('id_sch_s')
			if (req_duration > 8 && !id_sch_s) mµ.notifyUser(
				document.getElementById('tab_title').textContent,
				µ.trim(document.getElementById('search_stats').textContent).replace(' 🔗', '')
			)
			if (await mµ.get_stored_keep_host_perm() === '0')
				mµ.drop_host_perm()
			if (id_sch_s)
				add_elt_url(id_sch_s)
			// search source in meta finding
			if (mp_req.metaFindingsList.size() > 30)
				document.getElementById('meta_search').style.display = 'flex'
			document.getElementById('mp_meta_search').addEventListener("keyup", searchInMetaCallback)
			document.getElementById('mp_clear_search').onclick = evt => {
				document.getElementById('mp_meta_search').value = ""
			}
			document.getElementById('mp_search_permalink').href = µ.rm_href_anchor(
				gen_permalink(true))
			document.dispatchEvent(new Event('result_load'))
		}
	}
	const DIFF_TEXT = {
		'f_h1': 'New title for article',
		'f_dt': 'New date for article',
		'f_by': 'New author for article',
		'f_txt': 'New text description for article',
		'f_url': 'New url for article'
	}
	function clear_results() {
		mp_req.final_result_displayed = 0
		document.getElementById('no_results').style.display = 'none'
		document.getElementById('mp_page_sizes').style.display = 'none'
		document.getElementById('mp_query_meta_total').textContent = '…'
		document.getElementById('mp_query_meta_local_total').textContent = '…'
		document.getElementById('mp_running_stats').style.display = 'none'
		document.getElementById('mp_query_countdown').textContent = '…'
		document.getElementById('mp_running_src_fetched').textContent = '…'
		document.getElementById('mp_duration').textContent = '…'
		document.getElementById('mp_query_search_terms').textContent = '…'
		document.getElementById('mp_src_nb').textContent = '…'
		document.getElementById('mp_src_fetched').textContent = '…'
		document.getElementById('mp_duration').textContent = '…'
		document.getElementById('meta_search').style.display='none'
		document.getElementById('clear_date').click()
		document.getElementById('mp_clear_filter').click()
		if ('none' !== document.getElementById('mp_sel_all').style.display)
			document.getElementById('mp_sel_mod').click()

		// document.getElementById('mp_sel_mod').style.display = 'none'
		for (let sbm of document.getElementsByClassName('sidebar-module'))
			sbm.style.display='none'
		mp_req.findingList.clear()
		mp_req.metaFindingsList.clear()
		let update_event = mp_req.findingList.handlers.updated
		mp_req.findingList.handlers.updated = µ.remove_array_value(update_event, result_attach_event)
	}
	document.getElementById('mp_autosearch_permalink').addEventListener('click', () => {
		var sch_sea = gen_permalink(false)
		sch_sea = sch_sea.toString() // Chrome doesn't support URL in browser.storage elt
		browser.storage.sync.set({new_sch_sea: sch_sea})
	})
	document.getElementById('mp_stop').addEventListener('click', () => {
		controller.abort() 
	})
	function reload_keeping_query(submit) {
		// location.reload()
		window.location = µ.rm_href_anchor(gen_permalink(submit))
	}
	/**
	 * Generate an URL who caontain all the sear settings.
	 * @param {boolean} is_submit if true add autosubmit 
	 */
	function gen_permalink(is_submit) {
		let permalink = new URL(window.location)
		permalink.searchParams.set('q', document.getElementById('mp_query').value)
		for (let t of Object.keys(tag_select_multiple)) {
			permalink.searchParams.delete(t)
			if ('remove_source_selection' === t || 'add_source_selection' === t) continue
			for (let v of tag_select_multiple[t].getValue(true))
				permalink.searchParams.append(t, v)
		}
		if (tag_select_multiple.remove_source_selection.length > 0) {
			permalink.searchParams.delete('rm_src_select')
			for (const i of tag_select_multiple.remove_source_selection)
				permalink.searchParams.append('rm_src_select', i)
		}
		if (add_source_selection.length > 0) {
			permalink.searchParams.delete('rm_src_select')
			for (const i of add_source_selection)
				permalink.searchParams.append('add_src_select', i)
		}
		if (is_submit)
			permalink.searchParams.set('submit', 1)
		else
			permalink.searchParams.delete('submit')
		return permalink
	}
	const HEADLINES_H_RATIO = 21
	const HEADLINES_H_CONST = 64
	const headlines_div = document.getElementById('headlines')
	for (let pg_size of [5, 10, 20, 50])
		document.getElementById(`mp_headlines_page_${pg_size}`).addEventListener('click', () => {
			HEADLINE_PAGE_SIZE=pg_size
			headlineList.show(0, HEADLINE_PAGE_SIZE)
			let hdl_shown_nb = Math.min(pg_size, headlineList.visibleItems.length)
			headlines_div.style.height = `${hdl_shown_nb * HEADLINES_H_RATIO + HEADLINES_H_CONST}px`
		})
	document.getElementById('mp_headlines_page_all').addEventListener('click', () => {
		HEADLINE_PAGE_SIZE=headlineList.size()
		headlineList.show(0, HEADLINE_PAGE_SIZE)
		headlines_div.style.height = `${
			HEADLINE_PAGE_SIZE * HEADLINES_H_RATIO + HEADLINES_H_CONST}px`
	})
	for (let pg_size of [10, 20, 50])
		document.getElementById(`mp_page_${pg_size}`).addEventListener('click', () => {
			mp_req.findingList.show(0, pg_size)})
	document.getElementById('mp_page_all').addEventListener('click', () => {
		mp_req.findingList.show(0, mp_req.findingList.size())})

	// import rss atom json csv
	document.getElementById('mp_import_RSS').addEventListener('click', e=>import_XML(e, 'RSS'))
	document.getElementById('mp_import_ATOM').addEventListener('click',e=>import_XML(e,'ATOM'))
	document.getElementById('mp_import_CSV').addEventListener('click', e=>import_CSV(e))
	document.getElementById('mp_import_JSON').addEventListener('click',e=>import_JSON(e))
	function import_CSV(evt) {
		mp_req.query_start_date = ndt()
		mµ.file_open_treat(function (evt) {
			try {
				let input_csv = evt.target.result
				let mf = {}
				input_csv = input_csv.split(/",\n/)
				input_csv.splice(0, 1)
				for (let i = input_csv.length; i--;) {
					input_csv[i] = input_csv[i].split('","')
					for (let j = input_csv[i].length; j--;) {
							input_csv[i][j] = input_csv[i][j].replace(/^\"|\"$/, '')
							input_csv[i][j] = µ.drop_escaped_quote_file(input_csv[i][j])
					}
				}
				tags_fromCSV(input_csv[0])
				mp_req.search_terms = input_csv[0][18]
				for (const i of input_csv) {
					if (i.length === 10) {
						let f_h1 = i[4]
						let f_url = i[3]
						let f_icon = i[0]
						let f_img_src = i[8]
						let f_img_descr = i[9]
						let f_source = i[1]
						let f_src_key = i[2]
						let r_txt = i[6]
						let r_by = i[7]
						let r_dt = new Date(i[5])
						let item_obj = {
							f_h1: f_h1,
							f_h1_title: f_h1,
							f_url: f_url,
							f_icon: f_icon,
							f_img_src: f_img_src,
							f_img_alt: f_img_descr,
							f_img_title: f_img_descr,
							mp_data_img: f_img_src,
							mp_data_alt: f_img_descr,
							mp_data_title: f_img_descr,
							mp_data_key: f_src_key,
							f_txt: r_txt,
							f_by: r_by || f_source,
							f_by_title: r_by,
							f_dt: r_dt.toISOString().slice(0, -14),
							f_dt_title: r_dt.toLocaleString(userLang),
							f_epoc: r_dt.valueOf(),
							f_ISO_dt: r_dt.toISOString(),
							f_source: f_source,
							f_source_title: f_source
						}
						mp_req.findingList.add(item_obj)
						let n = sources_objs[f_src_key]
						let url = ''
						if (typeof(n) === 'undefined')
							url = f_src_key
						else
							url = n && format_search_url(n.search_url_web || n.search_url, mp_req.search_terms)
						if (typeof (mf[f_source]) === 'undefined') {
							mf[f_source] = {
								mf_icon: f_icon,
								mf_name: f_source,
								mf_title: mf_title(f_source),
								mf_ext_link: url,
								mf_remove_source: f_source,
								mf_res_nb: 0
							}
						}
						mf[f_source].mf_res_nb += 1
						mf[f_source].mf_locale_res_nb = mf[f_source].mf_res_nb.toLocaleString(userLang)
					}
				}
				let v
				for (let k of Object.keys(mf)) {
					v = mf[k]
					mp_req.metaFindingsList.add(v)
				}
				
				console.log("Import CSV")
				display_ongoing_results()
				document.body.style.cursor = 'auto'
			} catch (exec) {
				console.error("CSV loading failure", exec)
				document.body.style.cursor = 'auto'
			}
		})
	}
	function import_JSON(evt) {
		mp_req.query_start_date = ndt()
		mµ.file_open_treat(function (evt) {
			try {
				let input_json = JSON.parse(evt.target.result)
				tag_select_multiple.remove_source_selection = input_json.remove_source_selection
				add_source_selection = input_json.add_source_selection
				tags_fromJSON(input_json.filters)
				clear_results()
				mp_req.findingList.add(input_json.findings)
				mp_req.metaFindingsList.add(input_json.meta_findings)
				mp_req.search_terms = input_json.search_terms
				document.getElementById('mp_query').value = input_json.search_terms
				console.log("Import JSON")
				display_ongoing_results()
				document.body.style.cursor = 'auto'
			} catch (exc) {
				console.error("JSON loading failure", exc)
				document.body.style.cursor = 'auto'
			}
		})
	}
	function import_XML(evt, flux_type) {
		if (document.getElementById('tags_row').style.display === 'block')
			document.getElementById('tags_handle').click()
		mp_req.query_start_date = ndt()
		mµ.file_open_treat(function (evt) {
			try {
				let input_xml = µ.dom_parser.parseFromString(evt.target.result, 'application/xml')
				clear_results()
				let r_src, n, r_dt, r_by, f_source, f_icon, item_obj, f_h1, f_url, l_fmp
				let mf = {}
				let p = xml_src[flux_type] // parser info
				tags_fromXML(input_xml.querySelectorAll(p.filters), flux_type)
				mp_req.search_terms = input_xml.querySelector('title').textContent.replace(
					'Meta-Press.es : ', '')
				document.getElementById('mp_query').value = mp_req.search_terms
				let is_mp_RSS = false
				let generator = input_xml.getElementsByTagName('generator')[0]
				if (typeof generator !== 'undefined') {
					generator = generator.textContent
					if (typeof generator === 'string')
						is_mp_RSS = generator.startsWith('Meta-Press.es')
				}
				for (let item of input_xml.querySelectorAll(p.results)) {
					l_fmp = {
						'f_img_src': '', 'f_img_alt': '', 'f_img_title': '', 'mp_data_img': '',
						'mp_data_alt': '', 'mp_data_title': '', 'f_txt': '', 'mp_data_key': ''
					}
					f_url = $_('r_url', item, p)
					r_src = µ.domain_part(f_url)
					n = sources_objs[r_src]
					l_fmp['f_txt'] = $_('r_txt', item, p)
					if (!is_mp_RSS) {  // it's a wild RSS
						// First we sanitize the input
						l_fmp = separate_txt_img(l_fmp, '', { domain_part: r_src, type: 'XML' })
						if (typeof n !== 'undefined' && n.type === 'XML') {  // we might know better
							p = n
							if (typeof p.r_img_src !== 'undefined')
								l_fmp['f_img_src'] = $_('r_img_src', item, p)
							if (typeof p.r_img_alt !== 'undefined')
								l_fmp['f_img_alt'] = $_('r_img_alt', item, p)
							if (typeof p.r_img_title !== 'undefined')
								l_fmp['f_img_title'] = $_('r_img_title', item, p)
						}
					} else {  // it's our format
						l_fmp = separate_txt_img(l_fmp, '', { domain_part: r_src, type: 'XML' })
						if ('RSS' === flux_type)
							l_fmp['mp_data_key'] = µ.evaluateXPath(item, './dc:identifier').textContent
						else
							l_fmp['mp_data_key'] = item.querySelector('contributor > uri').textContent
						n = sources_objs[l_fmp['mp_data_key']]
					} 
					f_h1 = $_('r_h1', item, p)
					r_dt = new Date($_('r_dt', item, p))
					r_by = $_('r_by', item, p)
					f_icon = n && n.favicon_url || '/img/Feed-icon.svg'
					//console.log(n.favicon_url)
					f_source = n && n.tags.name || µ.domain_name(f_url) || ''
					item_obj = {
						f_h1: f_h1,
						f_h1_title: f_h1,
						f_url: f_url,
						f_icon: f_icon,
						f_img_src: l_fmp['f_img_src'],
						f_img_alt: l_fmp['f_img_alt'],
						f_img_title: l_fmp['f_img_title'],
						mp_data_img: l_fmp['mp_data_img'],
						mp_data_alt: l_fmp['mp_data_alt'],
						mp_data_title: l_fmp['mp_data_title'],
						mp_data_key: l_fmp['mp_data_key'],
						f_txt: l_fmp['f_txt'],
						f_by: r_by || f_source,
						f_by_title: r_by,
						f_dt: r_dt.toISOString().slice(0, -14),
						f_dt_title: r_dt.toLocaleString(userLang),
						f_epoc: r_dt.valueOf(),
						f_ISO_dt: r_dt.toISOString(),
						f_source: f_source,
						f_source_title: f_source
					}
					mp_req.findingList.add(item_obj)
					if (typeof (mf[f_source]) === 'undefined') {
						mf[f_source] = {
							mf_icon: f_icon,
							mf_name: f_source,
							mf_title: mf_title(f_source),
							mf_ext_link: n && format_search_url(n.search_url_web || n.search_url,
								mp_req.search_terms) || '',
							mf_remove_source: f_source,
							mf_res_nb: 0
						}
					}
					mf[f_source].mf_res_nb += 1
					mf[f_source].mf_locale_res_nb = mf[f_source].mf_res_nb.toLocaleString(userLang)
				}
				let v
				for (let k of Object.keys(mf)) {
					v = mf[k]
					mp_req.metaFindingsList.add(v)
				}
				document.body.style.cursor = 'auto'
				console.log("Import XML")
				display_ongoing_results()
			} catch (exc) {
				document.body.style.cursor = 'auto'
				console.error(flux_type, "loading failure", r_src, exc)
			}
		})
	}
	function mf_title(src_name) {
		return `${mp_i18n.gettext('Filter results of')} '${src_name}' ${mp_i18n.gettext('only')}`
	}
	document.getElementById('mp_sel_mod').addEventListener('click', () => {
		let cur_sel_all = document.getElementById('mp_sel_all') 
		let cur_unsel_all = document.getElementById('mp_unsel_all') 
		let cur_sel_inverse = document.getElementById('mp_sel_inverse') 
		cur_sel_all.style.display = cur_sel_all.style.display === "none" ? "inline" : "none"
		cur_unsel_all.style.display = cur_unsel_all.style.display === "none" ? "inline" : "none"
		cur_sel_inverse.style.display = cur_sel_inverse.style.display === "none" ? "inline" : "none"
		let cur_sel_mod = dynamic_css.rules[CSS_CHECKBOX_RULE_INDEX].cssText.match(
			/(block|none)/)[0]
		let next_sel_mod = cur_sel_mod === 'none' ? 'block' : 'none'
		document.getElementsByClassName('mp_export_choices')[0].style.display = next_sel_mod
		dynamic_css.deleteRule(CSS_CHECKBOX_RULE_INDEX)
		dynamic_css.insertRule(
			`.f_selection {display:${next_sel_mod}!important}`, CSS_CHECKBOX_RULE_INDEX)
	})
	document.getElementById('mp_sel_all').addEventListener('click', () => { 
		for (let item of mp_req.findingList.visibleItems) {
			let check = item.elm.getElementsByClassName('f_selection')[0]
			check.checked = true
		}
	})
	document.getElementById('mp_unsel_all').addEventListener('click', () => { 
		for (let item of mp_req.findingList.visibleItems) {
			let check = item.elm.getElementsByClassName('f_selection')[0]
			check.checked = false
		}
	})
	document.getElementById('mp_sel_inverse').addEventListener('click', () => { 
		for (let item of mp_req.findingList.visibleItems) {
			let check = item.elm.getElementsByClassName('f_selection')[0]
			check.checked = check.checked === false
		}
	})
	/**
	 * Export the curent source selection json file.
	 */
	function export_JSON() {
		let cur_sel_items = []
		let rm_selec_src = ''
		let add_selec_src = ''
		for (const i of tag_select_multiple.remove_source_selection)
			rm_selec_src += `"${i}",`
		rm_selec_src = rm_selec_src.substring(0, rm_selec_src.length - 1); // rm last ,
		for (const i of add_source_selection)
			add_selec_src += `"${i}",`
		add_selec_src = add_selec_src.substring(0, add_selec_src.length - 1); // rm last ,
		for (let i of mp_req.findingList.matchingItems) 
			cur_sel_items.push(i.values())
		let json_str = `{
			"filters": ${JSON.stringify(tags_toJSON())},
			"findings": ${JSON.stringify(cur_sel_items)},
			"remove_source_selection": [${rm_selec_src}],
			"add_source_selection": [${add_selec_src}],
			"meta_findings": ${JSON.stringify(mp_req.metaFindingsList.toJSON())},
			"search_terms": ${JSON.stringify(document.getElementById('mp_query').value)}
		}`
		µ.upload_file_to_user(`${µ.ndt_human_readable()}_meta-press.es.json`, json_str)
	}
	/**
	 * Export the curent source selection csv file.
	 */
	function export_CSV() {
		let csv_file = `"Favicon","Source","Source_key","Url","title","ISO_date","txt","by","img_src","img_alt",`
			+ `"q_type","q_lang","q_res_type","q_themes","q_tech","q_country","q_name","q_excluded","q_included","q_search",\n`
		let tags_1st_line = false
		for (const i of mp_req.findingList.matchingItems) {
			let v = i.values()
			csv_file += `"${µ.preg_quote_file(v.f_icon).normalize()}",`
				+ `"${µ.preg_quote_file(v.f_source).normalize()}",`
			if (typeof(mp_data_key) !== 'undefined') 
				csv_file += `"${µ.preg_quote_file(v.mp_data_key).normalize()}",`
			csv_file += `"${µ.preg_quote_file(v.f_url).normalize()}",`
				+ `"${µ.preg_quote_file(v.f_h1).normalize()}",`
				+ `"${µ.preg_quote_file(v.f_ISO_dt).normalize()}",`
				+ `"${µ.preg_quote_file(v.f_txt).normalize()}",`
				+ `"${µ.preg_quote_file(v.f_by).normalize()}",`
			if (v.f_img_src)
				if (v.f_img_alt === alt_load_txt) {
					let img = v.mp_data_img || ''
					let alt = v.mp_data_alt || ''
					csv_file += `"${µ.preg_quote_file(img).normalize()}",`
						+ `"${µ.preg_quote_file(alt).normalize()}",`
				} else
					csv_file += `"${µ.preg_quote_file(v.f_img_src).normalize()}",`
						+ `"${µ.preg_quote_file(v.f_img_alt).normalize()}",`
			else
				csv_file += `"","",`
			if (!tags_1st_line) {
				let rm_src = tag_select_multiple.remove_source_selection.toString() || ''
				let add_src = add_source_selection.toString() || ''
				csv_file += `"${µ.preg_quote_file(tag_select_multiple.src_type.getValue(true).toString()).normalize()}",`
					+ `"${µ.preg_quote_file(tag_select_multiple.lang.getValue(true).toString()).normalize()}",`
					+ `"${µ.preg_quote_file(tag_select_multiple.res_type.getValue(true).toString()).normalize()}",`
					+ `"${µ.preg_quote_file(tag_select_multiple.themes.getValue(true).toString()).normalize()}",`
					+ `"${µ.preg_quote_file(tag_select_multiple.tech.getValue(true).toString()).normalize()}",`
					+ `"${µ.preg_quote_file(tag_select_multiple.country.getValue(true).toString()).normalize()}",`
					+ `"${µ.preg_quote_file(tag_select_multiple.name.getValue(true).toString()).normalize()}",`
					+ `"${µ.preg_quote_file(rm_src).normalize()}",`
					+ `"${µ.preg_quote_file(add_src).normalize()}",`
					+ `"${µ.preg_quote_file(mp_req.search_terms).normalize()}",`
				tags_1st_line = true
			}
			csv_file += "\n"
		}
		µ.upload_file_to_user(`${µ.ndt_human_readable()}_meta-press.es${manifest.version}.csv`, csv_file)
	}
	function export_RSS() {
		document.body.style.cursor = 'wait'
		// https://validator.w3.org/feed/#validate_by_input
		let flux_items = '', v = {}, img=''
		for (let i of mp_req.findingList.matchingItems) {
			v = i.values()
			if (v.f_img_src) {
				if (v.f_img_alt === alt_load_txt) {
					img = mµ.img_tag(v.mp_data_img, v.mp_data_alt, v.mp_data_title)
				} else {
					img = mµ.img_tag(v.f_img_src, v.f_img_alt, v.f_img_title)
				}
			} else { img = '' }
			flux_items += `<item>
				<title>${µ.encode_XML(title_line(v))}</title>
				<description>${µ.encode_XML(img)}${µ.encode_XML(v.f_txt)}</description>
				<link>${µ.encode_XML(encodeURI(v.f_url))}</link>
				<pubDate>${new Date(v.f_ISO_dt).toUTCString()}</pubDate>
				<dc:creator>${µ.encode_XML(v.f_by)}</dc:creator>
				<guid>${µ.encode_XML(encodeURI(v.f_url))}</guid>
				<dc:identifier>${v.mp_data_key}</dc:identifier>
			</item>`
		}
		let flux = `<?xml version='1.0' encoding='UTF-8'?>
		<rss version='2.0' xmlns:dc='http://purl.org/dc/elements/1.1/'>
			<channel>
				<title>Meta-Press.es : ${document.getElementById('mp_query').value}</title>
				<description>Selection RSS 2.0</description>
				<link>https://www.meta-press.es</link>
				<lastBuildDate>${ndt().toUTCString()}</lastBuildDate>
				<language>${tag_select_multiple.lang.getValue(true) || userLang}</language>
				<generator>Meta-Press.es (v${manifest.version})</generator>
				<docs>http://www.rssboard.org/rss-specification</docs>
				${tags_toXML('RSS')}
				${flux_items}
			</channel>
		</rss>`
		document.body.style.cursor = 'auto'
		µ.upload_file_to_user(`${µ.ndt_human_readable()}_meta-press.es${manifest.version}.rss`, flux)
	}
	function export_ATOM() {
		// https://tools.ietf.org/html/rfc4287
		// https://validator.w3.org/feed/#validate_by_input
		let flux_items = '', v = {}, img=''
		for (let i of mp_req.findingList.matchingItems) {
			v = i.values()
			if (v.f_img_src)
				if (v.f_img_alt === alt_load_txt)
					img = mµ.img_tag(v.mp_data_img, v.mp_data_alt, v.mp_data_title)
				else
					img = mµ.img_tag(v.f_img_src, v.f_img_alt, v.f_img_title)
			else img = ''
			flux_items += `<entry>
				<title>${µ.encode_XML(title_line(v))}</title>
				<summary type='html'>${µ.encode_XML(img)}${µ.encode_XML(v.f_txt)}</summary>
				<published>${v.f_ISO_dt}</published>
				<updated>${v.f_ISO_dt}</updated>
				<link href='${µ.encode_XML(encodeURI(v.f_url))}'/>
				<author><name>${µ.encode_XML(v.f_by)}</name></author>
				<contributor>
					<name>${µ.encode_XML(v.f_source_title)}</name>
					<uri>${µ.encode_XML(v.mp_data_key)}</uri>
				</contributor>
				<id>${µ.encode_XML(encodeURI(v.f_url))}</id>
			</entry>`
		}
		let flux = `<?xml version='1.0' encoding='UTF-8'?>
		<feed xmlns='http://www.w3.org/2005/Atom'>
			<title>Meta-Press.es : ${document.getElementById('mp_query').value}</title>
			<subtitle>Decentralize press search engine</subtitle>
			<link href='https://www.meta-press.es'/>
			<generator uri='https://www.meta-press.es'>Meta-Press.es (v${manifest.version})</generator>
			<updated>${ndt().toISOString()}</updated>
			<author><name>Meta-Press.es</name></author>
			<id>https://www.meta-press.es/</id>
			${tags_toXML('ATOM')}
			${flux_items}
		</feed>`
		µ.upload_file_to_user(`${µ.ndt_human_readable()}_meta-press.es${manifest.version}.atom`, flux)
	}
	// select export
	document.getElementById('mp_export_sel_CSV').addEventListener('click', () => {
		mp_req.findingList.filter(i => {
		if (i.elm) return i.elm.getElementsByClassName('f_selection')[0].checked})
		export_CSV()
		mp_req.findingList.filter()
	})
	document.getElementById('mp_export_sel_JSON').addEventListener('click', () => {
		mp_req.findingList.filter(i => {
		if (i.elm) return i.elm.getElementsByClassName('f_selection')[0].checked})
		export_JSON()
		mp_req.findingList.filter()
	})
	document.getElementById('mp_export_sel_ATOM').addEventListener('click', () => {
		mp_req.findingList.filter(i => {
		if (i.elm) 
		return i.elm.getElementsByClassName('f_selection')[0].checked})
		export_ATOM()
		mp_req.findingList.filter()
	})
	document.getElementById('mp_export_sel_RSS').addEventListener('click', () => {
		mp_req.findingList.filter(i => {
		if (i.elm) return i.elm.getElementsByClassName('f_selection')[0].checked})
		export_RSS()
		mp_req.findingList.filter()
	})
	// export all
	document.getElementById('mp_export_search_RSS').addEventListener('click', () => {
		export_RSS()
	})
	document.getElementById('mp_export_search_ATOM').addEventListener('click', () => {
		export_ATOM()
	})
	document.getElementById('mp_export_search_JSON').addEventListener('click', () => {
		export_JSON()
	})
	document.getElementById('mp_export_search_CSV').addEventListener('click', () => {
		export_CSV()
	})
	
	function title_line(v) {
		if (typeof v.f_h1 !== 'string') return v.f_source
		var src_prefix = `[${v.f_source}]`
		return v.f_h1.indexOf(src_prefix) !== -1 ? `${src_prefix} ${v.f_h1}` : `${v.f_h1}`
	}
	function ndt() { return new Date() }
	const now = ndt()
	var date_filters = [
		function mf_last_day(r)  {return r.values().f_epoc>ndt().setDate(now.getDate()-1)},
		function mf_last_2d(r)	 {return r.values().f_epoc>ndt().setDate(now.getDate()-2)},
		function mf_last_3d(r)	 {return r.values().f_epoc>ndt().setDate(now.getDate()-3)},
		function mf_last_week(r) {return r.values().f_epoc>ndt().setDate(now.getDate()-7)},
		function mf_last_2w(r)	 {return r.values().f_epoc>ndt().setDate(now.getDate()-14)},
		function mf_last_month(r){return r.values().f_epoc>ndt().setMonth(now.getMonth()-1)},
		function mf_last_2m(r){return r.values().f_epoc>ndt().setMonth(now.getMonth()-2)},
		function mf_last_6m(r){return r.values().f_epoc>ndt().setMonth(now.getMonth()-6)},
		function mf_last_1y(r){return r.values().f_epoc>ndt().setFullYear(now.getFullYear()-1)},
		function mf_last_2y(r){return r.values().f_epoc>ndt().setFullYear(now.getFullYear()-2)},
		function mf_last_5y(r){return r.values().f_epoc>ndt().setFullYear(now.getFullYear()-5)},
		function mf_last_dk(r){return r.values().f_epoc>ndt().setFullYear(now.getFullYear()-10)}
	]
	for (let flt of date_filters)
		document.getElementById(flt.name).addEventListener('click',
			evt => filter_last(evt.target, flt))
	document.getElementById('mf_all_res').addEventListener('click',
		evt => filter_last(evt.target, null))
	function mf_date_slicing() {
		let prev_nb = 0
		let total_nb = set_filter_nb(null, 'mf_all_res', 0, 0)
		for (let flt of date_filters)
			prev_nb = set_filter_nb(flt, flt.name, prev_nb, total_nb)
		mp_req.findingList.filter()
	}
	function set_filter_nb(fct, id, prev_nb, total_nb) {
		mp_req.findingList.filter()
		if(fct) mp_req.findingList.filter(fct)
		let nb = mp_req.findingList.matchingItems.length
		if (nb > 0 && nb > prev_nb + (total_nb * 0.06)) {
			let p = Math.floor(nb * 40 / mp_req.findingList.size())
			document.getElementById(id+'_nb').textContent =
				`${nb.toLocaleString(userLang)} ${'.'.repeat(p)}`
			document.getElementById(id).parentNode.style.display='flex'
		} else {
			document.getElementById(id).parentNode.style.display='none'
		}
		return nb
	}
	/**
	 * Filter the result in the starting date.
	 * @param {*} target 
	 * @param {Function} fct the filter function 
	 */
	function filter_last(target, fct) {
		mp_req.findingList.filter()
		if(fct) mp_req.findingList.filter(fct)
		bold_clicked_a(target, 'mf_dt_lbl')
	}
	/**
	 * Add the given class to the target and remove the other user of this class.
	 * @param {Node} target target
	 * @param {string} a_class class to add
	 */
	function bold_clicked_a(target, a_class) {
		for (let i of document.getElementsByClassName(a_class))
			i.classList.remove('bold')
		target.classList.add('bold')
	}
	document.getElementById('mp_filter').addEventListener('keyup', searchInResCallback)
	document.getElementById('mp_clear_filter').addEventListener('click', () => {
		document.getElementById('mp_filter').value = ''
		searchInResCallback({target: {value: ''}})
	})
	var searchInResCallback_timeout
	function searchInResCallback (evt) {
		clearTimeout(searchInResCallback_timeout)
		searchInResCallback_timeout = setTimeout(() => {
			if (mp_req.findingList.searched) { document.getElementById('mf_total').click() }
			if (mp_req.findingList.filtered) {filter_last(document.getElementById('mf_all_res'),
				null) }
			if (evt.target.value) {
				mp_req.findingList.search(evt.target.value, ['f_h1', 'f_source', 'f_dt', 'f_by',
					'f_txt'])
			} else {
				mp_req.findingList.search()
			}
			mf_date_slicing()
		}, evt.keyCode === 13 ? 0 : 500)
	}
	function format_search_url(search_url, token) {
		return search_url.replace('{}', token)
			.replace('{#}', MAX_RES_BY_SRC)
			.replace('{+}', token.replace(' ', '+'))
		//		.replace('{%}', encodeURIComponent(token))
	}
	function $_(elt, dom, src, f_nb=0) {	// parsing lookup
		let val = ''
		let src_elt = src[elt]
		// console.log('src_elt', src_elt)
		if (src.type === 'JSON' && elt !== 'headline_selector' && elt !== 'h_title') {
			if (typeof(src_elt) === 'undefined') return ''  // we need undef src_elt to seek _xpath
			if (µ.is_array(src_elt)) {
				try {
					let val_lst = []
					for (let val_idx of src_elt)
						val_lst.push(µ.deref_json_path(val_idx, dom) || '')
					val = µ.str_fmt(src[`${elt}_tpl`], val_lst)
				} catch (exc) { add_err(`${f_nb} - ${elt} ${exc}`,src.tags.name,'warn', 'test_src') }
			} else {
				try {
						/*if (elt_opt.includes(elt)) 
							val = µ.deref_json_path(src_elt, dom, true)
						else*/
					val = µ.deref_json_path(src_elt, dom)
					if (elt !== 'results' && µ.is_array(val)) {	// if wanted elt 'results' return them
						let val_acc = ''
						let val_attr = src[`${elt}_attr`]
						for (let v of val) {
							if (typeof(val_attr) === 'string')
								val_acc += `${v[val_attr]}, `
							else
								val_acc += v
						}
						val = val_acc
					}
				} catch (exc) { add_err(`${f_nb} - ${elt} ${exc}`,src.tags.name,'warn', 'test_src') }
			}
		} else {
			if (typeof(src_elt) === 'string') {
				val = dom.querySelector(src_elt)
			} else if (µ.is_array(src_elt)) {
				try {
					let val_lst = []
					let val_attr = src[`${elt}_attr`]
					for (let val_idx of src_elt) {
						val_lst.push(dom.querySelector(val_idx))
						if (µ.is_array(val_attr) && val_attr[val_lst.length-1]) {
							val_lst[val_lst.length-1] = val_lst[val_lst.length-1].getAttribute(
								val_attr[val_lst.length-1])
						} else {
							val_lst[val_lst.length-1] = µ.triw(val_lst[val_lst.length-1].textContent)
						}
					}
					val = µ.str_fmt(src[`${elt}_tpl`], val_lst)
				} catch (exc) { add_err(`${f_nb} - ${elt} ${exc}`,src.tags.name,'warn', 'test_src') }
			} else {
				let elt_xpath = src[`${elt}_xpath`]
				if (typeof(elt_xpath) === 'string') {
					val = µ.evaluateXPath(dom, elt_xpath)
				} else { // console.error(
					return '' // `Missing ${elt} (or ${elt}_xpath) in ${src.tags.name}, or not string.`)
				}
			}
			var src_elt_attr = src[`${elt}_attr`]
			if (typeof (src_elt_attr) === 'string') {
				try {
					val = val.getAttribute(src_elt_attr)
				} catch (exc) {
					val = ''
					add_err(`${f_nb} - ${elt} ${src_elt_attr} ${val} ${exc}`,src.tags.name,'warn', 'test_src')
				}
			// } else if (! µ.is_array(src_elt)) { // is it possible to be an array here ?
			// } else if (typeof(src_elt) !== 'object') { // this prevents XPath objects to treated
			} else {  // is it possible to be an array here ?
				try {
					val = µ.triw(val.textContent)
				} catch (exc) {
					val = (elt === 'r_dt') ? '100000000000' : ''
					// it's an old date : 1973-03-03 09:46:40 GMT+0000
					add_err(`${f_nb} - ${elt} ${exc}`,src.tags.name,'warn', 'test_src')
				}
			}
		}
		// if (elt == 'r_img_src') console.log('before', val)
		var elt_re = src[`${elt}_re`]  // it should be a list of 2 elements
		if (typeof (elt_re) === 'object') {
			if(typeof (val) === 'string') {
				val = µ.regextract(elt_re[0], val, elt_re[1])
			} else {
				let val_type = typeof (val)
				val = String(val)
				val = µ.regextract(elt_re[0], val, elt_re[1])
				if(val_type === 'number') { val = Number(val) }
			}
		}
		// console.log(val)
		return val
	}
	/* * * Headlines * * */
	var headlineList = new List('headlines', {
		item: 'headline-item',
		valueNames: [
			'h_title',
			{ name: 'h_url',	attr: 'href' },
			{ name: 'h_icon',	attr: 'src' },
			{ name: 'h_tip',	attr: 'title' },
		],
		page: HEADLINE_PAGE_SIZE,
		pagination: {
			outerWindow: 10,
			innerWindow: 10
		}
	})
	var headline_item = 1
	var headline_timeout, headline_fadeout_timeout
	function rotate_headlines_timeout() {
		headline_timeout = setTimeout(() => {
			if (headlineList.size() > HEADLINE_PAGE_SIZE) {
				headline_item = (headline_item + HEADLINE_PAGE_SIZE) % headlineList.size()
				headlineList.show(headline_item, HEADLINE_PAGE_SIZE)
				rotate_headlines()
			}
		}, 10000)
	}
	function rotate_headlines() {
		clearTimeout(headline_fadeout_timeout)
		rotate_headlines_timeout()
		fadeout_pagination_dot('#headlines', 'rgba(0, 208, 192, 1)', 1)
	}
	function fadeout_pagination_dot(id, bg_color, dot_opacity) {
		headline_fadeout_timeout = setTimeout(() => {
			var dot = document.querySelector(`${id} ul.pagination li.active a.page`)
			dot_opacity -= 0.1
			dot.style.backgroundColor = bg_color.replace(/, 1\)/, `, ${dot_opacity})`)
			fadeout_pagination_dot(id, bg_color, dot_opacity)
		}, 1000)
	}
	document.getElementById('headlines').addEventListener('mouseover', () => {
		clearTimeout(headline_timeout)
		clearTimeout(headline_fadeout_timeout)
	})
	document.getElementById('headlines').addEventListener('mouseout', async () => {
		if (await mµ.get_stored_headline_loading() === '1')
			rotate_headlines()
	})
	let set_hdl = new Set()
	document.getElementById('load_headlines').addEventListener('click', async () => {
		set_hdl.clear()
		let headline_host = mµ.get_current_source_headline_hosts(
			current_source_selection, sources_objs)
		if (! await mµ.request_permissions(headline_host)) { alert_perm() }
		load_headlines()
	})
	document.getElementById('reload_headlines').addEventListener('click', async () => {
		set_hdl.clear()
		let headline_host = mµ.get_current_source_headline_hosts(
			current_source_selection, sources_objs)
		if (! await mµ.request_permissions(headline_host)) { alert_perm() }
		load_headlines()
	})
	document.getElementById('remove_headlines').addEventListener('click', async () => {
		headlineList.clear()
		document.getElementById('headlines_wrapper').style.display = 'none'
	})
	async function load_headlines() {
		document.getElementById('headlines_wrapper').style.display = 'block'
		headlineList.clear()
		let i = 0
		for (let h of document.querySelectorAll('.mp_hl'))
			headlineList.add({
				h_title: mp_i18n.gettext(h.innerHTML),
				h_url: h.href,
				h_icon: "./img/favicon-metapress-v2.png",
				h_tip: mp_i18n.gettext(h.getAttribute('title'))
			})
		// headlineList.items.slice(-1)[0].values({h_icon:"./img/favicon-metapress-active.png"})
		document.querySelector('#headlines > ul >li:nth-of-type(1) a').setAttribute("target","")
		// document.querySelector('#headlines > ul >li:nth-of-type(3) a').setAttribute("target","")
		let sub_load_headline = async (k) => {
			if(await mµ.check_host_perm (current_source_selection, sources_objs)) {
				document.getElementById('load_headlines').style.display = 'none'
				let n = sources_objs[k]
				if (! n.headline_url) { return }
				i += 1
				µ.sleep(1000 * i)
				let rep = await fetch(n.headline_url)
				if (!rep.ok) throw `status ${rep.status}`
				let c_type = rep.headers.get('content-type')
				if (n.tags.charset) {
					rep = await rep.arrayBuffer()
					var text_decoder = new TextDecoder(n.tags.charset, {fatal: true})
					rep = text_decoder.decode(rep)
				} else {
					rep = await rep.text()
				}
				let r = µ.dom_parser.parseFromString(rep, µ.clean_c_type(c_type))
				if(n.headline_selector && n.headline_selector !== '' ) {
					try {
						if (!n.domain_part || n.extends) n.domain_part = µ.domain_part(n.headline_url)
						if (!n.favicon_url) n.favicon_url = µ.get_favicon_url(r, n.domain_part)
						let h = r.querySelector(n.headline_selector)
						let h_title = $_(!n.h_title ? 'headline_selector' : 'h_title', r, n)
						if(!set_hdl.has(h_title)) {
							set_hdl.add(h_title)
							headlineList.add({
								h_title:µ.shorten(h_title, HEADLINE_TITLE_SIZE),
								h_url:	µ.urlify(h.getAttribute('href'), n.domain_part),
								h_icon: n.favicon_url,
								h_tip: `'${n.tags.name}' (${ndt().toLocaleString(userLang)})\n`+
									`${µ.triw(h.textContent)}`
							})
						}
					} catch (exc) {
						console.error('header for', k, exc)
						add_err(exc,k,'warn', 'test_src')
					}
				}
				document.getElementById('mp_headlines_page_sizes').style.display = 'block'
			}
		}
		for (let k of current_source_selection.slice(0,await mµ.get_stored_max_headline_loading()))
			sub_load_headline(k)
	}
	/* * * Tags * * */
	var select_multiple_opt = {
		removeItemButton: true,
		resetScrollPosition: false,
		placeholder: true,
		placeholderValue: '*',
		duplicateItemsAllowed: false,
		searchResultLimit: 8
	}
	var tag_select_multiple = {
		'tech': '',
		'lang': '',
		'country': '',
		'themes': '',
		'src_type': '',
		'res_type': '',
		//'scope': '',
		//freq': '',
		'name': '',
		'collection': '',
		'remove_source_selection': []
	}
	var add_source_selection = []
	var available_tags = {}
	async function populate_tags(source_selection) {
		for (let tag_name of Object.keys(tag_select_multiple)) {
			available_tags[tag_name] = {}
		}
		// collection
		let coll_ons = await mµ.get_stored_collections()
		for (const i of Object.keys(coll_ons.provided)) {
			available_tags['collection'][i] = coll_ons.provided[i].length
		}
		for (const i of Object.keys(coll_ons.created)) {
			available_tags['collection'][i] = coll_ons.created[i].length
		}
		for (let k of source_selection) {
			let n = sources_objs[k]
			if (typeof(n) === 'undefined') return
			for (let tag_name of Object.keys(tag_select_multiple)) {
				if ('remove_source_selection' === tag_name) continue
				if ('collection' === tag_name) continue
				let tag_val = n.tags[tag_name]
				if (typeof(tag_val) === 'string') {
					available_tags[tag_name][tag_val] = 1 + available_tags[tag_name][tag_val] || 1
				} else if (typeof(tag_val) === 'object') {
					for (let tag_val_itm of tag_val) {
						available_tags[tag_name][tag_val_itm] =
							1 + available_tags[tag_name][tag_val_itm] || 1
					}
				} else {
					console.warn(`Unknown tag type ${tag_name} ${tag_val} for ${k}`)
				}
			}
		}
		for (let tag_name of Object.keys(tag_select_multiple)) {
			let s = []
			let t_user_lang = ""
			for (var t of Object.keys(available_tags[tag_name]).sort()) {
				t_user_lang = mp_i18n.gettext(t)
				if(tag_name === 'lang') {
					s.push({value: t, label:
						/*`${µ.intl_lang_name(t)}<small> - <span>[${t_user_lang}]</span><span> (${*/
						`${LANG[t]}<small> - <span>[${t_user_lang}]</span><span> (${
							available_tags[tag_name][t]})</span></small>`})
				} else {
					s.push({value: t, label:
						`${t_user_lang}<small> (${available_tags[tag_name][t]})</small>`})
				}
			}
			if ('remove_source_selection' === tag_name) continue
			let element = document.getElementById(`tags_${tag_name}`)
			tag_select_multiple[tag_name] = new Choices(element,
				Object.assign({ choices: s }, select_multiple_opt))
		}
	}
	var already_add = []
	function dns_prefetch(current_source_selection) {
		/* let dns_links = document.getElementsByClassName('mp_dns_prefetch')
		for (let i = dns_links.length; i--;) {
			dns_links.item(i).remove() // parentNode.removeChild(dns_prefetch_link)
		} */
		for (let i of current_source_selection) {
			let n = sources_objs[i]
			if (typeof(n) === 'undefined') return
			if (n.search_url)
				append_prefetch_link(n.search_url)
			if (n.redir_url)
				if (µ.is_array(n.redir_url))
					for (const i of n.redir_url)
						append_prefetch_link(i)
				else
					append_prefetch_link(n.redir_url)
		}
	}
	function append_prefetch_link(url) {
		if (already_add.includes(url)) return
		let l = document.createElement('link')
		l.rel='dns-prefetch'
		l.href= url
		l.setAttribute("crossorigin", "anonymous")
		//l.classList.add('mp_dns_prefetch')
		document.head.appendChild(l)
		already_add.push(url)
	}
	async function on_tag_sel(opt) {
		let coll_ons =  await mµ.get_stored_collections()
		if (!cur_querystring.get('id_sch_s'))
			browser.storage.sync.set({filters: tags_toJSON()})
		let new_src_sel = {}
		let tag_names = ["name", "collection"]
		for (const i of tag_names) {
			let tag_name_length = tag_select_multiple[i].getValue(true).length
			if (tag_name_length) {
				for (let other_mul_sel_key of Object.keys(tag_select_multiple)) {
					if (other_mul_sel_key === i
						|| other_mul_sel_key === 'remove_source_selection') continue
					let other_mul_sel = tag_select_multiple[other_mul_sel_key]
					other_mul_sel.removeActiveItems()
					other_mul_sel.disable()
				}
				break
			} else {
				for (let other_mul_sel_key of Object.keys(tag_select_multiple)) {
					if (other_mul_sel_key === i
						|| other_mul_sel_key == 'remove_source_selection') continue
					let other_mul_sel = tag_select_multiple[other_mul_sel_key]
					other_mul_sel.enable()
				}
			}
		}
		for (const tag_name of Object.keys(tag_select_multiple)) {
			new_src_sel[tag_name] = {}
			if ('remove_source_selection' === tag_name || 'add_source_selection' === tag_name)
				continue
			else {
				for (let selected_tag of tag_select_multiple[tag_name].getValue(true)) {
					if (tag_name !== 'tech' && tag_name !== 'collection') { // inner accumulation
						for (let k of sources_keys)
							if (µ.is_array(sources_objs[k].tags[tag_name])) {
								if (sources_objs[k].tags[tag_name].includes(selected_tag))
									new_src_sel[tag_name][k] = ''
							} else {
								if (sources_objs[k].tags[tag_name] === selected_tag)
									new_src_sel[tag_name][k] = ''
							}
					} else if ('collection' === tag_name) {
						let res = coll_ons.created[selected_tag]
						if (typeof (res) === 'undefined')
							res = coll_ons.provided[selected_tag]

						for (const i of res) {
							new_src_sel[tag_name][i] = ''
						}
					} else { // inner intersection
						let cur_tag_keys = []
						for (let k of sources_keys)
							if (sources_objs[k].tags[tag_name].includes(selected_tag))
								cur_tag_keys.push(k)
						if (Object.keys(new_src_sel['tech']).length === 0) {
							for (let c of cur_tag_keys)
								new_src_sel['tech'][c] = ''
							continue
						} else {
							let prec_tag_keys = Object.keys(new_src_sel[tag_name])
							new_src_sel[tag_name] = {}
							for (let k of prec_tag_keys.filter(e => cur_tag_keys.includes(e))) {
								new_src_sel[tag_name][k] = ''
							}
						}
					}
				}
				if (tag_select_multiple[tag_name].getValue(true).length > 0 &&
					Object.keys(new_src_sel[tag_name]).length === 0) {
					empty_sel(opt)
					return
				}
			}
		}
		let new_src_sel_keys = sources_keys
		let tag_sel_keys = {}
		for (let tag_name of Object.keys(tag_select_multiple)) { // get the smallest ensemble
			if ('remove_source_selection' === tag_name) continue
			if (new_src_sel_keys.length === 0) {
				break
			} else {
				tag_sel_keys = Object.keys(new_src_sel[tag_name])
				if (tag_sel_keys.length > 0)
					new_src_sel_keys = new_src_sel_keys.filter(
						v => tag_sel_keys.includes(v))
			}
		}
		if (new_src_sel_keys.length > 0) {
			for (const i of tag_select_multiple.remove_source_selection) {
				let index = new_src_sel_keys.indexOf(i)
				if (index > -1)
					new_src_sel_keys.splice(index, 1)
				else {
					index = tag_select_multiple.remove_source_selection.indexOf(i)
					tag_select_multiple.remove_source_selection.splice(index, 1)
				}
			}
			for (const i of add_source_selection) {
				let index = new_src_sel_keys.indexOf(i)
				if (index > -1)
					new_src_sel_keys.push(i)
			}
			current_source_selection = new_src_sel_keys
			for (const i of add_source_selection) {
				if (current_source_selection.indexOf(i) == -1)
					current_source_selection.push(i)
			}
			current_source_nb = current_source_selection.length
			if (!cur_querystring.get('id_sch_s'))
				browser.storage.sync.set({filters: tags_toJSON()}) 
			if (await mµ.get_stored_live_search_reload() === '1' && document.getElementById('mp_query').value) {
				document.getElementById('mp_submit').click()
				dns_prefetch(current_source_selection)
			}
			if ((await mµ.get_stored_headline_loading() === '1' && headlineList.size() === 0)) {
				load_headlines()
			} else if(await mµ.get_stored_live_headline_reload() === '1' && headlineList.size()) {
				var h_display = document.getElementById('mp_headlines_page_sizes').style.display
				if(h_display !== 'none' && (opt && opt.target && opt.target.id !== 'tags_tech'))
					document.getElementById('reload_headlines').click()
			}
		} else {
			empty_sel(opt)
		}
		
		update_nb_src()
	}
	function empty_sel(opt) {
		alert(mp_i18n.gettext('This choice would result in an empty selection.'))
		if (opt && typeof (opt.parentNode) !== 'undefined') {
			tag_select_multiple[opt.parentNode.id.replace('tags_', '')].setValue(opt.value)
		} else {
			set_default_tags()
		}
		add_source_selection = []
		tag_select_multiple.remove_source_selection = []
		on_tag_sel({})
	}
	function tags_toJSON() {
		let tags = {}
		for (let t of Object.keys(tag_select_multiple)) {
			if ('remove_source_selection' === t) continue
			else tags[t] = tag_select_multiple[t].getValue(true)
		}
		return tags
	}
	function tags_toXML(type) {
		let tags = '', tag_val
		for (let t of Object.keys(tag_select_multiple)) {
			if ('remove_source_selection' === t) continue
			tag_val = tag_select_multiple[t].getValue(true)
			if (typeof(tag_val) === 'string') {
				if ('ATOM' === type) tags += `<category term='${tag_val}' label='mp__${t}' />\n`
				else tags += `<category>mp__${t}__${tag_val}</category>\n`
			} else if (typeof(tag_val) === 'object')
				for (let v of tag_val)
					if ('ATOM' === type) tags += `<category term='${v}' label='mp__${t}' />\n`
					else tags += `<category>mp__${t}__${v}</category>\n`
		}
		for (const i of tag_select_multiple.remove_source_selection) {
			if ('ATOM' === type)
				tags += `<category term='${i}' label='mp__remove_source_selection' />\n`
			else
				tags += `<category>mp__${i}__remove_source_selection</category>\n`
		}
		for (const i of add_source_selection) {
			if ('ATOM' === type)
				tags += `<category term='${i}' label='mp__add_source_selection' />\n`
			else
				tags += `<category>mp__${i}__add_source_selection</category>\n`
		}
		return tags
	}
	function tags_fromCSV(line) {
		let tag_values = {}
		for (let t of Object.keys(tag_select_multiple)) tag_values[t] = []
		let order = ["src_type", "lang", "res_type", "themes", "tech", "country", "name",
			"remove_source_selection", "add_source_selection"]
		let counter = 10
		for (const i of order) {
			if (line[counter].indexOf(",") > -1)
				tag_values[i] = line[counter].split(",")
			else
				tag_values[i] = [line[counter]]
			counter++
		}
		tags_fromJSON(tag_values)
		on_tag_sel({})
	}
	function tags_fromXML(tag_nodes, xml_type) {
		let tag_values = {}
		for (let t of Object.keys(tag_select_multiple)) tag_values[t] = []
		tag_values['add_source_selection']= []
		tag_values['remove_source_selection'] = []
		for (let c of tag_nodes)
			if ('ATOM' === xml_type) {
				let c_name = c.getAttribute('label')
				try { tag_values[c_name.split('__')[1]].push(`${c.getAttribute('term')}`) }
				catch (exc) { console.warn(`Bad ATOM filter name '${c_name}' (${exc}).`) }
			} else {
				let c_split = c.textContent.split('__')
				if (c_split.length > 2) {
					try { tag_values[c_split[1]].push(`${c_split[2]}`) }
					catch (exc) { console.warn(`Bad RSS filter name '${c.textContent}' (${exc}).`) }
				}
			}
		tags_fromJSON(tag_values)
		on_tag_sel({})
	}
	function tags_fromJSON(tag_values) {
		let v, s
		for (let tag_name of Object.keys(tag_values)) {
			if ('remove_source_selection' === tag_name) {
				tag_select_multiple.remove_source_selection = tag_values[tag_name]
				continue
			} else if ('add_source_selection' === tag_name) {
				add_source_selection = tag_values[tag_name]
				continue
			}
			// s = document.getElementById(`tags_${tag_name}`)
			s = tag_select_multiple[tag_name]
			v = tag_values[tag_name]
			if ('undefined' === typeof(s)) continue
			s.passedElement.element.removeEventListener('change', tag_change)
			s.removeActiveItems()
			s.setChoiceByValue(v)
			s.passedElement.element.addEventListener('change', tag_change)
		}
		on_tag_sel({})
	}
	function tag_change(evt) {
		add_source_selection = []
		tag_select_multiple.remove_source_selection = []
		on_tag_sel(evt)
	}
	function is_virgin_tags() {
		var r = true
		for (let k of Object.keys(tag_select_multiple)) {
			// console.log(k, "tag mult", tag_select_multiple)
			if ('remove_source_selection' === k) continue
			let v = tag_select_multiple[k].setChoiceByValue(k)
			if (v.getValue(true).length > 0) {
				r = false
			}
		}
		if(cur_querystring.get('id_sch_s')) {r=false}
		return r
	}
	function is_advanced_search() {
		var cur_tags = tags_toJSON()
		return JSON.stringify(get_default_tags()) !== JSON.stringify(cur_tags)
		/* // we used to check only country and name tags
		var r = false
		for (let k of ['country', 'name']) {
			let v = tag_select_multiple[k].setChoiceByValue(k)
			if (v.getValue(true).length > 0) {
				r = true
			}
		}
		return r*/
	}
	// button reset the search setting to the default
	document.getElementById('reset_tag_sel').addEventListener('click', () => {
		add_source_selection = []
		tag_select_multiple.remove_source_selection = []
		set_default_tags()
		on_tag_sel({})
		display_show_src()
	})
	function set_default_tags() {
		console.info('Meta-Press.es : loading default tags')
		tags_fromJSON(get_empty_json_tags())
		tags_fromJSON(get_default_tags())
	}
	/**
	 * Create an object with the default search tags.
	 * @returns {Object} the tags
	 */
	function get_default_tags() {
		return {
			'tech': ['HTTPS', 'fast'],
			'lang': [userLang.slice(0, 2).toLowerCase()],
			'country': [],
			'themes': [],
			'src_type': ['Press'],
			'res_type': [],
			'name': [],
			'remove_source_selection': []
		}
	}
	/**
	 * Set the tags search in browser data in the page.
	 * @param {Object} stored_tags the tags in memory
	 */
	function load_stored_tags(stored_tags) {
		if (typeof(stored_tags) === 'object' && typeof(stored_tags.filters) === 'object') {
			tags_fromJSON(stored_tags.filters)
		}

		if (is_virgin_tags())
			set_default_tags()
		if (is_advanced_search() && document.getElementById('tags_row').style.display === 'none')
			document.getElementById('tags_handle').click()
		document.getElementById('mp_loaded_source_total').textContent = sources_keys.length - 2
		document.getElementById('mp_loaded_source_countries').textContent =
			Object.keys(tag_select_multiple['country']['_initialState']['choices']).length
		document.getElementById('mp_loaded_source_langs').textContent =
			Object.keys(tag_select_multiple['lang']['_initialState']['choices']).length
	}
	/* * * end tags * * */
	/**
	 * Update the sources_keys and set the source selection to all sources.
	 * @param {Object} sources_objs The source Object
	 */
	function update_current_sources(sources_objs) {
		sources_keys = Object.keys(sources_objs)
		current_source_selection = sources_keys
		current_source_nb = sources_keys.length
		update_nb_src()
	}
	/**
	 * Set the search placeholder string with the define value.
	 */
	function set_query_placeholder() {
		var val = [
			'La Quadrature du Net',
			'ADPRIP',
			'Grimoire-Command.es',
			'Le Petit Prince',
			'Hong Kong',
			'Yellow vests',
			'Nuit Debout',
			'Alexandre Benalla',
			'Delta.Chat',
			'F-Droid.org',
			'OpenContacts',
			'AsciiDoctor',
			'Internet ou Minitel 2.0',
			'Pablo Servigne',
			'Laurent Gaudé',
			'Le Prince de Machiavel',
			'Pensées pour moi-même',
			'La princesse de Clèves',
			'Alpine Linux',
			'Fairphone',
			'Tor Browser',
			'Wikipedia',
			'OpenStreetMap',
			'Leto Atreides',
			'Pliocene Exile',
			'Another World by Éric Chahi',
			'5 centimeters per second',
			'Nausicaä',
			'Repair Café',
			'Reporterre',
			'Chelsea Manning',
		]
		document.getElementById('mp_query').placeholder =
			mp_i18n.gettext('Search terms, ex.') +` : ${val[µ.pick_between(0, val.length)]}`
	}
	document.getElementById('tags_handle').addEventListener('click', () => {
		document.getElementById('tags_row').style.display = document.getElementById(
			'tags_2nd_row').style.display === 'none' ? 'block' : 'none'
		document.getElementById('tags_2nd_row').style.display = document.getElementById(
			'tags_2nd_row').style.display === 'none' ? 'block' : 'none'
		document.getElementById('tags_handle').innerHTML=document.getElementById(
			'tags_handle').innerHTML === '➕' ? '&#x2796;' : '&#x2795;'
		document.getElementById('close_src').click()
		// cur_querystring.set('adv_search', cur_querystring.get('adv_search') ? 0 : 1)
	})
	document.getElementById("mp_running_button_src").addEventListener('click', (evt) => { 
		let btn_src_text = document.getElementById("mp_running_button_src_text")
		evt = evt.target
		evt.textContent = evt.textContent === '+' ? '-' : '+'
		btn_src_text.style.visibility = evt.textContent === '+' ? "hidden" : "visible"
		if(btn_src_text.style.visibility === "hidden")
			btn_src_text.textContent = ''
	})
	function is_tag_in_querystring(cur_querystring) {
		for (let t of Object.keys(tag_select_multiple))
			if (cur_querystring.get(t) || cur_querystring.get('id_sch_s'))
				return true
		if (cur_querystring.get('rm_src_select')) 
			return true
		return false
	}
	/**
	 * Reset tag_select_multiple, remove all the search settings.
	 */
	function get_empty_json_tags() {
		var json_obj = {}
		for (let t of Object.keys(tag_select_multiple))
			json_obj[t] = []
		return json_obj
	}
	function querystring_to_json(sp) {
		var json_obj = get_empty_json_tags()
		for (const [key, value] of sp) {
			if (typeof(json_obj[key]) !== 'undefined') {
				json_obj[key].push(value)
			}
			if (key === 'rm_src_select') {
				tag_select_multiple.remove_source_selection.push(value)
				current_source_selection = µ.remove_array_value(current_source_selection, value)
				// function ??
				update_nb_src()
				// end function ??
			}
		}
		return json_obj
	}
	function manage_mp_input_focus() {
		// for (let y of document.querySelectorAll('.choices__input'))
		for (let y of document.querySelectorAll(
			'.choices__list--dropdown > .choices__list > .choices__item')) {
			y.addEventListener('click', () => { // is not called
				document.querySelector('#mp_query').focus()
			}, true)
		}
	}
	var id_elt, last_elt, browser_str
	async function add_elt_url(id) {
		id_elt = id.split('__')[1]
		var list_elt = mp_req.findingList.items
		let not_in_future = false
		let i = 0
		while(!not_in_future && list_elt.length) {
			var dt_elt = new Date(list_elt[i]._values.f_epoc)
			not_in_future = new Date() > dt_elt
			if(not_in_future) {
				last_elt = dt_elt.toUTCString()
			} else { i++ }
		}
		await browser.storage.sync.get().then(setElt)
	}
	async function setElt(elt) {
		browser_str = elt
		await mµ.exec_in_background('init_table_sch_s', [browser_str])
		var sch_sea = elt[`sch_sea__${id_elt}`]
		let local_url = gen_permalink(false)
		let new_url = new URL(local_url.origin + local_url.pathname + sch_sea)
		let sto_r = new_url.searchParams.get('last_res')
		let temp= await mµ.set_text_params(new_url, mp_i18n)
		var list_p = temp['list_p']
		var params = temp['params']
		var body_txt = `${mp_i18n.gettext(' results for "')}`+list_p['q']+'"\n' + params
		local_url.searchParams.delete('last_res')
		local_url.searchParams.sort()
		new_url.searchParams.delete('last_res')
		new_url.searchParams.delete('submit')
		new_url.searchParams.sort()
		if(local_url.search === new_url.search) { // if true we continue
			sto_r = sto_r ? sto_r : 0
			new_url.searchParams.set('last_res', last_elt)
			sch_sea = new_url.search
			var nb_new_res = count_new_res(sto_r)
			if(nb_new_res > 0) {
				document.getElementById('favicon').href="img/favicon-metapress-active.png"
				if (nb_new_res >= 10) { nb_new_res = "+" + nb_new_res }
				body_txt = nb_new_res + mp_i18n.gettext(' new ') + body_txt
				var notif_txt = mp_i18n.gettext('New result for schedule search')
				mµ.notifyUser(`${notif_txt} ${id_elt}`, body_txt)
			}
			/**
			for(let i; i<table_sch_s.length;i++) {
				var m_id_sch_s = `sch_sea__${i}`
				var m_value_sch_s = table_sch_s[i]
				await getting.then((elt)=>{elt.modif_sch_s_storage(m_id_sch_s, m_value_sch_s)})
			}**/
			await mµ.exec_in_background('modif_sch_s_storage', [`sch_sea__${id_elt}`, sch_sea])
		}
	}
	function count_new_res(sto_r) { 
		var results = mp_req.findingList.matchingItems
		// console.log(results)
		let last = null
		var count = 0
		for(let r of results) {
			r.elm.classList.remove("first_new_result", "new_result", "last_new_result")
			var dt_r = r.elm.querySelector('time').dateTime
			if(sto_r !== 0 && new Date(sto_r) < new Date(dt_r)) {
				last = r.elm
				sto_r !== 0 ? last.classList.add("new_result") : ""
				count ? "" : last.classList.add("first_new_result")
				count+=1
			}
			else break
		}
		last ? last.classList.add("last_new_result") : ""
		return count
	}
	// date filter
	let start = document.getElementById('date_filter_sta')
	let stop = document.getElementById('date_filter_sto')
	stop.disabled = true
	document.getElementById('clear_date').onclick = evt => {
		start.value = ''
		stop.value = ''
		stop.setAttribute('min', '')
		stop.disabled = true
		filter_last(evt.target, null)

	}
	start.value = ""
	stop.value = ""
	start.onchange = evt => {
		if (new Date(evt.target.value)) {
			document.getElementById('date_filter_sto').setAttribute('min', evt.target.value)
			filter_last(evt.target, null)
			// reset if Start empty
			stop.disabled = evt.target.value === ''
			if (stop.disabled)
				stop.value = ''
			if (evt.target.value !== '')
				filter_date_range(evt)
		}
	}
	stop.onchange = evt => {
		if (new Date(evt.target.value))
			filter_date_range(evt)
	}
	/**
	 * Apply an date range filter with #date_filter_sta #date_filter_sto.
	 * @param {Event} evt The event fired
	 */
	function filter_date_range(evt) {
		filter_last(evt.target, (r) => {
			let start = document.getElementById('date_filter_sta').value			
			let stop = document.getElementById('date_filter_sto').value
			if (stop === "")
				stop = new Date().toISOString()
			if (start === "")
				start = new Date().toISOString()
			if (start > stop)
				start = stop
			return r.values().f_epoc >= new Date(start) &&
					r.values().f_epoc <= new Date(stop)
		})
	}
	// end date filter
	var nb_src_test_50 = 0
	const HEADLINE = []
	var mp_hl = document.querySelectorAll('.mp_hl')
	xml_src = await mµ.exec_in_background("get_XML_SRC")
	for (let h of mp_hl) {
		let v = h.innerHTML
		let u = h.href
		let t = h.getAttribute('title')
		HEADLINE.push([v,u,t])
	}
	var visibility_status = 0
	/**
	 * Refresh the Metapress sources (sources_objs, current_source_nb, current_source_selection)
	 * and also refresh the choice js selection.
	 * @param {string} src_name The name of the soure to get
	 * @param {boolean} build If we need to rebuild the sources before import
	 */
	async function update_source_objs(src_name="", build=false, init=true) {
		if (visibility_status === 0 && init) {
			visibility_status++
			return
		} else visibility_status = 0
		if (build) res = await mµ.exec_in_background('build_src')
		sources_objs = await mµ.exec_in_background('get_prov_src', [src_name])
		update_current_sources(sources_objs)
		if (typeof (mp_i18n) !== 'undefined') {
			let old = await browser.storage.sync.get('filters')
			if (!init && is_tag_in_querystring(cur_querystring))
				old  = {filters: querystring_to_json(cur_querystring)}
			if (typeof (tag_select_multiple.collection) === 'object' && init)
				for (const i of Object.keys(tag_select_multiple)) {
					if ('remove_source_selection' === i) continue
					tag_select_multiple[i].destroy()
				}
			populate_tags(current_source_selection).then(() => {load_stored_tags(old)})
			document.getElementById('cur_tag_perm_nb').onclick = () => {
				let search_src = {}
				for (const i of current_source_selection)
					search_src[i] = sources_objs[i]
				mµ.request_sources_perm(search_src).then(() => {update_nb_src()})
			}
			display_show_init()
		}
		document.dispatchEvent(new Event('tags_src_load'))
	}
	/* * end definitions * */
	await mµ.set_theme()
	let month_nb_json = await fetch('js/month_nb/month_nb.json')
	const cur_querystring = new URL(window.location).searchParams
	const img_load = '/img/photo_loading_placeholder.png'
	month_nb_json = await month_nb_json.json()
	test_src = cur_querystring.get('test_sources')
	if (test_src === '1') {
		var src_name = cur_querystring.getAll('name')
		await update_source_objs(src_name, false, false)
		// load_source_objs()
		document.getElementById('mp_submit').click()
	} else {
		let lang_names = await fetch('json/lang_names.json')
		await g.xgettext_html()
		var mp_i18n = await g.gettext_html_auto(userLang) // must be before populate tags
		var LANG = await lang_names.json() // must be before populate tags
		await update_source_objs("", false, false)
		document.onvisibilitychange = evt =>  {
			update_source_objs()
		}
		// load_source_objs(await browser.storage.sync.get('custom_src'))
		let is_launch_test = cur_querystring.get('launch_test')
		let sch_search_test = cur_querystring.get('submit')
		if(is_launch_test) {
			if(sch_search_test === 1) {
				before_test_sources(sources_objs)
				results_test(true)
			} else {
				test_integrity_data = await mµ.exec_in_background('get_test_integrity')
				let a = document.getElementById('mp_start_test')
				a.style = 'display:inline-block;'
				let provided_sources_json = await mµ.exec_in_background('get_raw_src')
				a.addEventListener('click', async() => {
					a.style = 'display:none;'
					document.getElementById('mp_display_report_test').style = 'display:inline-block;'
					if(await mµ.request_sources_perm(provided_sources_json)) {
						mµ.clear_reserved_name()
						before_test_sources(sources_objs)
						results_test(true)
					}
				})
				let b = document.getElementById('mp_start_test_nb')
				b.style = 'display:inline-block;'
				b.addEventListener('click', async() => {
					let src_key = Object.keys(sources_objs)
					document.getElementById('mp_display_report_test').style = 'display:inline-block;'
					let need_test = Math.min(nb_src_test_50 + 50, src_key.length)
					let src_test = {}
					for (let i = nb_src_test_50; i < need_test; i++) {
						src_test[src_key[i]] = sources_objs[src_key[i]]
					}
					b.style = 'display:none;'
					if(await mµ.request_sources_perm(provided_sources_json)) {
						before_test_sources(src_test)
						results_test(true)
					}
					b.style = 'display:inline-block;'
					nb_src_test_50 += 50
					if (nb_src_test_50 >= src_key.length)
						mµ.clear_reserved_name()
						nb_src_test_50 = 0
				})
			}
		}
		µ.set_html_lang(userLang)
		var is_load_photos = await mµ.get_stored_load_photos() === '1'
		var alt_load_txt = mp_i18n.gettext('Click to load image')
		const do_manage_mp_input_focus = false
		if (do_manage_mp_input_focus)
			manage_mp_input_focus() // must be after populate_tags
		// removed to allow kbd nav downward
		if(cur_querystring.get('id_sch_s') && !cur_querystring.get('submit')) {
			let modif_txt = mp_i18n.gettext('save modification')
			document.querySelector('.add_auto_s').innerHTML =
				`<span class="black">&#9999;&#65039;</span> ${modif_txt}`
		}
		dns_prefetch(current_source_selection)
		set_query_placeholder()
		var manifest = await fetch('manifest.json')
		manifest = await manifest.json()
		document.getElementById('mp_version').textContent = 'v' + manifest.version
		rotate_headlines_timeout()
		if (cur_querystring.get('submit')) document.getElementById('mp_submit').click()
		if(test_src === '0') {
			let elt = document.getElementById('refresh_src')
			elt.style = 'display:inline-block'
			elt.addEventListener('click', async() => {
				await mµ.exec_in_background('build_src')
				reload_keeping_query(true)

			})
		}
	}
	/**
	 * Import a json who contain the custom source.
	 * @param {Event} evt event fired
	 */
	async function import_custom_src(evt) {
		// https://stackoverflow.com/questions/16215771/how-to-open-select-file-dialog-via-js#answer-40971885
		let input = document.createElement('input');
		input.type = 'file';
		input.accept = '.json'
		input.click();
		input.onchange = async () => {
			let file = input.files.item(0)
			let txt = await file.text()
			try {
				let new_src =  JSON.parse(txt)
				let browser_data
				let old = {}
				await browser.storage.sync.get().then(elt=>{browser_data = elt})
				if (typeof (stored_data) === 'object' 
					&& typeof (browser_data.custom_src) === 'string'
					&& browser_data.custom_src && browser_data.custom_src !== '{}') {
						old = JSON.parse(browser_data.custom_src)
					}
				let merge_src = Object.assign(old, new_src)
				browser_data.custom_src = JSON.stringify(merge_src,null,  "   ")
				browser.storage.sync.set(browser_data)
				update_source_objs("", true)
			} catch (error) {
				window.alert("File not readable (bad format, corrupted file)")
			}
		}
	}
	/**
	 * Export the custom source user in JSON file.
	 * @param {Event} evt event fired
	 */
	function export_custom_src(evt) {
		browser.storage.sync.get('custom_src').then(
			stored_data =>{
				if (typeof (stored_data) === 'object' 
					&& typeof (stored_data.custom_src) === 'string'
					&& stored_data.custom_src && stored_data.custom_src !== '{}') {
					µ.upload_file_to_user(`${µ.ndt_human_readable()}_custom_sources.json`, stored_data.custom_src)
				} else window.alert("No custom sources found")
			},
			err => { console.error(`Loading custom_src: ${err}`)})
	}

	document.getElementById('import_c_src').onclick = import_custom_src
	document.getElementById('export_c_src').onclick = export_custom_src
	document.getElementById('mp_edit_v_src_search').onclick = () => {
		display_panel.page = sources_keys.length
		display_panel.update()
		let command_bar = document.getElementById('mp_v_sel')
		let need_select = document.querySelectorAll('.src_list_select')
		if ('none' === command_bar.style.display) {
			command_bar.style.display = 'block'
			command_bar.setAttribute('active', true)
			for (const i of need_select)
				i.style.display = 'block'
		} else {
			command_bar.style.display = 'none'
			command_bar.setAttribute('active', false)
			for (const i of need_select)
				i.style.display = 'none'
		}
		display_panel.page = 20
		display_panel.update()
	}

	// tab selector
	mµ.init_tabs('tab_v_src', id => {
		let edit = document.getElementById('mp_edit_v_src_search')
		let src_list = document.getElementById('list_src_t')
		let coll_list = document.getElementById('coll_list_t')
		src_list.style.display = 'block'
		coll_list.style.display = 'none'
		if ('v_all' === id) {
			display_panel.filter()
			edit.style.display = 'none'
			let command_bar = document.getElementById('mp_v_sel')
			if (command_bar.getAttribute('active') === 'true')
				edit.click()
			command_bar.style.display = 'none'
		}
		else if ('v_select' === id) {
			document.getElementById('mp_clear_v_src_search').click()
			filter_v_src_select()
			edit.style.display = 'block'
		} else if ('v_collection' === id) {
			src_list.style.display = 'none'
			coll_list.style.display = 'block'
			init_collection()
		}
	})

	/**
	 * @var The list.js wich permit the source management.
	 */
	var display_panel
	document.getElementById('view_select_src').onclick = () => {
		let section = document.getElementById('list_src')
		if (section.style.display === 'none') {
			section.style.display = 'block'
			display_show_src()
		} else section.style.display = 'none'
	}

	document.getElementById('close_src').onclick = () => {
		document.getElementById('list_src').style.display = 'none'
	}
	
	/**
	 * Create and fill the list.js display src panel with the source.
	 * @async
	 * @function
	 */
    async function display_show_init() {
		if (typeof (display_panel) === 'undefined') {
			let options = {
				item: "src_item_tpl",
				valueNames: [
					{ name: "src_list_select", attr: "src_url" },
					{ name: "f_icon", attr: "src" },
					{ name: "f_icon_title", attr: "title" },
					{ name: "f_icon_alt", attr: "alt" },
					"src_list_nom",
					"src_list_country",
					"src_list_lang",
					"src_list_type",
					"src_list_result_type",
					{ name: "src_list_nom_title", attr: "title" },
					{ name: "src_list_country_title", attr: "title" },
					{ name: "src_list_lang_title", attr: "title" },
					{ name: "src_list_type_title", attr: "title" },
					{ name: "src_list_result_type_title", attr: "title" },
					"black",
					{ name: "src_list_url", attr: "href" },
					{ name: "src_list_edit", attr: "href" },
					{ name: "src_list_del", attr: "src_url" },
					{ name: "src_list_add", attr: "src_url" }
				],
				page: 20,
				pagination: true,
				innerWindow: 4,
				left: 1,
				right: 1
			}
			display_panel = new List("list_src_t", options)
		} else
			display_panel.clear()
		// fill
		let form_src = document.baseURI
		form_src = µ.domain_part(form_src)
		let to_add = []
        for (const i of sources_keys) {
			let current_src = sources_objs[i]
			let symbol = ""
			let favicon_alt
			form_src += `/new_source.html?name=${i}`
			if (current_src["custom"]) {
				symbol = "&#128190;"
				favicon_alt = 'icon'
			}
			let favicon_url = current_src.favicon_url
			let name = current_src.tags.name
			let country = current_src.tags.country
			let lang = current_src.tags.lang
			let src_type = current_src.tags.src_type
			let res_type = current_src.tags.res_type
            to_add.push({
				src_list_select: i,
				f_icon: favicon_url,
				f_icon_title: name,
				f_icon_alt: favicon_alt || name,
				black: symbol,
				src_list_nom: name,
				src_list_nom_title: name,
				src_list_country: country,
				src_list_country_title: country,
				src_list_lang: lang,
				src_list_lang_title: lang,
				src_list_type: src_type,
				src_list_type_title: src_type,
				src_list_result_type: res_type,
				src_list_result_type_title: res_type,
				src_list_url : i,
				src_list_edit: form_src,
				src_list_del: i,
				src_list_add: i
			})
        }
		display_panel.add(to_add, () => {
			display_panel.sort('src_list_nom', { order: "asc" })
			display_show_src()
		})
    }
	/**
	 * Refresh the filter used when the sources to displayed change.
	 * @function
	 */
	function display_show_src() {
		if (typeof(display_panel) === 'undefined') return
		let current_tab = document.getElementById('tab_v_src').getAttribute('tab')
		if ('v_all' === current_tab) // refresh filter
			display_panel.filter()
		else
			filter_v_src_select()
		let search = document.getElementById('mp_v_src_search')
		if (search.value !== "")
			search.click() // manual reload
	}
	/**
	 * Apply a filter for only see the selected source.
	 */
	function filter_v_src_select() {
		display_panel.filter()
		display_panel.filter(item => {
			return current_source_selection.includes(item.values().src_list_url)
		})
	}
	display_panel.on('updated', () => {
		for (const i of display_panel.visibleItems) {
			let li = i.elm
			li.querySelector('.src_list_del').onclick = evt => {
				let key = evt.target.getAttribute("src_url")
				tag_select_multiple.remove_source_selection.push(key)
				µ.remove_array_value(current_source_selection, key)
				µ.remove_array_value(add_source_selection, key)
				update_nb_src()
				display_show_src()
			}
			li.querySelector('.src_list_add').onclick = evt => {
				let key = evt.target.getAttribute('src_url')
				tag_select_multiple.remove_source_selection = µ.remove_array_value
					(tag_select_multiple.remove_source_selection, key)
				current_source_selection.push(key)
				add_source_selection.push(key)
				update_nb_src()
				display_show_src()
			}
			li.querySelector('.src_list_only').onclick = evt => {
				let key = evt.target.parentNode.children[3].getAttribute('src_url')
				tag_select_multiple.remove_source_selection = current_source_selection
				tag_select_multiple.remove_source_selection = µ.remove_array_value
					(tag_select_multiple.remove_source_selection, key)
				current_source_selection = []
				current_source_selection.push(key)
				add_source_selection.push(key)
				update_nb_src()
				display_show_src()
			}
			// display the corresponding button
			let add = li.querySelector('.src_list_add')
			let del = li.querySelector('.src_list_del')
			let key = del.getAttribute('src_url')
			add.style.display = 'block'
			del.style.display = 'none'
			if (current_source_selection.includes(key)) {
				li.classList.add('selected')
				add.style.display = 'none'
				del.style.display = 'block'
			} else li.classList.remove('selected')
		}
	})

	document.getElementById('mp_v_src_remove').onclick = () => {
		display_panel.page = sources_keys.length
		display_panel.update()
		for (const i of display_panel.visibleItems)
			if (i.elm.querySelector('.src_list_select').checked)
				i.elm.querySelector('.src_list_del').click()
		display_panel.page = 20
		display_panel.update()
	}
	document.getElementById('mp_v_src_sel_all').onclick = () => {
		display_panel.page = sources_keys.length
		display_panel.update()
		for (const i of display_panel.visibleItems)
			i.elm.querySelector('.src_list_select').checked = true
		display_panel.page = 20
		display_panel.update()
	}
	document.getElementById('mp_v_src_unsel_all').onclick = () => {
		display_panel.page = sources_keys.length
		display_panel.update()
		for (const i of display_panel.visibleItems)
			i.elm.querySelector('.src_list_select').checked = false
		display_panel.page = 20
		display_panel.update()
	}
	
	// search in list
	var searchInSRCCallback_timeout
	document.getElementById('mp_v_src_search').onkeyup = search_v_src
	document.getElementById('mp_v_src_search').onclick = search_v_src

	/**
	 * Search in the display src panel.
	 * @param {Event} evt the event fired
	 */
	function search_v_src(evt) {
		clearTimeout(searchInSRCCallback_timeout)
		searchInSRCCallback_timeout = setTimeout(() => {
			display_panel.search(µ.triw(evt.target.value),
				["src_list_nom", "src_list_lang", "src_list_country", "src_list_type", "src_list_result_type"])
		}, evt.keyCode === 13 ? 0 : 500)
	}
	/**
	 * Clear the search bar.
	 */
	document.getElementById('mp_clear_v_src_search').onclick = () => {
		document.getElementById('mp_v_src_search').value = ''
		display_panel.search()
	}
	/**
	 * @var The collection list.
	 */
	var display_panel_coll
	/**
	 * Init the collection.
	 * @async
	 */
	async function init_collection() {
		if (typeof (display_panel_coll) === 'undefined') {
			let options = {
				item: "coll_list_tpl",
				valueNames: ["coll_list_nom"],
				page: 20,
				pagination: true
			}
			display_panel_coll = new List("coll_list_t", options)
		} else display_panel_coll.clear()

		let to_add = []
		let colls = await mµ.get_stored_collections()
		let provided_coll = colls.provided
		let created_coll = colls.created
		for (const i of Object.keys(provided_coll))to_add.push({coll_list_nom: i})
		for (const i of Object.keys(created_coll)) to_add.push({coll_list_nom: i})

		await display_panel_coll.add(to_add, (items) => {
			for (const i of items) {
				let li =  i.elm
				li.querySelector('.coll_list_only').onclick = evt => {
					let name = evt.target.parentNode.parentNode.firstElementChild.textContent
					tag_select_multiple.collection.removeActiveItems()
					tag_select_multiple.collection.setChoiceByValue(name)
					on_tag_sel({})
				}
				let del_btn  = li.querySelector('.coll_list_del')
				let name = li.firstElementChild.textContent
				if (colls.created[name])
					del_btn.style.display = 'block'
				del_btn.onclick = async evt => {
					let colls = await mµ.get_stored_collections()
					let name = evt.target.parentNode.parentNode.firstElementChild.textContent
					display_panel_coll.remove('coll_list_nom', name)
					delete colls.created[name]
					browser.storage.sync.set({'collections': colls}).then(reload_cherry_pick)
				}
			}
		})
	}
	async function reload_cherry_pick() {
		let old_selected = tag_select_multiple.collection.getValue(true)
		let available_tags = {}
		let tag_name = 'collection'
		let coll_ons = await mµ.get_stored_collections()
		tag_select_multiple.collection.destroy()
		for (const i of Object.keys(coll_ons.provided))
			available_tags[i] = coll_ons.provided[i].length
		for (const i of Object.keys(coll_ons.created))
			available_tags[i] = coll_ons.created[i].length
		let s = []
		let t_user_lang = ""
		for (let t of Object.keys(available_tags).sort()) {
			t_user_lang = mp_i18n.gettext(t)
			s.push({value: t, label:`${t_user_lang}<small> (${available_tags[t]})</small>`})
		}
		let element = document.getElementById(`tags_${tag_name}`)
		tag_select_multiple[tag_name] = new Choices(element,
			Object.assign({ choices: s }, select_multiple_opt))
		for (const i of old_selected) 
			tag_select_multiple.collection.setChoiceByValue(i)
		
	}
	document.getElementById('mp_v_coll_create').onclick = async () => {
		display_panel.page = sources_keys.length
		display_panel.update()
		let selec = document.querySelectorAll('.src_list_select')
		let to_collect = []
		for (const i of selec)
			if (i.checked) to_collect.push(i.getAttribute('src_url'))
		
		let colls = await mµ.get_stored_collections()
		let default_name = ''
		if (tag_select_multiple.collection.getValue(true).length === 1)
			default_name = tag_select_multiple.collection.getValue(true)[0]
		let name = prompt('Collection name :', default_name)
		if (name) {
			alert('Name not valid')
			display_panel.page = 20
			display_panel.update()
			return
		}
		if (to_collect.length === 0) {
			alert('No source selected')
			display_panel.page = 20
			display_panel.update()
			return
		}
		colls.created[name] = to_collect
		browser.storage.sync.set({'collections': colls}).then(reload_cherry_pick)
		for (const i of selec) i.checked = false
		display_panel.page = 20
		display_panel.update()
		document.getElementById('mp_edit_v_src_search').click()
	}

	var searchInCollCallback_timeout
	document.getElementById('mp_v_coll_search').onkeyup = (evt) => {
		clearTimeout(searchInCollCallback_timeout)
		searchInCollCallback_timeout = setTimeout(() => {
			display_panel_coll.search(µ.triw(evt.target.value), ['coll_list_nom'])
		}, evt.keyCode === 13 ? 0 : 500)
	}
	document.getElementById('mp_clear_v_coll_search').onclick = () => {
		coll_search.value = ''
		display_panel_coll.search()
	}

})()