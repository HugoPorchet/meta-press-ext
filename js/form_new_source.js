import * as mµ from './mp_utils.js'
import * as µ from './utils.js'
import * as g from './gettext_html_auto.js/gettext_html_auto.js'
import * as rjson from './deps/renderjson.js'

(async () => {
    /*
    let url = "https://www.nytimes.com/svc/add/v1/sitesearch.json?q=%22covid%22&sort=desc&offset=0"
    let raw = await fetch(url, {
        method: 'GET',
        headers: { 'Content-Type': 'application/x-www-form-urlencoded'}
    })
    let raw_json = await raw.json()

    document.getElementById("sub_dom").appendChild(
        renderjson(raw_json)
    )

    document.getElementById("sub_dom").addEventListener("click", (evt) => {
        console.log(evt.target)
        let path = part_selector_get_path_json(evt.target)
        console.log(path)
    })
*/



	'use strict' 
    const userLang = await mµ.get_wanted_locale()
	await g.xgettext_html()
	const mp_i18n = await g.gettext_html_auto(userLang)
    // Formulaire 
    var fns_start = {
        completed: false,
        passed: false,
        nb_required : 2,
        nb_optionnal : 1,
        title: "Start new source",
        doc : {
            fieldset : null,
            legend: null,
            tags_name: null,
            headline: null,
            favicon: null,
            btn_next: null
        },
        text: {
            MSG_ERROR_VALUE_EMPTY: "Sorry but this value is required.",
            MSG_ERROR_NAME_ALREADY_EXIST: "Sorry but this name already exist.",
            MSG_ERROR_URL_NOT_FOUND: "Sorry but this url is not found or not valid.",
            MSG_ERROR_FAVICON_NOT_FOUND: "Sorry but this favicon is not found or not valid",
            MSG_TIPS_TAGS_NAME: "Tips : Just give a name of the source like 'Custom Source'.",
            MSG_TIPS_HEADLINE_URL: "Tips : Give a url of the source like 'https://www.custom-source.eu'.",
            MSG_TIPS_FAVICON: "Tips : Give a url of the favicon source like 'https://www.custom-source.eu/custom/favicon.png'."
        },
        data : {
            tags_name : {type: "required", test_passed : false, value: ""}, 
            headline : {type: "required", test_passed : false, value: ""},
            favicon : {type: "optionnal", test_passed : false, value: ""}
        }
    }
    var fns_timezone = {
        completed: false,
        nb_required : 3,
        nb_optionnal : 0,
        title: "Timezone",
        doc : {
            fieldset : null,
            legend: null,
            tags_lang: null,
            tags_country: null,
            tags_timezone: null,
            check_all_timezone: null,
            btn_next: null
        },
        text: {
            MSG_ERROR_VALUE_EMPTY: "Sorry but this value is required.",
            MSG_ERROR_VALUE_IS_INCORRECT: "Sorry but this value is incorrect or empty."
        },
        data : {
            tags_lang : {type: "required", test_passed : false, value: ""}, 
            tags_country : {type: "required", test_passed : false, value: ""},
            tags_timezone : {type: "required", test_passed : false, value: ""}
        },
        choices : {
            lang : null,
            country: null,
            timezone: null
        }
    }
    var fns_selector = {
        completed: false,
        nb_required : 7,
        nb_optionnal : 4,
        title: "CSS Selector",
        title_css: "CSS Selector",
        title_json: "JSON Selector",
        mode: "get",
        doc : {
            fieldset : null,
            legend: null,
            search_url: null,
            sub_dom: null,
            body: null,
            results: null,
            r_h1: null,
            r_url: null,
            r_dt: null,
            res_nb: null,
            btn_next: null
        },
        text: {
            MSG_ERROR_VALUE_EMPTY: "Sorry but this value is required.",
            MSG_ERROR_BAD_SEARCH_TERM: "Sorry but we didn't found the search terms in the url. Please verify your search terms or the source may use the POST method.",
            MSG_ERROR_IS_NOT_LINK: "Sorry but this element is not a link.",
            MSG_ERROR_IS_NOT_PATH : "Sorry but this value is not a CSS path.",
            MSG_ERROR_REGEX_NOT_VALID : "Sorry but this regex is not valid",
            MSG_ERROR_TEXT_CONTENT_EMPTY : "Sorry but the text content is empty. Please choose a attribut before use a regex functionnality.",
            MSG_ERROR_NOT_ELEMENT_FOUND : "Sorry but not element found with this path.",
            MSG_ERROR_FAIL_TO_PARSE: "Sorry but this regex doesn't parse a text content of element.",
            MSG_ERROR_DATE_FOUND: "Sorry but a date we doesn't parse is found. Please write this regex.",
            MSG_TEXT_TITLE_BODY: "Body",
            MSG_TEXT_TITLE_RESULTS: "Result",
            MSG_TEXT_TITLE_R_H1: "Title",
            MSG_TEXT_TITLE_R_URL: "URL",
            MSG_TEXT_TITLE_R_DT: "Date",
            MSG_TEXT_TITLE_R_TXT: "Text",
            MSG_TEXT_TITLE_R_IMG: "Image",
            MSG_TEXT_TITLE_R_IMG_SRC: "Image Source",
            MSG_TEXT_TITLE_R_IMG_ALT: "Image Alternative Text",
            MSG_TEXT_TITLE_R_BY: "Author",
            MSG_TEXT_TITLE_RES_NB: "Number of articles",
            MSG_TEXT_PLACEHOLDER_BODY: "Choose a search bar",
            MSG_TEXT_PLACEHOLDER_RESULTS: "Choose a results path",
            MSG_TEXT_PLACEHOLDER_R_H1: "Choose a title path",
            MSG_TEXT_PLACEHOLDER_R_URL: "Choose a link path",
            MSG_TEXT_PLACEHOLDER_R_DT: "Choose a date path",
            MSG_TEXT_PLACEHOLDER_R_TXT: "Choose a text path",
            MSG_TEXT_PLACEHOLDER_R_IMG: "Choose a image path",
            MSG_TEXT_PLACEHOLDER_R_IMG_SRC: "Choose a source image path",
            MSG_TEXT_PLACEHOLDER_R_IMG_ALT: "Choose a alternative text image path",
            MSG_TEXT_PLACEHOLDER_R_BY: "Choose a author path",
            MSG_TEXT_PLACEHOLDER_RES_NB: "Choose a number of article path",
            MSG_TIPS_BODY: "Tips : Click on the input when you see your search terms.",
            MSG_TIPS_RESULTS: "Tips : Click on one result.",
            MSG_TIPS_R_H1: "Tips : Click on title of the result.",
            MSG_TIPS_R_URL: "Tips : Click on link of the result.",
            MSG_TIPS_R_DT: "Tips : Click on date of the result.",
            MSG_TIPS_R_TXT: "Tips : Click on text preview of the result.",
            MSG_TIPS_R_IMG: "Tips : Click on image associate of the result.",
            MSG_TIPS_R_IMG_SRC: "Tips : Click on image source associate of the result.",
            MSG_TIPS_R_IMG_ALT: "Tips : Click on image alternative text associate of the result.",
            MSG_TIPS_R_BY: "Tips : Click on author(s) of the result.",
            MSG_TIPS_RES_NB: "Tips : Click on number of the result.",
            MSG_TEXT_INPUT_DISABLED: "Pass",
            MSG_TEXT_BTN_CONFIRM : "It is a good",
            MSG_TEXT_BTN_BACK : "Change element",
            MSG_TEXT_BTN_SHOW: "See element",
            MSG_TEXT_BTN_REG: "Create/Use a regex",
            MSG_TEXT_BTN_ATTR: "Choose a attribut",
            MSG_TEXT_BTN_NOT_DEFINE : "Not defined for this JSON",
            MSG_TEXT_REG_PLACEHOLDER: "Write your regex here...",
            MSG_TEXT_TOKEN_PLACEHOLDER: "Write tokens here...",
            MSG_TEXT_MODE_TITLE : "Continue with :",
            MSG_TEXT_BTN_METHOD_CSS : "Method CSS",
            MSG_TEXT_BTN_METHOD_JSON : "Method JSON",
            MSG_TEXT_BTN_METHOD_RSS : "Method RSS",
            MSG_TEXT_CONTENT_TYPE_HTML : "We detected your source is a HTML content type. We advise you to click on the button 'Method CSS'.",
            MSG_TEXT_CONTENT_TYPE_JSON : "We detected your source is a JSON content type. We advise you to click on the button 'Method JSON'."
        },
        data : {
            search_url: {type: "required", test_passed : false, value: "", hidden_value: ""},
            search_url_terms: {type: "required", test_passed : false, value: ""},
            search_url_web: {type: "optionnal", test_passed : false, value: ""},
            body: {type: "required", test_passed: false, value: "", css_class: "form_selection_body", type_content: "text", 
            type_regex:"string", regex: null, attr:null, methode:["post"]},
            results: {type: "required", test_passed: false, value: "", css_class: "form_selection_results", type_content: "text", 
            methode:["get", "post", "json"]},
            r_h1: {type: "required", test_passed: false, value: "", css_class: "form_selection_r_h1", type_content: "text",
            type_regex:"string", regex:null, attr:null, nodes: ["h1", "h2", "h3", "h4", "h5"], methode:["get", "post", "json"]},
            r_url: {type: "required", test_passed: false, value: "", css_class: "form_selection_r_url", type_content: "link", 
            type_regex:"string", regex:null, attr:null, nodes: ["a"], methode:["get", "post", "json"]},
            r_dt: {type: "required", test_passed: false, value: "", css_class: "form_selection_r_dt", type_content: "text", 
            type_regex:"list", regex:[], index_regex: -1, attr:null, nodes: ["time"], methode:["get", "post", "json"]},
            r_txt: {type: "optionnal", test_passed: false, is_passed: false, value: "", css_class: "form_selection_r_txt", 
            type_content: "text", type_regex:"string", regex:null, attr:null, nodes: ["p"], methode:["get", "post", "json"]},
            r_img: {type: "optionnal", test_passed: false, is_passed: false, value: "", css_class: "form_selection_r_img", 
            type_content: "image", type_regex:"string", regex:null, attr:null, nodes: ["img"], methode:["get", "post", "json"]},
            r_img_src: {type: "required", sub: "r_img", test_passed: true, skip: true, value: "", css_class: "form_selection_r_img", 
            type_content: "image", type_regex:"string", regex:null, attr:null, nodes: ["img"], methode:["get", "post", "json"]},
            r_img_alt: {type: "required", sub: "r_img", test_passed: true, skip: true, value: "", css_class: "form_selection_r_img", 
            type_content: "image", type_regex:"string", regex:null, attr:null, nodes: ["img"], methode:["get", "post", "json"]},
            r_by: {type: "optionnal", test_passed: false, is_passed: false, value: "", css_class: "form_selection_r_by", 
            type_content: "text", type_regex:"string", regex:null, attr:null, nodes: [""], methode:["get", "post", "json"]},
            res_nb: {type: "optionnal", test_passed: false, is_passed: false, value: "", css_class: "form_selection_res_nb", 
            type_content: "text", type_regex:"string", regex:null, attr:null, nodes: [""], methode:["get", "post", "json"]}
        },
        current_index: 0,
        old_index: null,
        form_input : [
            "body",
            "res_nb",
            "results",
            "r_h1",
            "r_url",
            "r_dt",
            "r_txt",
            "r_img",
            "r_img_src",
            "r_img_alt",
            "r_by"
        ],
        class_list : [
            "form_selection_body",
            "form_selection_results",
            "form_selection_r_h1",
            "form_selection_r_url",
            "form_selection_r_dt",
            "form_selection_r_txt",
            "form_selection_r_img",
            "form_selection_r_by",
            "form_selection_res_nb"
        ],
        sub_dom : {
            class_show_div : "show_div",
            container_node : ["div", "article"]
        },
        request : {
            raw : null,
            content_type : null
        }
    }
    var fns_global = {
        icons: {
            triangular : "&#8227;",
            question: "&#63;",
            cross: "&#215;",
            check: "&#10003;",
            pen : "&#9998;"
        },
        test_integrity : null,
        month_nb_json: null,
        request_raw: null,
        virtual_doc: null,
        source_json : null,
        lang_code_json : null,
        country_code_json : null,
        timezone_json : null,
        zone_timezone_json : null,
        new_source_user : {},
        new_source_tags_user : {},
        current_step: 1,
        nb_step: 3,
        percent : 0,
        fns_parts: [
            fns_start,
            fns_timezone,
            fns_selector
        ]
    }
	await form_init()
	//form_handler()
    //Construction du formulaire
	async function form_init() {
		fns_global.test_integrity = await mµ.exec_in_background('get_test_integrity')
        fns_global.source_json = await mµ.exec_in_background('get_raw_src')
        fns_global.lang_code_json = await fetch('json/lang_names.json')
        fns_global.lang_code_json = await fns_global.lang_code_json.json()
        fns_global.country_code_json = await fetch('json/country_code_with_name.json')
        fns_global.country_code_json = await fns_global.country_code_json.json()
        fns_global.timezone_json = await fetch("json/timezones.json")
        fns_global.timezone_json = await fns_global.timezone_json.json()
        fns_global.zone_timezone_json = await fetch("json/timezones.json")
        fns_global.zone_timezone_json = await fns_global.zone_timezone_json.json()
        fns_global.month_nb_json = await fetch('js/month_nb/month_nb.json')
        fns_global.month_nb_json = await fns_global.month_nb_json.json()
        //await create_themes_choice()
        part_start_form_init()
	}
    function add_property(obj, key, val){
        obj[`${key}`]= val
        console.log(obj)
    }
    function remove_property(obj, key){
        if( typeof(obj[`${key}`]) !== "undefined")
            delete obj[`${key}`]
        console.log(obj)
    }
    function form_addclass(id, class_name) {
        try {
            let el = document.getElementById(`${id}`)
            if(!el.classList.contains(class_name))
                el.classList.add(class_name)
        } catch(err) {
            console.error(err)
        }
    }
    function form_removeclass(id, class_name) {
        try {
            let el = document.getElementById(`${id}`)
            el.classList.remove(class_name)
        } catch(err) {
            console.error(err)
        }
    }
    function form_search_and_removeclass(class_name) {
        try {
            let els = document.getElementsByClassName(`${class_name}`)
            for(let el of els)
                form_removeclass(el.id, class_name)
        } catch(err) {

        }
    }
    function form_msg(id, msg) {
        try {
            let el = document.getElementById(`${id}`)
            el.innerHTML = msg            
        } catch(err) {
            console.error(err)
        }
    }
    function form_msg_err(id, msg) {
        try {
            let error = document.getElementById(`${id}`)
            error.innerHTML = msg            
        } catch(err) {
            console.error(err)
        }
    }
    function form_msg_tips(id, msg) {
        try {
            let tips = document.getElementById(`${id}`)
            tips.innerHTML = msg            
        } catch(err) {
            console.error(err)
        }
    }
    function form_tips(display, tips, msg) {
        form_msg(tips.id, msg)
        if(tips.style.display === "none" || tips.style.display === "") {
            form_msg(display.id, `${fns_global.icons.cross}`)
            form_display(tips.id, true)
        } else {
            form_msg(display.id,  `${fns_global.icons.question}`)
            form_display(tips.id, false)
        }
    } 
    function form_set_input(id, value) {
        try {
            let input = document.getElementById(`${id}`)
            if(input === null)
                throw new Exception(`Input with id "${id}" not found.`)
            input.value = value
            input.dispatchEvent(new Event('input'));
        } catch(err) {
            console.error(err)
        }
    }
    function form_get_input(id) {
        try {
            let input = document.getElementById(`${id}`)
            if(input === null)
                throw new Exception(`Input with id "${id}" not found.`)
            return input.value
        } catch(err) {
            console.error(err)
            return null
        }
    }
    function form_display(id, show, type="inline"){
        try {
            let el = document.getElementById(`${id}`)
            if(show) {
                el.style.display = type
            } else {
                el.style.display = "none"
            }
        } catch(err) {
            console.error(err)
        }
    }
    function form_get(id) {
        try {
            let el = document.getElementById(id)
            return el
        } catch(err) {
            console.error(err)
        }
    }
    function form_part_get_data(fns_part, input) {
        try {
            let data = fns_part.data[`${input}`]
            if(typeof(data) === "undefined")
                throw new Error(`Data with key "${input}" doesn't exist.`)
            return data
        } catch(err) {
            console.error(err)
        }
    }
    function form_input_err(id_input, id_err, msg, required=true) {
        required ? form_addclass(id_input, "form_input_required") : form_addclass(id_input, "form_input_warning")
        form_msg(id_err, msg)
    }
    function form_update_progress_bar() {
        fns_global.percent = form_calcul_gauge_progress_bar()
        form_display_gauge_progress_bar(fns_global.percent)
    }
    function form_display_gauge_progress_bar(gauge) {
        document.getElementById("progress_bar_gauge").style.width = gauge + "%"
        gauge == 0 ? document.getElementById("progress_bar_text").textContent = "" 
        : document.getElementById("progress_bar_text").textContent = gauge + "%"
    }
    function form_count_required_input(fns_part) {
        let count = 0
        for(let d of Object.values(fns_part.data)) {
            if(d.type === "required")
                count += 1
        }
        return count
    }
    function form_calcul_gauge_progress_bar() {
        let gauge = 0
        for(let fns_part of Object.values(fns_global.fns_parts)) {
            for(let d of Object.values(fns_part.data)) {
                if(d.type === "required")
                    gauge += d.test_passed ? (form_calcul_step() / form_count_required_input(fns_part)) : 0
            }
        }
        return Math.ceil(gauge)
    }
    function form_calcul_step() {
        return (1 / fns_global.nb_step) * 100
    }
    function form_toggle(fieldset) {
        let icon = fieldset.querySelector("legend > span")
        icon.classList.toggle("form_icon_fieldset_show")
        icon.classList.toggle("form_icon_fieldset_hide")
        fieldset.classList.toggle("form_fieldset_hide")
    }
    /**
     * 
     * @param {*} params : List parameters for build template.
     * One param represent a HTML element
    *   {
    *       node : The tag of node ("div", "a", "p", etc...). This param is required.
    *       parent : Index of parent in the list params. Index start in 0.
    *       condition : Condition boolean to create node.
    *       all_other_HTML_attributs...  
    *   }
     *  
     */
    function form_builder(params) {
        let div = document.createElement("div")
        let black_list_key = ["node", "parent", "condition"]
        let nodes = []
        for(let param of params) {
            if(typeof(param.node) === "undefined")
                throw new Exception("'node' parameter is not defined.")
            if(typeof(param.condition) !== "undefined" && !param.condition) 
                continue
            let node = document.createElement(param.node)
            for( let [k, v] of  Object.entries(param)){
                if(black_list_key.includes(k))
                    continue
                try {
                    if(k === "classList")
                        v.forEach( (cls) => { node.classList.add(cls) })
                    else if(k === "textContent")
                        node.textContent = v
                    else if(k === "innerHTML")
                        node.innerHTML = v
                    else
                        node.setAttribute(k, v)
                } catch(err) {
                    console.error(err)
                }
            }
            nodes.push(node)
        }
        let index = 0
        for(let param of params) {
            if(typeof(param.condition) === "undefined" || param.condition) {
                if(typeof(param.parent) === "undefined") {
                    div.appendChild(nodes[index])
                } else {
                    let parent = nodes[param.parent]
                    if(typeof(parent) === "undefined")
                        div.appendChild(nodes[index])
                    else
                        parent.appendChild(nodes[index])
                }
                index += 1
            }

        }
        let obj = {
            nodes: nodes,
            nb_nodes: nodes.length,
            dom : div,
            params : params
        }
        return obj
    }
    
    /**
     * Function to insert a node in the DOM.
     * @param el : Node to insetrt in DOM.
     * @param parent : Node parent.
     * @param ref : Child of parent to insert before him.
     */
    function form_insert(el, parent, ref=null) {
        try {
            ref === null ? parent.appendChild(el) : parent.insertBefore(el, ref)
        } catch (err) {
            console.error(err)
        }
    }
    function form_remove(id) { 
        try {
            let el = document.getElementById(id)
            if(el !== null)
                el.parentNode.removeChild(el)
        } catch (err) {
            console.error(err)
        }
    }
    function part_start_form_init() {
        fns_start.doc.fieldset = form_get("part_start_form_id")
        fns_start.doc.legend = form_get("legend_part_start_form")
        fns_start.doc.btn_next = form_get("part_start_form_btn_next")
        form_msg("legend_part_start_form_icon", `${fns_global.icons.triangular}`)
        form_msg("legend_part_start_form_title", `${fns_start.title}`)
        part_start_form_builder()
        form_display(fns_start.doc.fieldset.id, true, "block")
        part_start_form_event()
    }
    function part_start_form_builder() {
        let temp_name = [
            {node: "div", id: "name_id"},
                {node: "div", classList:["input_container"], parent:0},
                    {node: "input", id: "name_input", classList: ["form_input_text"], 
                    placeholder: "Name of the new source...", title: "Ex : New Source", type:"text", 
                    value:  fns_start.data.tags_name.value, parent:1},
                    {node: "text", textContent: " "},
                    {node: "p", id: "name_tips", classList: ["form_input_tips"], parent:1},
                    {node: "text", textContent: " "},
                    {node : "button", id:"name_display_tips", classList:["a", "btn"], innerHTML: `${fns_global.icons.question}`, parent:1},
                {node: "p", id:"name_err", classList: ["form_input_error"], parent:0}
        ]
        let temp_headline = [
            {node: "div", id: "headline_url_id"},
                {node: "div", classList:["input_container"], parent:0},
                    {node: "input", id: "headline_url_input", classList: ["form_input_text"],
                    placeholder: "Adress (https://...)", title: "Ex : https://www.custom-source.eu", 
                    type:"text", value:  fns_start.data.headline.value, parent:1},
                    {node: "text", textContent: " "},
                    {node: "p", id: "headline_url_tips", classList: ["form_input_tips"], parent:1},
                    {node: "text", textContent: " "},
                    {node : "button", id:"headline_url_display_tips", classList:["a", "btn"], innerHTML: `${fns_global.icons.question}`, parent:1},
                {node: "p", id:"headline_url_err", classList: ["form_input_error"], parent:0}
        ]
        let temp_favicon = [
            {node: "div", id: "favicon_url_id"},
                {node: "div", classList:["input_container"], parent:0},
                    {node: "input", id: "favicon_url_input", classList: ["form_input_text", "form_warning"], 
                    placeholder: "Write a url of favicon (optionnal)...", title: "Ex : https://www.custom-source.eu/custom/favicon.png", 
                    type:"text", value: fns_start.data.favicon.value, parent:1},
                    {node: "text", textContent: " "},
                    {node: "img", id: "favicon_url_img", classList: ["form_favicon"], style:"display: none;", parent:1},
                    {node: "text", textContent: " "},
                    {node: "p", id: "favicon_url_tips", classList: ["form_input_tips"], parent:1},
                    {node: "text", textContent: " "},
                    {node : "button", id:"favicon_url_display_tips", classList:["a", "btn"], innerHTML: `${fns_global.icons.question}`, parent:1},
                {node: "p", id:"favicon_url_err", classList: ["form_input_error"], parent:0}
        ]
        let b_name = form_builder(temp_name)
        let b_headline = form_builder(temp_headline)
        let b_favicon = form_builder(temp_favicon)
        form_insert(b_name.dom.firstChild, fns_start.doc.fieldset, fns_start.doc.btn_next)
        form_insert(b_headline.dom.firstChild, fns_start.doc.fieldset, fns_start.doc.btn_next)
        form_insert(b_favicon.dom.firstChild, fns_start.doc.fieldset, fns_start.doc.btn_next)
        fns_start.doc.tags_name = form_get("name_id")
        fns_start.doc.headline = form_get("headline_url_id")
        fns_start.doc.favicon = form_get("favicon_url_id")
    }
    function part_start_form_event() {
        mµ.load_src_in_reserved_name(fns_global.source_json)
        fns_start.doc.legend.addEventListener("click", (evt) => {
            let target = fns_start.doc.fieldset
            form_toggle(target)
        })
        form_get("name_input").addEventListener("input", (evt) => {
            let value = µ.triw(evt.target.value)
            if(value === ""){
                form_input_err("name_input", "name_err", fns_start.text.MSG_ERROR_VALUE_EMPTY)
                part_start_form_update_data("tags_name", false, value)
                part_start_form_validator()
                return
            }
            let res = mµ.test_name_unicity(value)
            if(!res) {
                form_input_err("name_input", "name_err",  fns_start.text.MSG_ERROR_NAME_ALREADY_EXIST)
                part_start_form_update_data("tags_name", false, value)
                part_start_form_validator()
                return
            }
            form_removeclass("name_input", "form_input_required")
            form_msg("name_err", "")
            add_property(fns_global.new_source_tags_user, "name", value)
            part_start_form_update_data("tags_name", true, value)
            part_start_form_validator()
        })
        form_get("name_display_tips").addEventListener("click", (evt) => {
            let display = form_get("name_display_tips")
            let tips = form_get("name_tips")
            form_tips(display, tips, fns_start.text.MSG_TIPS_TAGS_NAME)
        })
        form_get("headline_url_input").addEventListener("input", async (evt) => {
            let value = µ.triw(evt.target.value)
            if(value === "") {
                form_input_err("headline_url_input", "headline_url_err",  fns_start.text.MSG_ERROR_VALUE_EMPTY)
                part_start_form_update_data("headline", false, value)
                part_start_form_validator()
                return
            }
            await mµ.request_permissions(["<all_urls>"])
            let res = mµ.test_attribut(
                {}, 
                fns_global.test_integrity, 
                {headline_url : value, tags : {name: fns_global.new_source_tags_user["name"]}}, 
                "headline_url"
            )
            if(!res[0]) {
                form_input_err("headline_url_input", "headline_url_err",  fns_start.text.MSG_ERROR_URL_NOT_FOUND)
                part_start_form_update_data("headline", false, value)
                part_start_form_validator()
                return
            }
            form_removeclass("headline_url_input", "form_input_required")
            form_msg("headline_url_err", "")
            add_property(fns_global.new_source_user, "headline_url", value)
            part_start_form_try_get_favicon(value)
            part_start_form_update_data("headline", true, value)
            part_start_form_validator()
        })
        form_get("headline_url_display_tips").addEventListener("click", (evt) => {
            let display = form_get("headline_url_display_tips")
            let tips = form_get("headline_url_tips")
            form_tips(display, tips, fns_start.text.MSG_TIPS_HEADLINE_URL)
        })
        form_get("favicon_url_input").addEventListener("input", async (evt) => {
            let value =  µ.triw(evt.target.value)
            if(value === "") {
                form_display("favicon_url_img", false)
                form_removeclass("favicon_url_input", "form_input_required")
                part_start_form_update_data("favicon", false, value)
                part_start_form_validator()
                return
            }
            let res = mµ.test_attribut(
                {}, 
                fns_global.test_integrity, 
                {favicon_url : value, tags : {name: fns_global.new_source_tags_user["name"]}}, 
                "favicon_url"
            )
            if(res[1][0]) {
                form_display("favicon_url_img", false)
                form_input_err("favicon_url_input", "favicon_url_err", fns_start.text.MSG_ERROR_FAVICON_NOT_FOUND, false)
                part_start_form_update_data("favicon", false, value)
                part_start_form_validator()
                return
            }
            form_removeclass("favicon_url_input", "form_input_warning")
            form_msg("favicon_url_err", "")
            form_get("favicon_url_img").src = value
            form_display("favicon_url_img", true)
            add_property(fns_global.new_source_user, "favicon_url", value)
            part_start_form_update_data("favicon", true, value)
        })
        form_get("favicon_url_display_tips").addEventListener("click", (evt) => {
            let display = form_get("favicon_url_display_tips")
            let tips = form_get("favicon_url_tips")
            form_tips(display, tips, fns_start.text.MSG_TIPS_FAVICON)
        })
        form_get("part_start_form_btn").addEventListener("click", (evt) => {
            if(fns_start.completed && !fns_start.passed) {
                part_start_form_next_part()
            }
        })
    }
    function part_start_form_next_part() {
        fns_start.passed = true
        form_toggle(fns_start.doc.fieldset)
        part_timezone_init()
    }
    function part_start_form_update_data(input, res_test, value) {
        fns_start.data[`${input}`].value = value
        fns_start.data[`${input}`].test_passed = res_test
    }
    function part_start_form_try_get_favicon(url) {
        let favicon = µ.get_favicon(url)
        if(favicon !== "") {
            if(form_get_input("favicon_url_input") === "")
                form_set_input("favicon_url_input", favicon)
        }
    }
    function part_start_form_validator() {
        let valid = true
        for(let d of Object.values(fns_start.data)) {
            if(d.type === "required"){
                valid = valid && d.test_passed
            }
        }
        fns_start.completed = valid
        fns_start.completed ? form_display("part_start_form_btn_next", true) : form_display("part_start_form_btn_next", false)
        form_update_progress_bar()
    }
    function part_timezone_init() {
        fns_timezone.doc.fieldset = document.getElementById("part_timezone_id")
        fns_timezone.doc.legend = document.getElementById("legend_part_timezone")
        fns_timezone.doc.tags_lang = document.getElementById("lang_id")
        fns_timezone.doc.tags_country = document.getElementById("country_id")
        fns_timezone.doc.tags_timezone = document.getElementById("tz_id")
        fns_timezone.doc.check_all_timezone = document.getElementById("check_all_tz_id")
        fns_timezone.doc.btn_next = document.getElementById("part_timezone_btn_next")
        form_msg("legend_part_timezone_icon", `${fns_global.icons.triangular}`)
        form_msg("legend_part_timezone_title", `${fns_timezone.title}`)
        part_timezone_create_lang_choice()
        part_timezone_create_country_choice()
        part_timezone_create_timezone_choice()
        fns_timezone.choices.lang.setChoiceByValue("")
        fns_timezone.choices.country.setChoiceByValue("")
        fns_timezone.choices.timezone.setChoiceByValue("")
        form_get("check_all_tz").checked = false
        form_display(fns_timezone.doc.fieldset.id, true, "block")
        part_timezone_event()
        let url = fns_global.new_source_user["headline_url"]
        if(url) 
            part_timezone_try_get_lang(url)
    }
    function part_timezone_create_lang_choice(){
        let select = fns_timezone.doc.tags_lang.querySelector("#lang_select")
        for(let lang of Object.entries(fns_global.lang_code_json)) {
            if(lang[1] !== "error") {
                let option = document.createElement('option')
                option.text = `${lang[1]}`
                option.value = lang[0]
                select.add(option)
            }
        }
        let default_option = document.createElement('option')
        default_option.text = 'Choose a lang of source'
        default_option.value = ""
        select.add(default_option, 0)
        let select_opt = {
            resetScrollPosition: false,
            duplicateItemsAllowed: false,
            searchResultLimit: 8,
            shouldSort : false,
            searchFields: ['label'],
            classNames: {
                containerInner: "choices__inner form_required"
            }
        }
        fns_timezone.choices.lang = new Choices(select, Object.assign(select_opt))
        fns_timezone.choices.lang.setChoiceByValue(default_option.value)
    }
    
    function part_timezone_create_country_choice(){
        let select = document.getElementById('country_select')
        for(let country of Object.entries(fns_global.country_code_json)) {
            let option = document.createElement('option')
            option.text = `${country[1]}`
            option.value = country[0]
            select.add(option)
        }
        let default_option = document.createElement('option')
        default_option.text = 'Choose a country of source'
        default_option.value = ""
        select.add(default_option, 0)
        let select_opt = {
            resetScrollPosition: false,
            duplicateItemsAllowed: false,
            searchResultLimit: 8,
            shouldSort : false,
            searchFields: ['label'],
            classNames: {
                containerInner: "choices__inner form_required"
            }
        }
        fns_timezone.choices.country = new Choices(select, Object.assign(select_opt))
        fns_timezone.choices.country.setChoiceByValue(default_option.value)
    }
    function part_timezone_create_timezone_choice() {
        let select = document.getElementById('tz_select')
        for(let cn of Object.entries(fns_global.zone_timezone_json)) {
            let option = document.createElement('option')
            option.text = `${cn[1]}`
            option.value = cn[1]
            select.add(option)
        }
        let default_option = document.createElement('option')
        default_option.text = 'Choose a timezone of source'
        default_option.value = ""
        select.add(default_option, 0)
        let select_opt = {
            resetScrollPosition: false,
            duplicateItemsAllowed: false,
            searchResultLimit: 8,
            shouldSort : false,
            searchFields: ['label'],
            classNames: {
                containerInner: "choices__inner form_required"
            }
        }
        fns_timezone.choices.timezone = new Choices(select, Object.assign(select_opt))
        fns_timezone.choices.timezone.setChoiceByValue(default_option.value)
    }
    function part_timezone_event() {
        fns_timezone.doc.legend.addEventListener("click", (evt) => {
            let target = fns_timezone.doc.fieldset
            form_toggle(target)
        })
        fns_timezone.choices.lang.passedElement.element.addEventListener("addItem", (evt) => {
            let val = evt.detail.value
            fns_timezone.data.tags_lang.value = val
            fns_timezone.data.tags_lang.test_passed = val !== ""
            if(!fns_timezone.data.tags_lang.test_passed) {
                let el = fns_timezone.choices.lang.containerInner.element
                if(!el.classList.contains("form_input_required"))
                    el.classList.add("form_input_required")
                form_msg("lang_err", fns_timezone.text.MSG_ERROR_VALUE_EMPTY)
                part_timezone_validator()
                return
            }
            let res = mµ.test_attribut(
                {}, 
                fns_global.test_integrity, 
                {tags : {name: fns_global.new_source_tags_user["name"], lang: val}}, 
                "tags.lang"
            )
            if(!res[0]) {
                let el = fns_timezone.choices.lang.containerInner.element
                if(!el.classList.contains("form_input_required"))
                    el.classList.add("form_input_required")
                form_msg("lang_err", fns_timezone.text.MSG_ERROR_VALUE_IS_INCORRECT)
                part_timezone_validator()
                return
            }
            fns_timezone.choices.lang.containerInner.element.classList.remove("form_input_required")
            form_msg("lang_err", "")
            add_property(fns_global.new_source_tags_user, "lang", val)
            if(fns_timezone.choices.country.getValue().value === "")
                fns_timezone.choices.country.setChoiceByValue(val)
            part_timezone_validator()
        })
        fns_timezone.choices.country.passedElement.element.addEventListener("addItem", (evt) => {
            let val = evt.detail.value
            fns_timezone.data.tags_country.value = val
            fns_timezone.data.tags_country.test_passed = val !== ""
            if(!fns_timezone.data.tags_country.test_passed) {
                let el = fns_timezone.choices.country.containerInner.element
                if(!el.classList.contains("form_input_required"))
                    el.classList.add("form_input_required")
                form_msg("country_err", fns_timezone.text.MSG_ERROR_VALUE_EMPTY)
                part_timezone_validator()
                return
            }
            let res = mµ.test_attribut(
                {}, 
                fns_global.test_integrity, 
                {tags : {name: fns_global.new_source_tags_user["name"], country: val}}, 
                "tags.country"
            )
            if(!res[0]) {
                let el = fns_timezone.choices.country.containerInner.element
                if(!el.classList.contains("form_input_required"))
                    el.classList.add("form_input_required")
                form_msg("country_err", fns_timezone.text.MSG_ERROR_VALUE_IS_INCORRECT)
                part_timezone_validator()
                return
            }
            fns_timezone.choices.country.containerInner.element.classList.remove("form_input_required")
            form_msg("country_err", "")
            add_property(fns_global.new_source_tags_user, "country", val)
            part_timezone_update_timezone_choice(val)
            part_timezone_validator()
		})
        fns_timezone.choices.timezone.passedElement.element.addEventListener("addItem", (evt) => {
            let val = evt.detail.value
            fns_timezone.data.tags_timezone.value = val
            fns_timezone.data.tags_timezone.test_passed = val !== ""
            if(!fns_timezone.data.tags_timezone.test_passed) {
                console.log(val,fns_timezone.data.tags_timezone.test_passed )
                let el = fns_timezone.choices.timezone.containerInner.element
                if(!el.classList.contains("form_input_required"))
                    el.classList.add("form_input_required")
                form_msg("tz_select_err", fns_timezone.text.MSG_ERROR_VALUE_EMPTY)
                part_timezone_validator()
                return
            }
            let res = mµ.test_attribut(
                {}, 
                fns_global.test_integrity, 
                {tags : {name: fns_global.new_source_tags_user["name"], tz: val}}, 
                "tags.tz"
            )
            if(!res[0]) {
                let el = fns_timezone.choices.timezone.containerInner.element
                if(!el.classList.contains("form_input_required"))
                    el.classList.add("form_input_required")
                form_msg("tz_select_err", fns_timezone.text.MSG_ERROR_VALUE_IS_INCORRECT)
                part_timezone_validator()
                return
            }
            fns_timezone.choices.timezone.containerInner.element.classList.remove("form_input_required")
            form_msg("tz_select_err", "")
            add_property(fns_global.new_source_tags_user, "tz", val)
            part_timezone_validator()
        })
        form_get("check_all_tz").addEventListener("click", (evt) => {
            evt.target.checked ? part_timezone_update_timezone_choice() : 
            part_timezone_update_timezone_choice(fns_timezone.choices.country.getValue().value)
        })
        form_get("part_timezone_btn").addEventListener("click", (evt) => {
            if(fns_timezone.completed) {
                form_toggle(fns_timezone.doc.fieldset)
                part_selector_init()
            }
        })
    }
    function part_timezone_try_get_lang(url) { 
        let re = new RegExp('^[a-z]{2,3}/?$');
        let part = url.split(".")
        let find = re.exec(part[part.length - 1])
        let ignore = ["com", "org"]
        if(find) {
            find = find[0]
            if(find.endsWith("/"))
                find = find.replace("/", "")
            if(Object.keys(fns_global.lang_code_json).includes(find) && !ignore.includes(find))
                fns_timezone.choices.lang.setChoiceByValue(find)
        } 
    }
    function part_timezone_update_timezone_choice(country_code) {
        let json_tz
        if(country_code === undefined)
            json_tz = fns_global.zone_timezone_json
        else 
            json_tz = fns_global.timezone_json[`${country_code}`]
        let choices = [{label: 'Choose a timezone of source', value: "", placeholder: true}]
        for(let v of Object.values(json_tz)){
            if(v instanceof Array)
                v = v[0]
            choices.push({label : v, value: v})
        }
        fns_timezone.choices.timezone.clearStore()
        fns_timezone.choices.timezone.setChoices(choices, "value", "label", false)
        fns_timezone.choices.timezone.setChoiceByValue(choices.filter((el) => {return el.value !== ""})[0].value)
    }
    function part_timezone_validator() {
        let valid = true
        for(let d of Object.values(fns_timezone.data)) {
            if(d.type === "required"){
                valid = valid && d.test_passed
            }
        }
        fns_timezone.completed = valid
        fns_timezone.completed ? form_display("part_timezone_btn_next", true) : form_display("part_timezone_btn_next", false)
        form_update_progress_bar()
    }
    function part_selector_init() {
        fns_selector.doc.fieldset = document.getElementById("part_selector_id")
        fns_selector.doc.legend = document.getElementById("legend_part_selector")
        fns_selector.doc.search_url = document.getElementById("search_url_id")
        fns_selector.doc.sub_dom = document.getElementById("sub_dom")
        fns_selector.doc.search_url.querySelector("#search_url_input").value = ""
        fns_selector.doc.search_url.querySelector("#search_url_terms_input").value = ""
        fns_selector.doc.search_url.querySelector("#search_url_web_input").value = ""
        form_msg("legend_part_selector_icon", `${fns_global.icons.triangular}`)
        form_msg("legend_part_selector_title", `${fns_selector.title}`)
        form_display(fns_selector.doc.fieldset.id, true, "block")
        part_selector_event()
    }
    function part_selector_event() {
        fns_selector.doc.legend.addEventListener("click", (evt) => {
            let target = fns_selector.doc.fieldset
            form_toggle(target)
        })
        form_get("search_url_input").addEventListener("input", (evt) => {
            let val = µ.triw(evt.target.value)
            fns_selector.data.search_url.value = val
            if(val === "") {
                form_input_err("search_url_input", "search_url_err", fns_selector.text.MSG_ERROR_VALUE_EMPTY)
                fns_selector.data.search_url.test_passed = false
                part_selector_validator()
                return
            }
            form_removeclass("search_url_input", "form_input_required")
            form_msg("search_url_err", "")
            fns_selector.data.search_url.test_passed = true
            part_selector_validator()
        })
        form_get("search_url_terms_input").addEventListener("input", (evt) => {
            let val = µ.dry_triw(evt.target.value)
            fns_selector.data.search_url_terms.value = val
            if(val === "") {
                form_input_err("search_url_terms_input", "search_url_err", fns_selector.text.MSG_ERROR_VALUE_EMPTY)
                fns_selector.data.search_url_terms.test_passed = false
                part_selector_validator()
                return
            }
            let match = part_selector_try_find_search_terms(fns_selector.data.search_url.value, val)
            if(match === null && fns_selector.mode !== "post") {
                form_input_err("search_url_terms_input", "search_url_err", fns_selector.text.MSG_ERROR_BAD_SEARCH_TERM)
                fns_selector.data.search_url_terms.test_passed = false
                form_display("search_url_method_post", true)
                form_get("search_url_method_post").addEventListener("click", (evt) => {
                    fns_selector.mode = "post"
                    form_get("search_url_terms_input").dispatchEvent(new Event("input"))
                })
                part_selector_validator()
                return
            }
            form_removeclass("search_url_terms_input", "form_input_required")
            form_msg("search_url_err", "")
            form_display("search_url_method_post", false)
            fns_selector.data.search_url_terms.test_passed = true
            part_selector_validator()
        })
        form_get("search_url_btn").addEventListener("click", async (evt) => {
            if(fns_selector.data.search_url.test_passed && fns_selector.data.search_url_terms.test_passed) {
                //part_selector_reset()
                let searchUrl = fns_selector.data.search_url.value 
                let terms = fns_selector.data.search_url_terms.value
                fns_selector.data.search_url.hidden_value = searchUrl.replace(terms, "{}")
                add_property(fns_global.new_source_user, "search_url", fns_selector.data.search_url.hidden_value)
                await part_selector_fetch(searchUrl)
                let template = [
                    {node : "div", id:"mode_selection", classList : ["input_container"], style: "margin-top: 0.5em;"},
                        {node : "p", id: "text_type_content_detected", classList : ["form_text_info"],
                        textContent : fns_selector.text.MSG_TEXT_CONTENT_TYPE_HTML, 
                        condition: fns_selector.request.content_type === "html", parent: 0},
                        {node : "p", id: "text_type_content_detected", classList : ["form_text_info"],
                        textContent : fns_selector.text.MSG_TEXT_CONTENT_TYPE_JSON, 
                        condition: fns_selector.request.content_type === "json", parent: 0},
                        {node : "span", id: "mode_title", 
                        textContent : fns_selector.text.MSG_TEXT_MODE_TITLE, parent : 0},
                        {node : "button", id:"mode_css", classList : ["a", "btn", "form_selection_input_btn"], 
                        textContent: fns_selector.text.MSG_TEXT_BTN_METHOD_CSS, parent: 0},
                        {node : "button", id:"mode_json", classList : ["a", "btn", "form_selection_input_btn"], 
                        textContent: fns_selector.text.MSG_TEXT_BTN_METHOD_JSON, parent: 0},
                        {node : "button", id:"mode_rss", classList : ["a", "btn", "form_selection_input_btn"], 
                        textContent: fns_selector.text.MSG_TEXT_BTN_METHOD_RSS, parent: 0},
                ]
                let builder = form_builder(template)
                let el = builder.dom.firstChild
                form_insert(el, fns_selector.doc.fieldset, form_get("sub_dom"))
                form_get("mode_css").addEventListener("click", (evt) => {
                    fns_selector.mode = "get"
                    form_search_and_removeclass("form_mode_selected")
                    form_addclass(evt.target.id, "form_mode_selected")
                    //form_msg("legend_part_selector_title", `${fns_selector.title_css}`)
                    //remove_property(fns_global.new_source_user, `search_url_web`)
                    //form_get("search_url_web_input").style.display = "none"
                    part_selector_before_sub_dom()
                })
                form_get("mode_json").addEventListener("click", (evt) => {
                    fns_selector.mode = "json"
                    form_search_and_removeclass("form_mode_selected")
                    form_addclass(evt.target.id, "form_mode_selected")
                    //form_msg("legend_part_selector_title", `${fns_selector.title_json}`)
                    //form_get("search_url_web_input").style.display = "inline"
                    part_selector_before_sub_dom()
                })
                fns_selector.request.content_type === "html" ? 
                form_addclass("mode_css", "form_input_warning") : form_addclass("mode_json", "form_input_warning") 
            }
        })
    }
    async function part_selector_before_sub_dom() {
        fns_selector.request.content_type === "html" ? 
        form_removeclass("mode_css", "form_input_warning") : form_removeclass("mode_json", "form_input_warning") 
        form_remove("text_type_content_detected")
        form_display("sub_dom", false)
        part_selector_reset()
        let resp = await part_selector_try_get_virtual_doc()
        if(!resp) {
            let template = [
                {node: "p", id: "request_erro", textContent: "Sorry but this method trigger a error. Please try other method", 
                style:"color: red; margin:auto;"}
            ]
            let builder = form_builder(template)
            form_insert(builder.dom.firstChild, fns_selector.doc.fieldset, form_get("sub_dom"))
            return
        } else {
            form_remove("request_erro")
        }
        if(fns_selector.mode === "get" ||  fns_selector.mode === "post") {
            let body = fns_global.virtual_doc.body
            body = new DOMParser().parseFromString( body.innerHTML, "text/html")
            body = body.children[0].children[1]
            part_selector_depth_clean_virtual_doc(body.childNodes)
            part_selector_child_have_text_content(body.childNodes)
            part_selector_set_sub_dom(body)
        } else if(fns_selector.mode === "json") {
            let search_url_web = µ.dry_triw(form_get("search_url_web_input").value)
            if(search_url_web === "") {
                add_property(fns_global.new_source_user, `search_url_web`, fns_start.data.headline.value)
                form_get("search_url_web_input").value = fns_start.data.headline.value
            } else {
                add_property(fns_global.new_source_user, `search_url_web`, search_url_web)
            }
            let json = fns_global.virtual_doc
            part_selector_set_sub_dom(json)
        }
        form_display("sub_dom", true, "block")
        fns_selector[`current_index`] = 0
        console.log("part_selector_before_sub_dom")
        part_selector_build_input()
    }
    function part_selector_reset() {
        fns_selector[`current_index`] = 0
        fns_selector[`old_index`] = null
        fns_selector[`completed`] = false
        console.log("RESET", fns_selector)
        for(let input of fns_selector.form_input) {
            let child = form_get(`${input}_id`)
            if(child !== null) {
                form_remove(child.id)
                let data = fns_selector.data[`${input}`]
                if(input === "r_dt") {
                    let count = 0
                    for(let dt of data.regex) {
                        remove_property(fns_global.new_source_user, `${input}_fmt_${count}`)
                        count += 1
                    }
                }
                part_selector_reset_data(input)
                remove_property(fns_global.new_source_user, `${input}`)
                remove_property(fns_global.new_source_user, `${input}_re`)
                remove_property(fns_global.new_source_user, `${input}_attr`)
                remove_property(fns_global.new_source_user, `${input}_xpath`)
            }
        }
    }
    function part_selector_reset_data(input) {
        let data = fns_selector.data[`${input}`]
        data.value = ""
        if(typeof(data.regex) !== "undefined")
            data.type_regex === "string" ? data.regex = null : data.regex = []
        if(typeof(data.attr) !== "undefined")
            data.attr = null
        if (typeof(data.sub) !== "undefined") {
            data.test_passed = true
            data.skip = true
        } else {
            data.test_passed = false 
        }
    }
    function part_selector_try_find_search_terms(url, val) {
        let separator = ["=", "/"]
        let spaces = ["+", "%20"]
        try {
            let url = new URL(url)
            let tmp = url.search.split("=")
            let term = tmp[tmp.length - 1]
            let match = null
            for(let space of spaces) {
                let tmp_val = new RegExp(`${val.replaceAll(" ", `${space === '+' ? `\\${space}` : space}`)}`)
                match = term.match(tmp_val)
                if(match !== [] && match !== null)
                    return match
            }
            return null
        } catch(err) {}
        for(let sep of separator) {
            let tmp = url.split(sep)
            let match = null
            for(let term of tmp) {
                for(let space of spaces) {
                    let tmp_val = new RegExp(`${val.replaceAll(" ", `${space === '+' ? `\\${space}` : space}`)}`)
                    match = term.match(tmp_val)
                    if(match !== [] && match !== null)
                        break
                }
                if(match === [] || match === null)
                    continue
                return match
            }
        }
        return null
    }
    async function part_selector_fetch(url) {
        let raw = await fetch(url, {
            method: 'GET',
            headers: { 'Content-Type': 'application/x-www-form-urlencoded'}
        })
        fns_selector.request.raw = raw
        fns_selector.request.content_type = await part_selector_get_response_content_type()
    }
    async function part_selector_get_response_content_type() {
        for(let info of fns_selector.request.raw.headers.entries()) {
            if(info[0] === "content-type") {
                if(info[1] === "application/json")
                    return "json"
                else if(info[1].match(/text\/html/) !== null)
                    return "html"
            }
        }
        return "html" //Default value
    }
    async function part_selector_try_get_virtual_doc() {
        let url = fns_selector.data.search_url.value
        await part_selector_fetch(url)
        if(fns_selector.mode === "get" ||  fns_selector.mode === "post") {
            try {
                let raw_text = await fns_selector.request.raw.text()
                fns_global.virtual_doc = new DOMParser().parseFromString(raw_text, 'text/html')
            } catch(err) {
                return false
            }
        } else if (fns_selector.mode === "json") {
            try {
                let raw_json = await fns_selector.request.raw.json()
                console.log(raw_json)
                fns_global.virtual_doc = raw_json
            } catch(err) {
                return false
            }
        }
        return true
    }
    function part_selector_depth_clean_virtual_doc(childs){
        let ignoreTags = ["meta", "link", "svg"]
        let reg = new RegExp(/on.*/)
        for(let c of childs){
            if(c.childNodes.length > 0)
                part_selector_depth_clean_virtual_doc(c.childNodes)
            if(c.localName === "a") {
                c.disabled = true
                c.removeAttribute("href")
            }
            if(ignoreTags.includes(c.localName))
                c.parentNode.removeChild(c)
            if(c.localName === "script"){
                c.removeAttribute("src")
                c.textContent = ""
            }
            if(c.localName === "input" || c.localName === "button"){
                c.removeAttribute("type")
                if(c.localName === "button")
                    c.disabled = true
            }
            if(typeof(c.attributes) !== "undefined") {
                for(let attr of c.attributes) {
                    if(attr.localName.match(reg) !== []) {
                        c.removeAttribute(attr)
                    }
                }
            }
        }

    }
    function part_selector_add_class_in_first_container(node) {
        while(node.localName !== "body") {
            node = node.parentNode
            if(fns_selector.sub_dom.container_node.includes(node.localName) && node.childNodes.length > 1 && !node.classList.contains("show_div")) {
                node.classList.add("show_div")
                break
            }
        }
    }
    function part_selector_child_have_text_content(childs) {
        let count = 0
        while(count < childs.length) {
            let child = childs[count]
            if(child.nodeName === "#comment" || child.nodeName === "#text") {
                count += 1 
                continue
            }
            if(µ.dry_triw(child.textContent) !== "" && !fns_selector.sub_dom.container_node.includes(child.localName) )
            part_selector_add_class_in_first_container(child)
            if(child.childNodes.length > 0)
                childs = [...childs, ...child.childNodes]
            count += 1
        }
    }
    function part_selector_build_input() {
        console.log("FNS_SELECTOR BUILD", fns_selector.current_index)
        console.log("BUIDL", fns_selector)
        if(fns_selector.current_index < fns_selector.form_input.length) {
            let input = fns_selector.form_input[fns_selector.current_index]
            let data = form_part_get_data(fns_selector, input)
            console.log(input, data, !data.methode.includes(fns_selector.mode))
            if(!data.methode.includes(fns_selector.mode)) {
                fns_selector.current_index += 1
                part_selector_build_input()
                return
            }
            console.log(input, data, typeof(data.skip) !== "undefined" && data.skip)
            if(typeof(data.skip) !== "undefined" && data.skip) {
                fns_selector.current_index += 1
                part_selector_build_input()
                return
            }
            let template  = null
            if(data.type === "required") {
                template = [
                    {node: "div", id:`${input}_id`, name:`${input}`, classList: ["form_selection_container"], style:"display: none"},
                        {node:"div", classList: [`input_container`], parent:0},
                            {node: "label", id:`${input}_text`, parent:1},
                                {node: "span", style:"font-weight: bold;", 
                                textContent:fns_selector.text[`MSG_TEXT_TITLE_${input.toUpperCase()}`],
                                classList: [`${data.css_class}_text`], parent: 2},
                                {node: "text", textContent:" : ", parent: 2},
                                {node: "span", id:`${input}_final`, classList: ["form_selection_text"], 
                                parent: 2},
                            {node: "input", id: `${input}_input`, 
                            placeholder:fns_selector.text[`MSG_TEXT_PLACEHOLDER_${input.toUpperCase()}`], 
                            title:"Ex : https://www.custom-source.eu", type:"text", 
                            classList: ["form_input_text"], value: data.value, parent:1},
                            {node:"button", id:`${input}_btn_confirm`, classList: ["btn", "a", "form_selection_input_btn"], 
                            textContent: fns_selector.text.MSG_TEXT_BTN_CONFIRM, parent:1},
                            {node:"button", id:`${input}_btn_back`, classList: ["btn", "a", "form_selection_input_btn"], 
                            style: "display: none;", textContent: fns_selector.text.MSG_TEXT_BTN_BACK, parent:1},
                            {node:"button", id:`${input}_btn_show`, classList: ["btn", "a", "form_selection_input_btn"], 
                            textContent: fns_selector.text.MSG_TEXT_BTN_SHOW, parent:1},
                            {node:"button", id:`${input}_btn_not_define`, classList: ["btn", "a", "form_selection_input_btn"],  
                            textContent: fns_selector.text.MSG_TEXT_BTN_NOT_DEFINE, 
                            condition: fns_selector.mode === "json" && input === "results", parent:1},
                            {node:"button", id:`${input}_btn_attr`, classList: ["btn", "a", "form_selection_input_btn"],  
                            style: "display: none;", textContent: fns_selector.text.MSG_TEXT_BTN_ATTR, parent:1},
                            {node: "span", id:`${input}_final_attr`, classList: ["form_selection_text"], 
                            parent: 1},
                            {node:"button", id:`${input}_btn_reg`, classList: ["btn", "a", "form_selection_input_btn"],  
                            style: "display: none;", textContent: fns_selector.text.MSG_TEXT_BTN_REG, parent:1},
                            {node: "span", id:`${input}_final_reg`, classList: ["form_selection_text"], 
                            parent: 1},
                            {node:"p", id:`${input}_tips`, classList: ["form_input_tips"], parent:1},
                            {node:"button", id:`${input}_display_tips`, classList: ["btn", "a", "form_selection_input_btn"], parent:1},
                        {node:"p", id:`${input}_err`, classList: ["form_input_error"], parent:0}
                ]
            } else {
                template = [
                    {node: "div", id:`${input}_id`, name:`${input}`, classList: ["form_selection_container"], style:"display: none"},
                        {node:"div", classList: [`input_container`], parent:0},
                            {node: "label", id:`${input}_text`, parent:1},
                                {node: "span", style:"font-weight: bold;", 
                                textContent:fns_selector.text[`MSG_TEXT_TITLE_${input.toUpperCase()}`],
                                classList: [`${data.css_class}_text`], parent: 2},
                                {node: "text", textContent:" : ", parent: 2},
                                {node: "span", id:`${input}_final`, classList: ["form_selection_text"], 
                                parent: 2},
                            {node: "input", id: `${input}_input`, 
                            placeholder:fns_selector.text[`MSG_TEXT_PLACEHOLDER_${input.toUpperCase()}`], 
                            title:"Ex : https://www.custom-source.eu", type:"text", classList: ["form_input_text"], 
                            value: data.value, parent:1},
                            {node:"button", id:`${input}_btn_confirm`, classList: ["btn", "a", "form_selection_input_btn"], 
                            textContent: fns_selector.text.MSG_TEXT_BTN_CONFIRM, parent:1},
                            {node:"button", id:`${input}_btn_back`, classList: ["btn", "a", "form_selection_input_btn"], 
                            style: "display: none;", textContent: fns_selector.text.MSG_TEXT_BTN_BACK, parent:1},
                            {node:"button", id:`${input}_btn_show`, classList: ["btn", "a", "form_selection_input_btn"], 
                            textContent: fns_selector.text.MSG_TEXT_BTN_SHOW, parent:1},
                            {node:"button", id:`${input}_btn_attr`, classList: ["btn", "a", "form_selection_input_btn"],  
                            style: "display: none;", textContent: fns_selector.text.MSG_TEXT_BTN_ATTR, parent:1},
                            {node: "span", id:`${input}_final_attr`, classList: ["form_selection_text"], 
                            parent: 1},
                            {node:"button", id:`${input}_btn_reg`, classList: ["btn", "a", "form_selection_input_btn"], 
                            style: "display: none;", textContent: fns_selector.text.MSG_TEXT_BTN_REG, parent:1},
                            {node: "span", id:`${input}_final_reg`, classList: ["form_selection_text"], 
                            parent: 1},
                            {node:"button", id:`${input}_btn_disable`, classList: ["btn", "a", "form_selection_input_btn"], 
                            innerHTML: fns_global.icons.cross, parent:1},
                            {node:"p", id:`${input}_tips`, classList: ["form_input_tips"], parent:1},
                            {node:"button", id:`${input}_display_tips`, classList: ["btn", "a", "form_selection_input_btn"], parent:1},
                        {node:"p", id:`${input}_err`, classList: ["form_input_error"], parent:0}
                ]
            }
            let builder = form_builder(template)
            let child = builder.dom.firstChild
            let sub_dom = form_get("sub_dom")
            fns_selector.doc.fieldset.insertBefore(child, sub_dom)
            fns_selector.doc[`${input}`] = form_get(`${input}_id`)
            let doc = fns_selector.doc[`${input}`] 
            if(data.type === "optionnal")
                form_addclass(`${input}_input`, `form_warning`)
            if(typeof(data.sub) !== "undefined") {
                form_addclass(`${input}_id`, 'form_selection_container_sub')
            }
            if(data.value !== "") {
                let el = part_selector_get_sub_dom_element(data.value)[0]
                if(typeof(el) !== "undefined") {
                    el.classList.add(`${data.css_class}`)
                }
            }
            form_get(`${input}_input`).value = data.value
            form_get(`${input}_display_tips`).innerHTML = `${fns_global.icons.question}`
            part_selector_new_sub_dom_listener(input, data, doc)
            if(fns_selector.mode === "json") {
                let old_input = fns_selector.form_input[fns_selector.current_index - 1]
                if(typeof(old_input) !== "undefined")  {
                    let old_data = form_part_get_data(fns_selector, old_input)
                    if(old_data.value !== "" && old_data.value !== "\"\"")
                        part_selector_json_show(old_data.value)
                }
            }
            form_get(`${input}_display_tips`).addEventListener("click", (evt) => {
                let display = form_get(`${input}_display_tips`)
                let tips = form_get(`${input}_tips`)
                form_tips(display, tips, fns_selector.text[`MSG_TIPS_${input.toUpperCase()}`] )
            })
            form_get(`${input}_input`).addEventListener("input", (evt) => {
                console.log("INPUT", evt.target.value, data)
                data.value = evt.target.value
            })
            form_get(`${input}_btn_back`).addEventListener("click", (evt) => {
                part_selector_clear_selection()
                form_removeclass(`${input}_btn_reg`, "form_input_required")
                form_msg(`${input}_err`, "")
                part_selector_reset_data(input)
                part_selector_input_focus()
                form_msg(`${input}_final`, "")
                form_get(`${input}_final`).textContent = ""
                form_display(`${input}_input`, true)
                form_display(`${input}_btn_confirm`, true)
                form_display(`${input}_btn_back`, false)
                fns_selector.mode === "json" && input === "results" ? form_display(`${input}_btn_not_define`, true) : null
                form_display(`${input}_btn_attr`, false)
                form_display(`${input}_btn_reg`, false)
                part_selector_validator()
            })
            form_get(`${input}_btn_show`).addEventListener("click", (evt) => {
                part_selector_clear_selection()
                let path = µ.triw(data.value)
                if(path === "") {
                    form_input_err(`${input}_input`, `${input}_err`,  fns_selector.text.MSG_ERROR_VALUE_EMPTY)
                    data.test_passed = false
                    part_selector_validator()
                    return
                }
                if(fns_selector.mode === "get" || fns_selector.mode === "post") {
                    let el = document.getElementById("sub_dom").querySelector(`${path}`)
                    if(el.id === "")
                        el.setAttribute("id", "sub_dom_tmp_show")
                    el.classList.add(`${data.css_class}`)
                    window.location.hash = el.id
                    el.id === "sub_dom_tmp_show" ? el.removeAttribute("id") : null
                } else if(fns_selector.mode === "json") {
                    let el = part_selector_json_show(path)
                    el = el.parentNode
                    let keys = el.querySelectorAll(".key")
                    path = path.split(".").reverse()[0]
                    for(let key of keys) {
                        if(key.textContent === `"${path}"`) {
                            key.classList.add(`${data.css_class}`)
                            break
                        }
                    }
                }
                form_removeclass(`${input}_input`, "form_input_required")
                form_msg(`${input}_err`, "")
                data.test_passed = true
            })
            if(fns_selector.mode === "json" && input === "results") {
                form_get(`${input}_btn_not_define`).addEventListener( "click", (evt) => {
                    data.value = '""'
                    data.test_passed = true
                    form_removeclass(`${input}_input`, "form_input_required")
                    form_msg(`${input}_err`, "")
                    form_display(`${input}_input`, false)
                    form_display(`${input}_btn_confirm`, false)
                    form_display(`${input}_btn_back`, true)
                    fns_selector.mode === "json" && input === "results" ? form_display(`${input}_btn_not_define`, false) : null
                    typeof(data.attr) !== "undefined" && fns_selector.mode !== "json" ? form_display(`${input}_btn_attr`, true) : null
                    typeof(data.attr) !== "undefined" ? form_display(`${input}_btn_reg`, true) : null
                    form_get(`${input}_final`).textContent = `Results is not defined`
                    console.log(fns_selector.old_index === null && form_part_get_data(fns_selector, fns_selector.form_input[fns_selector.current_index - 1]).test_passed)
                    if(fns_selector.old_index === null && form_part_get_data(fns_selector, fns_selector.form_input[fns_selector.current_index - 1]).test_passed )
                        part_selector_build_input()
                    else
                        part_selector_input_focus()
                    add_property(fns_global.new_source_user, input, data.value)
                    part_selector_validator()
                })
            }
            if(typeof(data.regex) !== "undefined") {
                form_get(`${input}_btn_reg`).addEventListener("click", (evt) => {
                    let val = part_selector_sub_dom_get_element_text(input).value
                    if(val === "" || val === null) {
                        part_selector_destroy_regex(input)
                        form_input_err(`${input}_btn_reg`,`${input}_err`,   fns_selector.text.MSG_ERROR_TEXT_CONTENT_EMPTY)
                        return
                    }
                    form_removeclass(`${input}_btn_reg`, "form_input_required")
                    form_msg(`${input}_err`, "")
                    if(form_get(`${input}_reg`) === null)
                        part_selector_init_regex(input, data, val)                    
                    else
                        part_selector_destroy_regex(input)
                })
            }
            if(typeof(data.attr) !== "undefined" && fns_selector.mode !== "json") {
                form_get(`${input}_btn_attr`).addEventListener("click", (evt) => {
                    form_removeclass(`${input}_btn_reg`, "form_input_required")
                    form_msg(`${input}_err`, "")
                    if(form_get(`${input}_attr`) === null) {
                        let contents = part_selector_get_content(input, data, doc)
                        if(contents !== [])
                            part_selector_init_items(contents, input)
                    } else {
                        part_selector_destroy_items(input)
                    }
                })
            }
            if(data.type === "optionnal") {
                form_get(`${input}_btn_disable`).addEventListener("click", (evt) => {
                    let comp = document.createElement("div")
                    comp.innerHTML = fns_selector.text.MSG_TEXT_BTN_BACK
                    part_selector_destroy_regex(input)
                    part_selector_destroy_items(input)
                    if(evt.target.innerHTML !== comp.innerHTML) {
                        evt.target.innerHTML = fns_selector.text.MSG_TEXT_BTN_BACK
                        form_display(`${input}_input`, false)
                        form_display(`${input}_btn_confirm`, false)
                        form_display(`${input}_btn_show`, false)
                        form_display(`${input}_btn_back`, false)
                        form_display(`${input}_btn_reg`, false)
                        form_display(`${input}_btn_attr`, false)
                        form_get(`${input}_final`).textContent = fns_selector.text.MSG_TEXT_INPUT_DISABLED
                        data.test_passed = false
                        data.is_passed = true
                        if(fns_selector.old_index !== null) {
                            part_selector_input_focus()
                        } else {
                            form_removeclass(`${input}_input`, "form_input_required")
                            form_msg(`${input}_err`, "")
                            data.test_passed = false
                            part_selector_clear_selection()
                            part_selector_build_input()
                            part_selector_validator()
                        }
                    } else {
                        evt.target.innerHTML = fns_global.icons.cross
                        form_display(`${input}_input`, true)
                        if(data.test_passed) {
                            form_display(`${input}_btn_back`, true)
                            form_display(`${input}_btn_reg`, true)
                        } else {
                            form_display(`${input}_btn_confirm`, true)
                        } 
                        form_display(`${input}_btn_show`, true)
                        form_get(`${input}_final`).textContent = ""
                        data.test_passed = false
                        data.is_passed = false
                        part_selector_input_focus()
                    }
                })
            }
            form_get(`${input}_btn_confirm`).addEventListener("click", (evt) => {
                form_removeclass(`${input}_input`, "form_input_required")
                form_msg(`${input}_err`, "")
                let val = form_get(`${input}_input`).value
                if(val === "") {
                    form_input_err(`${input}_input`, `${input}_err`,  fns_selector.text.MSG_ERROR_VALUE_EMPTY)
                    data.test_passed = false
                    part_selector_validator()
                    return
                }
                if(data.type_content === "link" && fns_selector.mode !== "json") {
                    let el = form_get("sub_dom").querySelector(val)
                    if(el.localName !== "a") {
                        form_input_err(`${input}_input`, `${input}_err`, fns_selector.text.MSG_ERROR_IS_NOT_LINK)
                        data.test_passed = false
                        part_selector_validator()
                        return
                    }
                }
                let el
                if(fns_selector.mode === "get" ||  fns_selector.mode === "post")
                    el = form_get("sub_dom").querySelector(val)
                if(fns_selector.mode === "json") {
                    el = fns_global.virtual_doc
                    let path = val.split(".")
                    for(let p of path) {
                        if(el.length === undefined)
                            el = el[`${p}`]
                        else
                            el = el[0][`${p}`]
                        if(typeof(el) === "undefined")
                            break   
                    }
                }
                if(el === null  || typeof(el) === "undefined") {
                    form_input_err(`${input}_input`, `${input}_err`, fns_selector.text.MSG_ERROR_NOT_ELEMENT_FOUND )
                    data.test_passed = false
                    part_selector_validator()
                    return
                }
                form_display(`${input}_input`, false)
                form_display(`${input}_btn_confirm`, false)
                form_display(`${input}_btn_back`, true)
                fns_selector.mode === "json" && input === "results" ? form_display(`${input}_btn_not_define`, false) : null
                typeof(data.attr) !== "undefined" && fns_selector.mode !== "json" ? form_display(`${input}_btn_attr`, true) : null
                typeof(data.attr) !== "undefined" ? form_display(`${input}_btn_reg`, true) : null
                data.value = val
                if(typeof(data.attr) !== "undefined" && input !== "r_img" && fns_selector.mode !== "json") {
                    let text = part_selector_sub_dom_get_element_text(input).value
                    console.log(text)
                    if(text === "" || text === null) {
                        let contents = part_selector_get_content(input, data, doc)
                        if(contents !== [])
                            part_selector_init_items(contents, input)
                        form_input_err(`${input}_btn_attr`, `${input}_err`, fns_selector.text.MSG_ERROR_TEXT_CONTENT_EMPTY)
                        data.test_passed = false
                        part_selector_validator()
                        return
                    }
                    form_removeclass(`${input}_btn_attr`, "form_input_required") 
                    part_selector_destroy_items(input)
                }
                if(input === "results" && fns_selector.mode !== "json") {
                    for(let fns_input of fns_selector.form_input) {
                        if(fns_input !== "body" && fns_input !== "results") {
                            let d = fns_selector.data[`${fns_input}`]
                            for( let node of d.nodes) {
                                if(node === "")
                                    continue
                                let el = form_get("sub_dom").querySelector(`${data.value} ${node}`)
                                if(el) {
                                    d.value = part_selector_get_css_path(el, data.css_class)
                                    break
                                }
                            }
                        }
                    }
                }
                if(input === "r_dt" && fns_selector.mode !== "json") {
                    part_selector_destroy_regex(input)
                    let obj = part_selector_build_fmt_date_obj(data)
                    let path = data.value
                    let dates = part_selector_get_sub_dom_element(path)
                    console.log(dates)
                    for(let date of dates) {
                        if(date.localName === "time") {
                            let datetime = date.getAttribute("datetime")
                            if(typeof(datetime) !== "undefined") {
                                add_property(fns_global.new_source_user, "r_dt_attr", "datetime")
                                break
                            }

                        }
                        let preview = date.textContent
                        try {
                            let raw_res = mµ.parse_dt_str(
                                preview, 
                                fns_timezone.data.tags_timezone.value, 
                                1, 
                                obj.fmts, 
                                fns_global.month_nb_json)
                            console.log(raw_res)
                        } catch(err) {
                            part_selector_init_regex(input, data, preview)
                            form_addclass(`${input}_reg_input`, "form_input_required")
                            form_addclass(`${input}_reg_input_token`, "form_input_required")
                            form_msg(`${input}_reg_err`, fns_selector.text.MSG_ERROR_DATE_FOUND)
                            return
                        }
                    }
                    console.log(obj)
                }
                if(input === "r_img" && fns_selector.mode !== "json") {
                    let path = data.value
                    let img = part_selector_get_sub_dom_element(path)[0]
                    if(typeof(img) !== "undefined") {
                        let src = img.src
                        let alt = img.alt
                        if(src === "") {
                            fns_selector.data.r_img_src.skip = false
                            fns_selector.data.r_img_src.test_passed = false
                            fns_selector.data.r_img_src.value = data.value
                        }
                        if(alt === "") {
                            fns_selector.data.r_img_alt.skip = false
                            fns_selector.data.r_img_alt.test_passed = false
                            fns_selector.data.r_img_alt.value = data.value
                        }
                    }
                }
                if(input === "res_nb") {
                    let val = part_selector_sub_dom_get_element_text(input).value
                    if(val === "" || val === null) {
                        form_input_err(`${input}_btn_attr`,`${input}_err`, fns_selector.text.MSG_ERROR_TEXT_CONTENT_EMPTY)
                        return
                    }
                    if(typeof(val) !== "number") {
                        try {
                            let reg = new RegExp("[A-Za-z]")
                            let res = val.match(reg)
                            console.log(res, val)
                            if(res !== null && res !== []) {
                                reg = data.regex
                                if(reg === null)
                                    throw new Error(`No regex found for ${input}`)
                                res = µ.regextract(reg[0].replaceAll("\\\\", "\\"), val, reg[1])
                                console.log(res, val)
                                if(res === val)
                                    throw new Error(`Parse don't work for ${input}`)
                            }
                        } catch ( err ) {
                            form_get(`${input}_btn_reg`).dispatchEvent(new Event("click"))
                            form_input_err(`${input}_btn_reg`, `${input}_err`,   fns_selector.text.MSG_ERROR_FAIL_TO_PARSE)
                            return
                        }
                    }
                    form_removeclass(`${input}_btn_reg`, "form_input_required")
                    form_msg(`${input}_err`, "") 
                }
                if(input === "body") {
                    let path = data.value
                    let body = fns_global.virtual_doc.body
                    let el = body.querySelector(`${path}`)
                    el.value = form_get(`search_url_terms_input`).value
                    while(el.localName !== "form" && el.id !== "sub_dom")
                        el= el.parentNode
                    el.addEventListener("submit", (evt) => {
                        evt.preventDefault()
                        return false
                    })
                    el.addEventListener("formdata", async (evt) => {
                        let data = evt.formData
                        let dom = await part_selector_build_post_request(el, data)
                        let body = dom.body
                        body = new DOMParser().parseFromString( body.innerHTML, "text/html")
                        body = body.children[0].children[1]
                        part_selector_depth_clean_virtual_doc(body.childNodes)
                        part_selector_child_have_text_content(body.childNodes)
                        part_selector_set_sub_dom(body)
                    })
                    el.submit()
                }
                form_removeclass(`${input}_input`, "form_input_required")
                form_msg(`${input}_err`, "")
                part_selector_destroy_regex(input)
                part_selector_destroy_items(input)
                data.test_passed = true
                part_selector_clear_selection()
                part_selector_update_data_form(input)
                if(fns_selector.old_index === null && form_part_get_data(fns_selector, fns_selector.form_input[fns_selector.current_index - 1]).test_passed )
                    part_selector_build_input()
                else
                    part_selector_input_focus()
                add_property(fns_global.new_source_user, input, data.value)
                part_selector_validator()
            })
            form_display(`${input}_id`, true, "inline-block")
            fns_selector.current_index += 1
        }
    }
    function part_selector_update_data_form(input) {
        let data = fns_selector.data[`${input}`]
        if(input === "r_url") {
            let path = data.value
            let elements = part_selector_get_sub_dom_element(`${path}`)
            console.log(elements)
        }
        if(typeof(data) !== "undefined" && data.value !== "") {
            let content = part_selector_sub_dom_get_element_text(input)
            if(input === "results") {
                let path = data.value
                let elements = part_selector_get_sub_dom_element(`${path}`)
                form_get(`${input}_final`).textContent = `${elements.length} results in the web page`
                form_get(`${input}_final`).title = `${path} (${elements.length})`
            } else  {
                form_get(`${input}_final`).textContent = content.value
                form_get(`${input}_final`).title = `${content.name}="${content.value}"`
            }
            if(typeof(data.regex) !== "undefined") {
                if(data.type_regex === "string" && data.regex !== null) {
                    let reg = data.regex
                    let res = µ.regextract(µ.drop_escaped_quotes(reg[0]), content.value, reg[1])
                    form_get(`${input}_final`).textContent = res
                    form_get(`${input}_final`).title = `${content.name}="${content.value}"`
                    form_get(`${input}_final_reg`).textContent = `${reg[0]}`
                    form_get(`${input}_final_reg`).title = `["${reg[0]}", "${reg[1]}"]`
                } else if(data.type_regex === "list" && data.regex.length !== 0) {
                    let reg = data.regex[0]
                    console.log("Le moment ou ça bug", content.value, reg, µ.drop_escaped_quotes(reg[0]))
                    let res = mµ.parse_dt_str(
                        content.value, 
                        fns_timezone.data.tags_timezone.value, 
                        1, 
                        {
                            r_dt_fmt_1 : µ.drop_escaped_quotes(reg[0])
                        }, 
                        fns_global.month_nb_json)
                    form_get(`${input}_final`).textContent = res
                    form_get(`${input}_final`).title = `${content.name}="${content.value}"`
                    let length = data.regex.length
                    if(length > 1) {
                        form_get(`${input}_final_reg`).textContent = `(${reg[0]})`
                        form_get(`${input}_final_reg`).title = `["${reg[0]}", "${reg[1]}"]`
                    } else  {
                        form_get(`${input}_final_reg`).textContent = `(${reg[0]})`
                        form_get(`${input}_final_reg`).title = `["${reg[0]}", "${reg[1]}"] +${length - 1}`
                    }
                } else {
                    form_get(`${input}_final_reg`).textContent = ""
                    form_get(`${input}_final_reg`).title = ""
                }
            }
            if(typeof(data.attr) !== "undefined") {
                if(data.attr !== null) {
                    if(content.name !== "textContent") {
                        form_get(`${input}_final_attr`).textContent = `${content.name}`
                        form_get(`${input}_final_attr`).title = `${content.name}="${content.value}"`
                    } else {
                        form_get(`${input}_final`).textContent = content.value
                        form_get(`${input}_final`).title = `${content.name}="${content.value}"`
                    }
                } else {
                    form_get(`${input}_final_attr`).textContent = ""
                    form_get(`${input}_final_attr`).title = ``
                }
            }
        }
    }
    function part_selector_sub_dom_get_element_text(input) {
        let data = fns_selector.data[`${input}`]
        let path = µ.triw(data.value)
        if(fns_selector.mode === "get" || fns_selector.mode === "post") {
            let el = form_get("sub_dom").querySelector(`${path}`)
            let attr = data.attr //form_get(`${input}_final`).textContent
            attr = µ.triw(attr)
            if(attr === null)
                return {name : "textContent", value: el.textContent}
            else
                return { name : attr, value : el.getAttribute(`${attr}`) }
        } else if ( fns_selector.mode === "json") {
            let el = fns_global.virtual_doc
            path = path.split(".")
            for(let p of path) {
                if(el.length === undefined)
                    el = el[`${p}`]
                else
                    el = el[0][`${p}`]
            }
            return {name : path, value: el}
        }

    }
    function part_selector_json_show(json_path) {
        let render = form_get("sub_dom").querySelector(".renderjson")
        let el = render.querySelector("span")
        let path = json_path.split(".")
        let count = 0
        while( count < path.length) {
            let child = null
            for(let c of el.childNodes) {
                if(typeof(c.classList) === "undefined")
                    continue
                if(c.classList.contains("object") && c.classList.length === 1){
                    child = c
                    break
                } else if(c.classList.contains("array") && c.classList.length === 1 ) {
                    child = c
                    break
                }
            }
            child.querySelector(".disclosure").dispatchEvent(new Event("click"))
            child = child.nextSibling
            if(child.classList[0] === "object") {
                let key = null
                for(let c of child.childNodes) {
                    if(c.textContent === `"${path[count]}"`) {
                        key = c
                        break
                    }
                }
                el = key.nextSibling.nextSibling
            } else if( child.classList[0] === "array") {
                el = child.childNodes[3]
                continue
            }
            count += 1
        }
        return el
    }
    function part_selector_input_focus() {
        let index = null
        for(let key of fns_selector.form_input) {
            let inp = fns_selector.data[key]
            if(!inp.methode.includes(fns_selector.mode))
                continue
            if(inp.test_passed)
                continue
            if(inp.type === "optionnal" && inp.is_passed)
                    continue
            index = fns_selector.form_input.indexOf(key)
            break
        }
        if(index !== null) {
            if(fns_selector.old_index === null)
                fns_selector.old_index = fns_selector.current_index
            else
                fns_selector.old_index = fns_selector.old_index === index + 1 ? null : fns_selector.old_index 
            fns_selector.current_index = index + 1
            let input = fns_selector.form_input[fns_selector.current_index - 1]
            let data = fns_selector.data[input]
            let doc = fns_selector.doc[input]
            part_selector_new_sub_dom_listener(input, data, doc)
        }
    }
    function part_selector_new_sub_dom_listener(input, data, doc) {
        part_selector_remove_sub_dom_listener()
        form_get("sub_dom").addEventListener("click", (evt) => { 
            let el = evt.target
            part_selector_clear_selection()
            let path 
            if(fns_selector.mode === "get" || fns_selector.mode === "post") {
                if(!el.classList.contains(`${data.css_class}`))
                    el.classList.add(`${data.css_class}`)
                path = part_selector_get_css_path(el, data.css_class)
            } else if( fns_selector.mode === "json")
                path = part_selector_get_path_json(el)
            console.log(document.getElementById(`${input}_input`))
            form_get(`${input}_input`).value = path
        })
    }
    function part_selector_remove_sub_dom_listener() {       
        let old_element = form_get("sub_dom");
        let new_element = old_element.cloneNode(true);
        old_element.parentNode.replaceChild(new_element, old_element);
        if(fns_selector.mode === "json")
            part_selector_set_sub_dom(fns_global.virtual_doc)
    }
    function part_selector_clear_selection() {
        for(let cl of fns_selector.class_list) {
            let old_el = document.getElementsByClassName(`${cl}`)[0]
            if(old_el !== undefined)
                old_el.classList.remove(`${cl}`)
        }
    }
    function part_selector_get_css_path(element, css_class){
        //let path = ElementCssPath(fns.virtual_doc.querySelector(element), { unique : true, startWithClosestID: true } )
        let path
        let pile = []
        let el = element
        while(el.id !== "sub_dom") {
            let tmp = ""
            if(el.id){
                tmp = `#${el.id}`
            } else if(el.classList.length !== 0 && el.classList[0] !== css_class && el.classList[0] !== fns_selector.sub_dom.class_show_div ) {
                tmp = `.${el.classList[0]}`
            } else {
                tmp = `${el.localName}`
            }
            pile.push(tmp)
            el = el.parentNode
        }
        pile = pile.reverse()
        path = pile.join(" > ")
        return path
    }
    function part_selector_get_path_json(el) {
        let path = []
        path.push(el.textContent.replace(/"/gi, ''))
        while(el.classList[0] !== "renderjson") {
            el = el.parentNode
            if(el.classList[0] === "object" || el.classList[0] === "array") {
                el = el.parentNode
                if(el.parentNode.classList[0] !== "renderjson") {
                    let pre = el
                    while( el !== null && el.classList[0] !== "key" ) {
                        pre = el
                        el = el.previousSibling
                    }
                    if(el === null)
                        el = pre
                }
            }
            if(el.classList[0] === "key")
                path.push(el.textContent.replace(/"/gi, ''))
        }
        path = path.reverse()
        return path.join(".")
    }
    function part_selector_get_content(input, data, doc) {
        let el = form_get("sub_dom").querySelector(data.value)
        console.log(el.localName, el)
        let black_list = [/^id/, /^class/]
        let attrs = []
        if(el.textContent !== "")
            attrs.push({name: "textContent", value: el.textContent})
        for(let attr of el.attributes) {
            let ok = true
            for(let reg of black_list) {
                if(attr.localName.match(reg) !== null)
                    ok = false
            }
            if(ok)
                attrs.push({name: attr.localName, value: attr.value})
        }
        return attrs
    }
    function part_selector_init_items(contents, input) {
        let list = [
            {node: "div", id:`${input}_attr`},
                {node: "ul", id: `${input}_attr_list`, parent:0 , classList: ["form_list"]},
                    {node: "div", id:`${input}_attr_placholder`, parent: 1, classList: ["form_item_placeholder"], textContent: "Choose a good value"}
        ]
        let builder = form_builder(list)
        let parent = form_get(`${input}_id`)
        parent.appendChild(builder.dom.firstChild)
        let count = 0
        for(let content of contents) {
            let item = [
                {node: "li", id: `${input}_attr_item_${count}`, classList: ["form_item_list"], title: `${content.name}="${content.value}"`},
                {node: "span", id: "value_attr", parent: 0, textContent: content.value, classList: ["form_item_text"]}
            ]
            builder = form_builder(item)
            parent = form_get(`${input}_attr_list`)
            parent.appendChild(builder.dom.firstChild)
            parent.querySelector(`#${input}_attr_item_${count}`).addEventListener("click", (evt) => {
                if(content.name !== "textContent") {
                    let data = fns_selector.data[`${input}`]
                    data.attr = content.name
                    add_property(fns_global.new_source_user, `${input}_attr`, data.attr)
                } else {
                    remove_property(fns_global.new_source_user, `${input}_attr`)
                }
                part_selector_destroy_items(input)
                form_get(`${input}_btn_confirm`).dispatchEvent(new Event("click"))
            })
            count += 1
        }
    }
    function part_selector_destroy_items(input) {
        let child = form_get(`${input}_attr`)
        if(child !== null)
            child.parentNode.removeChild(child)
    }
    function part_selector_init_regex(input, data, preview) {
        let template = [
            {node: "div", id: `${input}_reg`, classList: ["form_list"]},
                {node: "div", id : `${input}_reg_placeholder`, classList: ["form_item_placeholder"], 
                textContent: "Created a regex", parent: 0},
                {node: "ul", id: `${input}_reg_list`, parent: 0},
                {node: "p", id: `${input}_reg_preview_text`, parent: 0},
                    {node: "span", textContent: `Current selected text : `, parent: 3},
                    {node: "span", id: `${input}_reg_preview`, textContent: `${preview}`, parent: 3},
                {node: "div", id: `${input}_reg_input_part`, classList: ["input_container"], parent: 0},
                    {node: "input", id: `${input}_reg_input`, type:"text", 
                    classList:["form_input_text", "form_input_text_reg"], 
                    placeholder:fns_selector.text.MSG_TEXT_REG_PLACEHOLDER, parent: 6},
                    {node: "input", id: `${input}_reg_input_token`, type:"text", 
                    classList:["form_input_text", "form_input_text_reg"], 
                    placeholder:fns_selector.text.MSG_TEXT_TOKEN_PLACEHOLDER, value:"$1", parent: 6},
                    {node: "button", id: `${input}_reg_input_btn_add`, classList: ["btn", "a", "form_selection_input_btn"], 
                    textContent: "Add", parent: 6},
                    {node: "button", id: `${input}_reg_input_btn_reset`, classList: ["btn", "a", "form_selection_input_btn"], 
                    textContent: "RAZ", parent: 6},
                {node: "p", id: `${input}_reg_err`, classList:["form_input_error"], parent: 0},
                {node: "p", id: `${input}_reg_result`, parent: 0}
        ]
        let builder = form_builder(template)
        form_get(`${input}_err`).parentElement.appendChild(builder.dom.firstChild)
        form_get(`${input}_reg_input_token`).addEventListener("input", (evt) => {
            form_get(`${input}_reg_input`).dispatchEvent(new Event("input"))
        })
        form_get(`${input}_reg_input`).addEventListener("input", (evt) => {
            let val = evt.target.value
            console.log("INPUT BEFORE", val)
            val = µ.drop_escaped_quotes(val)
            console.log("INPUT AFTER", val)
            if(val === "") {
                form_input_err(`${input}_reg_input`, `${input}_reg_err`, fns_selector.text.MSG_ERROR_VALUE_EMPTY)
                form_msg(`${input}_reg_result`, "")
                return
            }
            let token = µ.triw(form_get(`${input}_reg_input_token`).value)
            if(token === "") {
                form_input_err(`${input}_reg_input_token`, `${input}_reg_err`, fns_selector.text.MSG_ERROR_VALUE_EMPTY)
                form_msg(`${input}_reg_result`, "")
                return
            }
            try {
                new RegExp(val)
            } catch(ex) {
                form_input_err(`${input}_reg_input`, `${input}_reg_err`, fns_selector.text.MSG_ERROR_REGEX_NOT_VALID)
                form_msg(`${input}_reg_result`, "")
                return
            }
            let res
            if(input === "r_dt") {
                try {
                    res = mµ.parse_dt_str(
                        preview, 
                        fns_timezone.data.tags_timezone.value, 
                        1, 
                        {
                            r_dt_fmt_1 : [
                                val,
                                token
                            ]
                        }, 
                        fns_global.month_nb_json)
                    console.log(val, token, res)
                } catch(err) {
                    form_input_err(`${input}_reg_input`, `${input}_reg_err`, fns_selector.text.MSG_ERROR_REGEX_NOT_VALID)
                    form_msg(`${input}_reg_result`, "")
                    return
                }
            } else {
                res = µ.regextract(val, preview, token)
            }
            console.log(val, token, res)
            if(res === preview || res === "") {
                form_addclass(`${input}_reg_input`, "form_input_required")
                form_addclass(`${input}_reg_input_token`, "form_input_required")
                form_msg(`${input}_reg_err`, fns_selector.text.MSG_ERROR_FAIL_TO_PARSE)
                form_msg(`${input}_reg_result`, "")
                return
            }
            form_msg(`${input}_reg_result`, `Result of parse : ${res}`)
            form_removeclass(`${input}_reg_input`, "form_input_required")
            form_removeclass(`${input}_reg_input_token`, "form_input_required")
            form_msg(`${input}_reg_err`, "")
        })
        form_get(`${input}_reg_input_btn_add`).addEventListener("click", (evt) => {
            form_removeclass(`${input}_reg_input`, "form_input_required")
            form_msg_err(`${input}_reg_err`, "")
            let reg_val = form_get(`${input}_reg_input`).value
            console.log("BEFORE", reg_val)
            reg_val = µ.drop_escaped_quotes(reg_val)
            console.log("AFTER", reg_val)
            if(reg_val === "") {
                form_input_err(`${input}_reg_input`, `${input}_reg_err`,  fns_selector.text.MSG_ERROR_VALUE_EMPTY)
                form_msg(`${input}_reg_result`, "")
                return
            }
            let token_val = µ.triw(form_get(`${input}_reg_input_token`).value)
            if(token_val === "") {
                form_input_err(`${input}_reg_input_token`, `${input}_reg_err`,  fns_selector.text.MSG_ERROR_VALUE_EMPTY)
                form_msg(`${input}_reg_result`, "")
                return
            }
            try {
                console.log(reg_val)
                new RegExp(reg_val)
            } catch(ex) {
                form_input_err(`${input}_reg_input`, `${input}_reg_err`,  fns_selector.text.MSG_ERROR_REGEX_NOT_VALID)
                form_msg(`${input}_reg_result`, "")
                return
            }
            let res
            if(input === "r_dt") {
                try {
                    res = mµ.parse_dt_str(
                        preview, 
                        fns_timezone.data.tags_timezone.value, 
                        1, 
                        {
                            r_dt_fmt_1 : [
                                reg_val,
                                token_val
                            ]
                        }, 
                        fns_global.month_nb_json)
                } catch(err) {
                    form_input_err(`${input}_reg_input`, `${input}_reg_err`,  err)
                    form_msg(`${input}_reg_result`, "")
                    return
                }
            } else {
                res = µ.regextract(reg_val, preview, token_val)
            }
            if(res === preview) {
                form_addclass(`${input}_reg_input`, "form_input_required")
                form_addclass(`${input}_reg_input_token`, "form_input_required")
                form_msg(`${input}_reg_err`, fns_selector.text.MSG_ERROR_FAIL_TO_PARSE)
                form_msg(`${input}_reg_result`, "")
                return
            }
            let val = [reg_val, token_val]
            if(data.type_regex === "string")
                data.regex = val
            else if (data.type_regex === "list") {
                if(data.index_regex !== -1) {
                    data.regex[data.index_regex] = val
                    data.index_regex = -1
                } else {
                    data.regex.push(val)
                }
            } else
                throw new Exception(`You have forget to define "type_regex" property for "${input}" key in data fns_selector`)
            form_removeclass(`${input}_reg_input`, "form_input_required")
            form_removeclass(`${input}_reg_input_token`, "form_input_required")
            form_msg(`${input}_reg_err`, "")
            part_selector_update_item_regex(input, data)
            form_get( `${input}_reg_input`).value = ""
            form_get( `${input}_reg_input_token`).value = "$1"
            if(data.type_regex === "string")
                part_selector_destroy_regex(input)
            if(input === "r_dt") {
                part_selector_set_dt_fmt(data.regex)
            } else {
                add_property(fns_global.new_source_user, `${input}_re`, data.regex)
            }
            part_selector_update_data_form(input)
            form_get(`${input}_btn_confirm`).dispatchEvent(new Event("click"))
        })
        form_get(`${input}_reg_input_btn_reset`).addEventListener("click", (evt) => {
            form_removeclass(`${input}_reg_input`, "form_input_required")
            form_removeclass(`${input}_reg_input_token`, "form_input_required")
            form_msg(`${input}_reg_err`, "")
            form_get( `${input}_reg_input`).value = ""
            form_get( `${input}_reg_input_token`).value = "$1" 
            fns_selector.data[`${input}`].index_regex = -1
        })
        part_selector_update_item_regex(input, data)
    }
    function part_selector_destroy_regex(input) {
        let child = form_get(`${input}_reg`)
        console.log(child, typeof(child))
        if(child !== null)
            child.parentNode.removeChild(child)
    }
    function part_selector_update_item_regex(input, data) {
        let list = form_get(`${input}_reg_list`)
        list.innerHTML = ""
        if(data.regex !== null && data.regex !== []) {
            let count = 0
            if(data.type_regex === "string") {
                part_selector_create_item_regex(input, data.regex, count, list)
            } else if(data.type_regex === "list") {
                for(let reg of data.regex) {
                    part_selector_create_item_regex(input, reg, count, list)
                    count += 1
                }
            } else
                throw new Exception(`You have forget to define "type_regex" property for "${input}" key in data fns_selector`)
        }
    }
    function part_selector_create_item_regex(input, reg, number, list) {
        let template_item = [
            {node: "li", id: `${input}_reg_item_${number}`, classList: ["form_item_list"]},
            {node: "span", id: `${input}_reg_value_${number}`, textContent: `${reg[0]}`, 
            parent: 0, classList: ["form_item_text"], title: `Regex : "${reg[0]}", Token: "${reg[1]}"`},
            {node: "button", id: `${input}_reg_btn_edit_${number}`, classList: ["form_item_btn"], 
            innerHTML: fns_global.icons.pen, parent:0},
            {node: "button", id: `${input}_reg_btn_delete_${number}`, classList: ["form_item_btn"],
            innerHTML: fns_global.icons.cross, parent:0}
        ]
        let builder = form_builder(template_item)
        let item = builder.dom.firstChild
        list.appendChild(item)
        form_get(`${input}_reg_btn_edit_${number}`).addEventListener("click", (evt) => {
            let data = fns_selector.data[`${input}`]
            console.log(data)
            let reg = null
            if(data.type_regex === "string") {
                reg = data.regex
            } else if(data.type_regex === "list") {
                let index = Number.parseInt(evt.target.id.split("_").pop())
                data.index_regex = index
                reg = data.regex[index]
            }
            if(input === "r_dt") {
                part_selector_set_dt_fmt(data.regex)
            } else {
                add_property(fns_global.new_source_user, `${input}_re`, data.regex)
            }
            form_get( `${input}_reg_input`).value = reg[0]
            form_get( `${input}_reg_input_token`).value = reg[1]
        })
        form_get(`${input}_reg_btn_delete_${number}`).addEventListener("click", (evt) => {
            let data = fns_selector.data[`${input}`]
            if(data.type_regex === "string") {
                data.regex = null
                remove_property(fns_global.new_source_user, `${input}_re`)
            } else if(data.type_regex === "list") {
                let index = Number.parseInt(evt.target.id.split("_").pop())
                if(index === data.index_regex)
                    data.index_regex = -1
                else if (index < data.index_regex)
                    data.index_regex -= 1
                data.regex = data.regex.filter( (val, i) => {return i !== index})
                part_selector_set_dt_fmt(data.regex)
            }
            part_selector_update_item_regex(input, data)
        })
    }
    function part_selector_set_dt_fmt(regex) {
        let count = 1
        fns_global.new_source_user[``]
        while( typeof(fns_global.new_source_user[`r_dt_fmt_${count}`]) !== "undefined" ) {
            remove_property(fns_global.new_source_user, `r_dt_fmt_${count}`)
            count += 1
        }
        count = 1
        for(let dt of regex) {
            add_property(fns_global.new_source_user, `r_dt_fmt_${count}`, dt)
            count += 1
        }
    }
    function part_selector_build_fmt_date_obj(data) {
        let fmts = {}
        let count = 0
        for(let reg of data.regex) {
            count += 1
            add_property(fmts, `r_dt_fmt_${count}`, reg)
        }
        return {
            fmts : fmts,
            count: count
        }
    }
    function part_selector_get_sub_dom_element(path) {
        let sub_dom = form_get("sub_dom")
        if(fns_selector.mode === "get" || fns_selector.mode === "post") {
            return sub_dom.querySelectorAll(path)
        } else if(fns_selector.mode === "json") {
            path = path.split(".")
            let json = fns_global.virtual_doc
            for(let p of path) {
                json = json[p]
            }
            return json
        }
    }
    function part_selector_set_sub_dom(src) {
        let sub_dom = form_get("sub_dom")
        sub_dom.innerHTML = ""
        if(fns_selector.mode === "get" || fns_selector.mode === "post") {
            sub_dom.innerHTML = src.innerHTML
            let links = sub_dom.querySelectorAll("a")
            console.log("LINKS", links)
            for(let link of links) {
                link.setAttribute("disabled", "true")
                link.disabled = true
                link.addEventListener("click", (evt) => {
                    return false
                })
            }
        } else if(fns_selector.mode === "json") {
            sub_dom.appendChild( renderjson(src) ) 
        }
    }
    async function part_selector_build_post_request(form, data) {
        let request = {
            method: 'POST',
            headers: { 
                'Content-Type': 'application/x-www-form-urlencoded',
            },
            body : ""
        }
        for(let [key, value] of data.entries()) {
            request.body === "" ? request.body += `${key}=${value}` : request.body += `&${key}=${value}`
        }
        console.log(form.action, request)
        let raw_post = await fetch(form.action, request)
        let raw_text_post = await raw_post.text()
        let test_html_post = new DOMParser().parseFromString(raw_text_post, "text/html")
        return test_html_post
    }
    function part_selector_validator() {
        let valid = true
        for(let d of Object.values(fns_selector.data)) {
            if(d.type === "required"){
                valid = valid && d.test_passed
            }
        }
        fns_selector.completed = valid
        form_update_progress_bar()
    }
    var themes_list = []
    function part_tags_info_init() {
        part_tags_info_event()
        if(document.getElementById("part_tags_info_id").style.display === "") {
            fns.current_step_form += 1
            document.getElementById("legend_part_tags_info").textContent = `Step ${form_calcul_step()} %`
            document.getElementById("part_tags_info_id").style.display = "block"
        }
    }

    function part_tags_info_event() {
        create_themes_event()
        create_tech_search_system_event()
        create_tech_accuracy_result_event()
    }
    async function create_themes_choice(){
        let themes = await build_themes_list()
        let element = document.getElementById('themes_select')
        for(let theme of themes) {
            let option = document.createElement('option')
            option.text = theme.charAt(0).toUpperCase() + theme.substr(1).toLowerCase()
            option.value = theme
            element.add(option)
        }
        let select_opt = {
            removeItemButton: true,
            resetScrollPosition: false,
            placeholder: true,
            placeholderValue: '*',
            duplicateItemsAllowed: false,
            searchResultLimit: 8
        }
        fns.choice_themes = new Choices(element, Object.assign(select_opt))
    }
    async function build_themes_list() {
        let themes = []
        let sources = await fetch('json/sources.json')
        sources = await sources.json()
        for(let [key, obj] of Object.entries(sources)){
            if(obj.tags.themes !== undefined){
                //if(obj.tags.themes.includes("test"))
                    //console.log(obj)
                themes = [...themes, ...obj.tags.themes.filter((v) => !themes.includes(v))]
            }
        }
        return themes.sort()
    }
    function update_themes_choice(theme) {
        fns.choice_themes.setValue([
            {
                label : theme.charAt(0).toUpperCase() + theme.substr(1).toLowerCase(), 
                value: theme
            }
        ])
    }
    function create_themes_event() {
        fns.choice_themes.passedElement.element.addEventListener("addItem", (evt) => {
            let theme
            try {
                theme = evt.detail.value
                if(theme === "")
                    throw new Error("Value is empty")
            } catch(ex) {
                console.log(ex)
                return
            }
            add_theme_to_themes_list(theme)
            refresh_result_new_source_area()
        })
        fns.choice_themes.passedElement.element.addEventListener("removeItem", (evt) => {
            let theme
            try {
                theme = evt.detail.value
                if(theme === "")
                    throw new Error("Value is empty")
            } catch(ex) {
                console.log(ex)
                return
            }
            remove_theme_to_themes_list(theme)
            refresh_result_new_source_area()
        })
        document.getElementById("themes_user_btn_new").addEventListener("click", (evt) => {
            display_themes_user_input()
        })
        document.getElementById("themes_user_btn_finish").addEventListener("click", (evt) => {
            let theme = document.getElementById("themes_user_input").value
            theme = theme.toLowerCase()
            add_theme_to_themes_list(theme)
            update_themes_choice(theme)
            hide_themes_user_input()
        })
    }
    function add_theme_to_themes_list(theme) {
        if(Object.keys(fns.new_source_tags_user).includes("themes")){
            let themes = fns.new_source_tags_user.themes
            if(!themes.includes(theme)){
                themes.push(theme)
                add_property(fns.new_source_tags_user, "themes", themes)
            }
        } else {
            add_property(fns.new_source_tags_user, "themes", [theme])
        }
    }
    function remove_theme_to_themes_list(theme) {
        if(Object.keys(fns.new_source_tags_user).includes("themes")){
            let themes = fns.new_source_tags_user.themes
            if(themes.includes(theme)){
                themes = themes.filter( (th) => {return th !== theme} )
                add_property(fns.new_source_tags_user, "themes", themes)
            }
        }
    }
    function display_themes_user_input() {
        let div_input = document.getElementById("themes_user_input_id")
        let div_new = document.getElementById("themes_user_btn_id")
        div_input.querySelector("#themes_user_input").value = null
        div_input.style.display = "inline"
        div_new.style.display = "none"
    }
    function hide_themes_user_input() {
        document.getElementById("themes_user_input_id").style.display = "none"
        document.getElementById("themes_user_btn_id").style.display = "inline"
    }
    const CLASS_SEARCH_SYSTEM_USER_CHOICE = "tech_search_system_user_choice"
    const TAG_INTERN = "internal search"
    const TAG_EXTERN = "external search"
    function create_tech_search_system_event() {
        document.getElementById("tech_search_system_user_input_intern").addEventListener("click", (evt) => {
            let value = evt.target.value
            display_choice_tech_search_system(value)
            remove_tech_search_system_value()
            add_tech_search_system_value(TAG_INTERN)
            refresh_result_new_source_area()
        })
        document.getElementById("tech_search_system_user_input_extern").addEventListener("click", (evt) => {
            let value = evt.target.value
            display_choice_tech_search_system(value)
            remove_tech_search_system_value()
            add_tech_search_system_value(TAG_EXTERN)
            refresh_result_new_source_area()
        })
        document.getElementById("tech_search_system_user_input_both").addEventListener("click", (evt) => {
            let value = evt.target.value
            display_choice_tech_search_system(value)
            remove_tech_search_system_value()
            add_tech_search_system_value(TAG_INTERN)
            add_tech_search_system_value(TAG_EXTERN)
            refresh_result_new_source_area()
        })
    }
    function display_choice_tech_search_system(value) {
        let choice = document.getElementById("tech_search_system_response")
        if(!choice.classList.contains(CLASS_SEARCH_SYSTEM_USER_CHOICE))
            choice.classList.add(CLASS_SEARCH_SYSTEM_USER_CHOICE)
        choice.textContent = value
    }
    function add_tech_search_system_value(value) {
        let techs = fns.new_source_tags_user["tech"]
        if(typeof(techs) === "undefined") {
            add_property(fns.new_source_tags_user, "tech", [value])
        } else {
            techs.push(value)
            add_property(fns.new_source_tags_user, "tech", techs)
        }
    }
    function remove_tech_search_system_value() {
        let techs = fns.new_source_tags_user["tech"]
        if(typeof(techs) !== "undefined"){
            techs = techs.filter( (tech) => { return tech !== TAG_INTERN && tech !== TAG_EXTERN } )
            add_property(fns.new_source_tags_user, "tech", techs)
        }
    }
    const CLASS_ACCURACY_RESULT_USER_CHOICE = "tech_accuracy_result_user_choice"
    const TAG_EXACT = "exact"
    const TAG_APPROX = "approx"
    function create_tech_accuracy_result_event() {
        document.getElementById("tech_accuracy_result_user_input_exact").addEventListener("click", (evt) => {
            let value = evt.target.value
            display_choice_tech_accuracy_result(value)
            remove_tech_accuracy_result_value()
            add_tech_accuracy_result_value(TAG_EXACT)
            refresh_result_new_source_area()
        })
        document.getElementById("tech_accuracy_result_user_input_approx").addEventListener("click", (evt) => {
            let value = evt.target.value
            display_choice_tech_accuracy_result(value)
            remove_tech_accuracy_result_value()
            add_tech_accuracy_result_value(TAG_APPROX)
            refresh_result_new_source_area()
        })
    }
    function display_choice_tech_accuracy_result(value) {
        let choice = document.getElementById("tech_accuracy_result_response")
        if(!choice.classList.contains(CLASS_ACCURACY_RESULT_USER_CHOICE))
            choice.classList.add(CLASS_ACCURACY_RESULT_USER_CHOICE)
        choice.textContent = value
    }
    function add_tech_accuracy_result_value(value) {
        let techs = fns.new_source_tags_user["tech"]
        if(typeof(techs) === "undefined") {
            add_property(fns.new_source_tags_user, "tech", [value])
        } else {
            techs.push(value)
            add_property(fns.new_source_tags_user, "tech", techs)
        }
    }
    function remove_tech_accuracy_result_value() {
        let techs = fns.new_source_tags_user["tech"]
        if(typeof(techs) !== "undefined"){
            techs = techs.filter( (tech) => { return tech !== TAG_EXACT && tech !== TAG_APPROX } )
            add_property(fns.new_source_tags_user, "tech", techs)
        }
    }
    function optimized_json_object_value() {
        let result_path = fns.new_source_user["results"]
        fns.new_source_user["r_h1"] = fns.new_source_user["r_h1"].replace(result_path, "")
        fns.new_source_user["r_txt"] = fns.new_source_user["r_txt"].replace(result_path, "")
        fns.new_source_user["r_img"] = fns.new_source_user["r_img"].replace(result_path, "")
        fns.new_source_user["r_dt"] = fns.new_source_user["r_dt"].replace(result_path, "")
        fns.new_source_user["r_by"] = fns.new_source_user["r_by"].replace(result_path, "")
    }
    var init = false
    var codeM
	async function refresh_result_new_source_area(){ 
        //optimized_json_object_value()
        if(init === false) {
            add_property(fns.new_source_user, "tags", fns.new_source_tags_user)
            //document.getElementById("result_area_new_source").textContent = `{\n ${format_JSON_obj(fns.new_source_user, "\t")} \n}`
            document.getElementById("result_area_new_source").textContent = JSON.stringify(fns.new_source_user, null, 4)
            codeM = CodeMirror.fromTextArea(
                document.getElementById("result_area_new_source"), {
                    mode: 'application/json',
                    // mode: {name:'javascript', json:true},
                    indentWithTabs: true,
                    screenReaderLabel: 'CodeMirror',
                    readOnly: 'nocursor'
                }
            )
            init = true
        } else {
            add_property(fns.new_source_user, "tags", fns.new_source_tags_user)
            //let content = `{\n ${format_JSON_obj(fns.new_source_user, "\t")} \n}`
            let content = JSON.stringify(fns.new_source_user, null, 4)
            codeM.setValue(content)
        }
	}
    function format_JSON_obj(obj, tab) {
        let str = ""
        let keys = Object.keys(obj)
        let length = keys.length
        let count = 0
        for(let key of keys) {
            let value = obj[`${key}`]
            let tmp = ""
            console.log(value, typeof(value))
            if( value instanceof Array)
                tmp = `${tab}"${key}": [${value}],\n`
            else if( value instanceof Object)
                tmp = `${tab}"${key}": {\n ${format_JSON_obj(value, tab + "\t")} \n${tab}}\n`
            else if(count + 1 === length)
                tmp = `${tab}"${key}": "${value}"`
            else
                tmp = `${tab}"${key}": "${value}",\n`
            str += tmp
            count += 1
        }
        return str
    }
})()